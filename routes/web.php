<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ReportsController;
use App\Http\Controllers\AeropuertoController;
use App\Http\Controllers\BancosController;
use App\Http\Controllers\ClasificacionController;
use App\Http\Controllers\ContenedorController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\EmailsController;
use App\Http\Controllers\EntidadesBancoController;
use App\Http\Controllers\EntidadesController;
use App\Http\Controllers\EstadoController;
use App\Http\Controllers\OperacionController;
use App\Http\Controllers\OperacionDetalleController;
use App\Http\Controllers\OperationDetailsController;
use App\Http\Controllers\PuertoController;
use App\Http\Controllers\CargoController;
use App\Http\Controllers\ConteinerReportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Auth::routes([
	'register' => false
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('users', UserController::class);
    Route::resource('roles', RoleController::class);
    Route::resource('permissions', PermissionController::class);
    Route::resource('posts', PostController::class);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
	//Route::resource('users', UserController::class);
	Route::resource('users', UsersController::class);
	Route::get('get-data-users-datatables', ['as'=>'get.users','uses'=>'App\Http\Controllers\UsersController@getData']);
	Route::post('users/restore', 'App\Http\Controllers\UsersController@restore')->name('users/restore');
    Route::resource('roles', RoleController::class);
	Route::get('get-data-roles-datatables', ['as'=>'get.roles','uses'=>'App\Http\Controllers\RoleController@getData']);
	Route::post('roles/restore', 'App\Http\Controllers\RoleController@restore')->name('roles/restore');
    Route::resource('permissions', PermissionController::class);
	Route::get('get-data-permissions-datatables', ['as'=>'get.permissions','uses'=>'App\Http\Controllers\PermissionController@getData']);
	Route::post('permissions/restore', 'App\Http\Controllers\PermissionController@restore')->name('permissions/restore');
    Route::resource('posts', PostController::class);
	//Route::resource('reportgneral', ReportsController::class);
	Route::get('report/general', ['as'=>'get.general','uses'=>'App\Http\Controllers\ReportsController@index']);
	Route::post('report/general/download', ['as'=>'get.download','uses'=>'App\Http\Controllers\ReportsController@download']);
	Route::post('generate/general', 'App\Http\Controllers\ReportsController@getData')->name('generate/general');
	
	/**Conteiner Report */
	Route::get('report/containers', [ConteinerReportController::class, 'index'])->name('get.containers.report');
	Route::post('generate/report/containers', ['as'=>'get.reportcontainers','uses'=>'App\Http\Controllers\ConteinerReportController@generateReport']);

	/* Bancos */
	Route::resource('bancos', BancosController::class);
	Route::get('get-data-bancos-datatables', [BancosController::class, 'getData'])->name('get.bancos');

	/* Entidades */
	Route::resource('entidades', EntidadesController::class);
	Route::post('entidades/update',[EntidadesController::class, 'update'])->name('entidades/update');
	Route::get('get-data-entidades-datatables', [EntidadesController::class, 'getData'])->name('get.entidades');

	/* Entidades Banco*/
	Route::resource('entidadbanco', EntidadesBancoController::class);
	Route::get('get-data-entidades-banco-datatables', [EntidadesBancoController::class, 'EntidadesController@getDataBanco'])->name('get.entidades.banco');
	Route::get('get-data-ent-banco-datatables', [EntidadesBancoController::class, 'getDataBancoView'])->name('get.ent.banco');

	/* clasificacion admin */
	Route::resource('clasificaciones', ClasificacionController::class);
	Route::get('get-data-clasificaciones-datatables', [ClasificacionController::class, 'getData'])->name('get.clasificaciones');

	/* contenedores operaciones */
	Route::resource('contenedores', ContenedorController::class);
	Route::get('get-data-contenedores-datatables', [ContenedorController::class, 'getData'])->name('get.contenedores');

	/* puertos operaciones */
	Route::resource('puertos', PuertoController::class);
	Route::get('get-data-puertos-datatables', [PuertoController::class, 'getData'])->name('get.puertos');

	/* catalogo aeropuertos de  operaciones */
	Route::resource('airports', AeropuertoController::class);
	Route::get('get-data-airports-datatables', [AeropuertoController::class, 'getData'])->name('get.airports');

	/* catalogo estados de  operaciones */
	Route::resource('states', EstadoController::class);
	Route::get('get-data-states-datatables', [EstadoController::class, 'getData'])->name('get.states');

	/* operaciones -- operaciones */
	Route::resource('operaciones', OperacionController::class);
	Route::get('get-data-operaciones-datatables', [OperacionController::class, 'getData'])->name('get.operaciones');
	Route::post('operaciones/edit/hbl', [OperacionController::class, 'updateHBL'])->name('operaciones/edit/hbl');

	/*Agregar detalle de contenedor*/
	Route::get('operaciones/dcontainer/{idoperation}/{typeoperation}/{operaciondetail}',[OperacionController::class, 'createdetails'])->name('operaciones.dcontainer');
	Route::post('operaciones/addDetails', [OperacionController::class, 'addDetails'])->name('operaciones/addDetails');
	Route::get('get-data-columns', [OperacionController::class, 'getColumns'])->name('get.columns');
	Route::post('operaciones-puerto',[OperacionController::class, 'puertoSearch']);
	Route::get('autocomplete/fetch', [OperacionController::class, 'puertoSearch'])->name('autocomplete.fetch');
	Route::get('autocomplete/entidad', [OperacionController::class, 'entidadSearch'])->name('autocomplete.entidad');
	Route::post('operaciones-files',[OperacionController::class, 'files']);
	Route::get('operaciones/view/{file}',[OperacionController::class, 'viewFile'])->name('view.file');
	Route::post('operaciones/update/telex', [OperacionController::class, 'telex'])->name('operaciones/update/telex');
	Route::post('operaciones/restore', [OperacionController::class, 'restorePreOperation'])->name('operaciones/restore');


	/** operacion detalle contenedor  **/
	Route::post('operation/container/store',[OperationDetailsController::class, 'store'])->name('container.store');
	Route::post('operation/container/update',[OperationDetailsController::class, 'update'])->name('container.update');
	Route::post('operation/container/delete',[OperationDetailsController::class, 'destroy'])->name('container.delete');
	Route::get('get-data-container-details', [OperationDetailsController::class, 'getData'])->name('get.container.details');
	
	/** Busqueda por contenedor **/
	Route::get('operacion/contenedor/filter',[OperationDetailsController::class, 'search'])->name('operationdetail.filter');
	Route::get('get-containers', [OperationDetailsController::class, 'getContainer'])->name('get.containers');
	Route::get('get-data-container-filter', [OperationDetailsController::class, 'getDataDetails'])->name('get.container.filter');
	
	/** Retornar vacio contenedor **/
	Route::post('operation/container/emptyContainer',[OperationDetailsController::class, 'emptyContainer'])->name('container.emptyContainer');
	Route::post('operation/details/emptyContainer',[OperationDetailsController::class, 'emptyContainer'])->name('operationdetail.emptyContainer');
	Route::get('get-data-operacion-filter', [OperationDetailsController::class, 'getDataDetails'])->name('get.operacion.filter');

	/* cargos admin */
	Route::resource('cargos', CargoController::class);
	Route::get('get-data-cargos-datatables', [CargoController::class, 'getData'])->name('get.cargos');

	/* Portal Acceso a Clientes */
	Route::resource('customer', CustomerController::class);
	Route::get('get-data-customer-datatables', [CustomerController::class, 'getData'])->name('get.customer');
	Route::post('customer/delete', [CustomerController::class, 'delete'])->name('customer/delete');
	Route::post('customer/create', [CustomerController::class, 'add'])->name('customer/create');

});

