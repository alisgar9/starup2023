@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Contenedores<small>Catálogo de Contenedores</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Contenedores</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

  @if(session()->has('flash_message'))
  <div class="alert alert-success alert-dismissible fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Éxito!</h4>
    {{ session()->get('flash_message') }}
  </div>
  @endif

  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Contenedores</span>
          <span class="info-box-number">{{ $contenedores }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-check"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Contenedores Habilitados</span>
          <span class="info-box-number">{{ $contenedoresEnabled }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-trash"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Contenedores Eliminados</span>
          <span class="info-box-number">{{ $contenedoresDisabled }}</span>
          <a href="#" data-filter=".property-sale" class="click"><span>ver Eliminados <i class="fa fa-arrow-circle-right"></i></span></a>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Lista de Contenedores</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="areas-table" class="table table-striped table-bordered dt-responsive"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      @can('Crear')
      <a href="{{ URL::to('contenedores/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Agregar Contenedor</a>
      @endcan
    </div>
    <!-- /.col -->
  </div>

  <!-- Delete Model -->
  <div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Eliminar Contenedor</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Por favor, confirme que desea eliminar el contenedor: <b><span id="fav-title"></span></b>?
          <hr>
          <form action="{{ route('contenedores.destroy','1') }}" id="delForm" method="post">
            {{method_field('delete')}} {{csrf_field()}}
            <input name="idcontenedor" id="clave" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="submit" value="delete" class="btn btn-success">Aceptar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Delete Model -->
</section>

<script>
  $(document).ready(function () {
    // se activa cuando el modal está a punto de ser mostrado.
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      // obtener el atributo clave seleccionado.
      $("#fav-title").html($(e.relatedTarget).data('title'));
      var idEmpresa = $(e.relatedTarget).data('clave');
      modal.find('.modal-body #clave').val(idEmpresa);
    });
  });

  $(function () {
    $('.click').click(function () {
      $("#example").dataTable().fnDestroy();
      loadTable('0'); 
    });
  });
  
  $(function () {
    loadTable('1');
  });

  function loadTable(eliminados) {
    $('#areas-table').DataTable({
      processing: true,
      serverSide: true,
      destroy: true,
      type: 'GET',
      ajax: "{{ route('get.contenedores') }}?showDeleted="+eliminados,
      language: {
        "search": "Buscar",
        "lengthMenu": "Mostar _MENU_ registros por página",
        "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
        "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
        "infoEmpty": "Registros no encontrados",
        "infoFiltered": "(Filtrado en _MAX_ registros totales)",
        "paginate": {
          "previous": "Anterior",
          "next": "Siguiente",
        },
      },
      columns: [
        {data: 'DT_RowIndex', name: 'DT_RowIndex', title: '#'},
        { data: 'descripcion', name: 'descripcion', title: 'Descripción' },
        {
          data: 'activo', name: 'activo', title: 'Activo', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<div class="iradio_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Habilitada';
            } else {
              return '<div class="iradio_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Eliminada';
            }
          }
        },
        { data: 'created_at', name: 'created_at', title: 'Creada' },
        { data: 'updated_at', name: 'updated_at', title: 'Actualizada' },
        { data: 'action', name: 'action', title: 'Acciones', orderable: false, searchable: false }
      ]
    });
  }
</script>
</section>
@endsection
