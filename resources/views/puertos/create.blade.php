@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Puertos<small>Catálogo de Puertos</small></h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="{{ url('puertos') }}">Puertos</a></li>
        <li class="active">Agregar</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> ¡Alerta!</h5>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Nuevo Puerto</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {{ Form::open(array('url' => 'puertos')) }}
                <div class="box-body">
                        <div class="form-group">
                            {{ Form::label('puerto', 'Nombre de Puerto') }}
                            {!! Form::text('puerto', null, [
                            'class' => 'form-control',
                            'required' => 'required',
                            ]) !!}
                        </div>

                        <div class="form-group">
                                        {{ Form::label('pais', 'País') }}
                                        {{ Form::select('pais',$pais,null,[
                                        'class' => 'form-control','required' => 'required','placeholder' => 'seleccione uno...'],array('name'=>'cargos[]'))}}
                                        
                        </div>   
                        <div class="form-group">
                            {{ Form::label('clave_puerto', 'Clave de Puerto') }}
                            {!! Form::text('clave_puerto', null, [
                            'class' => 'form-control',
                            ]) !!}
                        </div>     

                </div>
                <div class="box-footer">
                    {{ Form::button('<i class="fa fa-save" aria-hidden="true"></i> Guardar', ['class' => 'btn btn-primary', 'type' => 'submit']) }}
                </div>
                {{ Form::close() }}
            </div>
            <!-- /.box -->

        </div>
        <!--/.col (left) -->

    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection
