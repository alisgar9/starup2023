@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Transit Time<small> Report</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Transit Time Report</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">
  
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Transit Time Report</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body text-center">
         
         @if(session()->has('message'))
            <div class="alert alert-success">{{ session()->get('message') }} </div>
         @endif 
            <form id="filtrar">
               <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-3">
                        <span class="input-group-addon"><i class="fa fa-users" aria-hidden="true"></i> Customer:</span>
                        <input id="consignee_desc" name="Scon" value="{{old('Scon')}}" type="text" class="form-control" placeholder="Search Customer" />
                        <input type="hidden" id="custom" name="custom" >
                    </div>
                    <div class="col-md-3">
                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i> Start ETA:</span>
                        <input type="date" class="form-control" name="etas" id="etas" autocomplete="off" required="required" >

                    </div>
                    <div class="col-md-3">
                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i> Final ETA:</span>
                        <input type="date" class="form-control" name="etast" id="etast" autocomplete="off" required="required" >
                    </div>

                    <div class="col-md-12 text-right">
                      <br><br>

                        <button type="button" id="btnFiltro" class="btn btn-info btn-flat"><i class="fa fa-paper-plane" aria-hidden="true"></i> Generate</button>

                        <button type="button" id="btnDownload" class="btn btn-warning btn-flat"><i class="fa fa-download" aria-hidden="true"></i> Download Report</button>

                    </div>

                    
                  <!-- /.col-lg-6 -->
               </div>
            </form>

        </div>
      </div>
    <!-- /.col -->
    </div>
    <div class="row" id="ddd"></div>

      <div class="row" id="resultado">
      
      </div>    


    </div>

 

  <div class="modal fade in" id="modal-danger" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">*Required Fields</h4>
              </div>
              <div class="modal-body">
                <h4 class="text-center">Custom, Start ETA and Stop ETA.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Try Again</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
  </section>

  @endsection
@yield('content')
@section('scripts')

<script type="text/javascript">


  
  $(document).ready( function() {

    function entidadSearch(tipo,etiqueta){
        $("#"+etiqueta).easyAutocomplete({
            url: function(search) {
                return "{{route('autocomplete.entidad')}}?tipo=consignee&search=" + search;
            },
        
            getValue: "nombre",
            list: {
                    onSelectItemEvent: function() {
                        var dataID = $("#"+etiqueta).getSelectedItemData().id;
                        $("#"+tipo).val(dataID);
                    },
            match: {
                     enabled: true
                    }
            }
        });
    }

    $('#consignee_desc').ready(function(){
        entidadSearch("custom", "consignee_desc");
    });


    $("button[id=btnDownload]").on('click',function () {
      var custom = $("#custom").val();
        var etas = $("#etas").val();
        var etast = $("#etast").val();
      if(custom.length > 0 && etas.length > 0 && etast.length > 0){ 

        window.location = "{{url('ttr/download')}}/" + custom + "/" + etas + "/" + etast;
      }else{
            $('#modal-danger').modal('show');
      }

    });
   




    $("#resultado").hide();  
    
    
    $("button[id=btnFiltro]").on('click',function () {
      $("#resultado").empty();
        var custom = $("#custom").val();
        var etas = $("#etas").val();
        var etast = $("#etast").val();
        if(custom.length > 0 && etas.length > 0 && etast.length > 0){ 
              $("#resultado").show();
              $("#resultado").append('<div class="col-md-12"><div class="box box-primary"><div class="box-header with-border"><h3 class="box-title"></h3></div><div class="box-body text-center"><table id="demorrage-table" class="table table-striped table-bordered dt-responsive"></table></div></div></div>');
              $('#demorrage-table').DataTable({
                processing: true,
                serverSide: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: "{{ url('generate/ttr') }}",
                    type: "POST",
                    data: function (d) {
                          d.custom = custom;
                          d.etas = etas;
                          d.etast = etast;
                          d._token = "{{ csrf_token() }}";
                    },
                },
                language: {
                  "search": "Buscar",
                  "lengthMenu": "Mostar _MENU_ registros por página",
                  "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
                  "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
                  "infoEmpty": "Registros no encontrados",
                  "infoFiltered": "(Filtrado en _MAX_ registros totales)",
                  "paginate": {
                    "previous": "Anterior",
                    "next": "Siguiente",
                  },
                },
                columns: [
                  { data: 'SUMEX', name: 'SUMEX', title: 'Folio' },
                  { data: 'container_number', name: 'container_number', title: '# Container' },
                  { data: 'shipper', name: 'shipper', title: 'Shipper' },
                  { data: 'ttr', name: 'ttr', title: 'Transit Time' },
                  { data: 'ETD', name: 'ETD', title: 'ETD' },
                  { data: 'ATD', name: 'ATD', title: 'ATD' },
                  { data: 'ETA', name: 'ETA', title: 'ETA' },
                  { data: 'ATA', name: 'ATA', title: 'ATA' },
                  { data: 'PO', name: 'PO', title: '# PO' },
                  { data: 'PI', name: 'PI', title: '# PI' },
                ]
              });

          }else{
            $('#modal-danger').modal('show');
          }

    });    
});



</script>
@stop
@yield('scripts')