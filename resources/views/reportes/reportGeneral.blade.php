@extends('layouts.app', ['activePage' => 'report-general', 'titlePage' => __('')])

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>General<small> Report</small></h1>
  <ul class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i>  Home </a></li>
    <li class="active">General Report</li>
  </ul>
</section>

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('General Report') }}</h4>
                <p class="card-category">{{ __('*All fields are required.') }}</p>
              </div>
              <div class="card-body ">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Opps!</strong> Something went wrong, check the errors below.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                  {!! Form::open(array('route' => 'get.download','method'=>'POST')) !!}
                    <div class="col-md-4">
                                    <span class="input-group-addon font-weight-bold"><i class="fa fa-users" aria-hidden="true"></i> Customer:</span>
                                    {{ Form::select('custom',$consignee,null,[
                                        'class' => 'form-control','placeholder' => 'Choose one...', 'required' => 'required', 'id' => 'custom'],array('name'=>'consignee[]'))}}
                    </div>
                    <div class="col-md-4">
                                    <span class="input-group-addon font-weight-bold"><i class="fa fa-calendar" aria-hidden="true"></i> Start ETA:</span>
                                    <input type="date" class="form-control" name="etas" id="etas" autocomplete="off" required="required" >
                    </div>
                    <div class="col-md-4">
                                    <span class="input-group-addon font-weight-bold"><i class="fa fa-calendar" aria-hidden="true"></i> Final ETA:</span>
                                    <input type="date" class="form-control" name="etast" id="etast" autocomplete="off" required="required" >
                    </div>
                    <button type="button" id="btnFiltro" class="btn btn-success btn-flat"><i class="fa fa-paper-plane" aria-hidden="true"></i> Generate</button>
                    <button type="submit" class="btn btn-info btn-flat"><i class="fa fa-download" aria-hidden="true"></i> Download Report</button>                      
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>
        <div class="col-md-12">
        <div class="row" id="resultado"></div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade in" id="modal-danger" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">*Required Fields</h4>
              </div>
              <div class="modal-body">
                <h4 class="text-center">Custom, Start ETA and Stop ETA.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Try Again</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
  </div>

  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>

<script type="text/javascript">
$(document).ready( function() {

  $("button[id=btnFiltro]").on('click',function () {
    $("#resultado").empty();
      var custom = $("#custom").val();
      var etas = $("#etas").val();
      var etast = $("#etast").val();
      if(custom.length > 0 && etas.length > 0 && etast.length > 0){ 
            $("#resultado").show();
            $("#resultado").append('<div class="col-md-12"><div class="box box-primary"><div class="box-header with-border"><h3 class="box-title"></h3></div><div class="box-body pre-scrollable text-center"><table id="demorrage-table" class="table table-striped table-bordered dt-responsive"></table></div></div></div>');
            $('#demorrage-table').DataTable({
              processing: true,
              serverSide: true,
              buttons: [
                  'copy', 'csv', 'excel', 'pdf', 'print'
              ],
              ajax: {
                  url: "{{ url('generate/general') }}",
                  type: "POST",
                  data: function (d) {
                        d.custom = custom;
                        d.etas = etas;
                        d.etast = etast;
                        d._token = "{{ csrf_token() }}";
                  },
              },
              language: {
                "search": "Buscar",
                "lengthMenu": "Mostar _MENU_ registros por página",
                "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
                "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
                "infoEmpty": "Registros no encontrados",
                "infoFiltered": "(Filtrado en _MAX_ registros totales)",
                "paginate": {
                  "previous": "Anterior",
                  "next": "Siguiente",
                },
              },
              columns: [
                { data: 'SUMEX', name: 'SUMEX', title: 'Folio' },
                { data: 'PI', name: 'PI', title: '# PI' },
                { data: 'PO', name: 'PO', title: '# PO' },
                { data: 'factura', name: 'factura', title: '# Invoice' },
                { data: 'hbl', name: 'hbl', title: 'HBL' },
                { data: 'mbl', name: 'mbl', title: 'MBL' },
                { data: 'ETD', name: 'ETD', title: 'ETD' },
                { data: 'ATD', name: 'ATD', title: 'ATD' },
                { data: 'ETA', name: 'ETA', title: 'ETA' },
                { data: 'ATA', name: 'ATA', title: 'ATA' },
                { data: 'container_number', name: 'container_number', title: '# Container' },
                { data: 'shipper', name: 'shipper', title: 'Shipper' },
                { data: 'POL', name: 'POL', title: 'POL' },
                { data: 'POD', name: 'POD', title: 'POD' },
                { data: 'terminal', name: 'terminal', title: 'Terminal' },
                { data: 'telex', name: 'telex', title: 'Telex' },
                { data: 'free_demurrages_days_cust', name: 'free_demurrages_days_cust', title: 'Free Days' },
                { data: 'comments', name: 'comments', title: 'Comments' },
              ]
            });

        }else{
          $('#modal-danger').modal('show');
        }

  });    
});



</script>
@endsection
