@extends('layouts.app', ['activePage' => 'roles', 'titlePage' => __('Editar Rol')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">

    

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Editar Rol de Usuario') }}</h4>
                <p class="card-category">{{ __('*Todos los campos son requeridos.') }}</p>
              </div>
              <div class="card-body ">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Opps!</strong> Algo salió mal, verifique los errores a continuación.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::model($role, ['route' => ['roles.update', encrypt($role->id)],'method' => 'PATCH']) !!}

                    <div class="form-group">
                        <strong>*Nombre:</strong>
                        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control', 'required' => 'required')) !!}
                    </div>
                    <div class="form-group">
                        <strong>*Permisos:</strong>
                        <br/>
                        @foreach($permission as $value)
                            <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                            {{ $value->name }}</label>
                        <br/>
                        @endforeach
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                {!! Form::close() !!}
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection




