@extends('layouts.app', ['activePage' => 'roles', 'titlePage' => __('Roles de Usuario')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Catálogo de Roles de Usuario</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            @can('role-create')
                    <span class="float-right">
                        <a class="btn btn-primary" href="{{ route('roles.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo Rol de Usuario</a>
                    </span>
            @endcan
            <table id="roles-table" class="table table-hover table-striped table-bordered dt-responsive"></table>
            
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Delete Model -->
<div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <h4 class="modal-title" id="mySmallModalLabel">Eliminar Rol de Usuario</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Por favor, confirme que desea eliminar el rol de usuario: <b><span id="fav-title"></span></b>?
          <hr>
          @can('role-delete')
            {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', 0],'style'=>'display:inline']) !!}
            <input name="id_rol" id="id_rol" type="hidden" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="submit" value="delete" class="btn btn-success">Aceptar</button>
            {!! Form::close() !!}
          @endcan
        </div>
      </div>
    </div>
  </div>
  <!-- /.Delete Model -->

  <!-- Restaurar Model -->
  <div id="restore" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <h4 class="modal-title" id="mySmallModalLabel">Restaurar Rol de Usuario </h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Por favor, confirme que desea restaurar Rol de Usuario: <b><span id="favs-title"></span></b>?
          <hr>
          {!! Form::open(['method' => 'POST','route' => ['roles/restore'],'style'=>'display:inline']) !!}
            {{csrf_field()}}
            <input name="id_rol" id="id_rol" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="submit" value="delete" class="btn btn-success">Aceptar</button>
            {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
  <!-- /.Restaurar Model -->


<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script>
  $( document ).ready(function() {
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      $("#fav-title").html($(e.relatedTarget).data('title'));
      var id_rol = $(e.relatedTarget).data('idrol');
      modal.find('.modal-body #id_rol').val(id_rol);
    });

    $('#restore').on('show.bs.modal', function (e) {
      var modal = $(this);
      $("#favs-title").html($(e.relatedTarget).data('tit'));
      var id_rol = $(e.relatedTarget).data('idrol');      
      modal.find('.modal-body #id_rol').val(id_rol);
    });

  });

  $(function () {
    $('#roles-table').DataTable({
      processing: true,
      serverSide: true,
      type: 'GET',
      ajax: "{{ route('get.roles') }}",
      language: {
        "search": "Buscar",
        "lengthMenu": "Mostar _MENU_ registros por página",
        "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
        "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
        "infoEmpty": "Registros no encontrados",
        "infoFiltered": "(Filtrado en _MAX_ registros totales)",
        "paginate": {
          "previous": "Anterior",
          "next": "Siguiente",
        },
      },
      columns: [
        { data: 'id', name: 'id', title: '#' },
        { data: 'name', name: 'name', title: 'Nombre' },
        {
          data: 'status', name: 'status', title: 'Estado', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<div class="iradio_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Habilitada';
            } else {
              return '<div class="iradio_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Eliminada';
            }
          }
        },
        { data: 'created_at', name: 'created_at', title: 'Creada' },
        { data: 'updated_at', name: 'updated_at', title: 'Actualizada' },
        { data: 'action', name: 'action', title: 'Acciones', orderable: false, searchable: false }
      ]
    });
  });
</script>
@endsection
