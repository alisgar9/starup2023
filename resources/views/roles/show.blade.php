@extends('layouts.app', ['activePage' => 'roles', 'titlePage' => __('Rol de Usuario')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">

    

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Rol de Usuario') }}</h4>
              </div>
              <div class="card-body ">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Opps!</strong> Algo salió mal, verifique los errores a continuación.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                    <div class="form-group">
                        <strong>*Nombre:</strong>
                        {!! Form::text('name',$role->name, array('placeholder' => 'Name','class' => 'form-control', 'required' => 'required')) !!}
                    </div>
                    <div class="form-group">
                        <strong>*Permisos:</strong>
                        <br/>
                        @if(!empty($rolePermissions))
                            @foreach($rolePermissions as $permission)
                                <label class="badge badge-success">{{ $permission->name }}</label>
                            @endforeach
                        @endif
                    </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection


