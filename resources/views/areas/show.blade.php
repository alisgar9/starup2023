@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Áreas<small>Catálogo de áreas</small></h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="{{ url('areas') }}">Áreas</a></li>
        <li class="active">Detalle</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-map-marker"></i> {{ $area->nombre }}
            <small class="pull-right">Fecha: {{ \Carbon\Carbon::parse($user->from_date)->format('d/m/Y')}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            Municipio
          <address>
            <strong>{{ $area->nombre }}</strong><br>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
        Cabecera Municipa
          <address>
            <strong>{{ $area->descripcion }}</strong><br>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Clave #{{ $area->id }}</b><br>
          <br>
          <b>Fecha de Alta:</b> {{ $area->created_at }}<br>
          <b>Fecha de Actualización:</b> {{ $area->updated_at }}<br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      </br>
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="#"  class="btn btn-info" onClick="window.print()"><i class="fa fa-print"></i> Imprimir</a>
        </div>
      </div>
    </section>

<!-- Main content -->
<section class="content">




</section>
@endsection