@extends('layouts.app', ['activePage' => 'users', 'titlePage' => __('Editar Usuario')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">

    

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Editar Usuario') }}</h4>
                <p class="card-category">{{ __('*Todos los campos son requeridos.') }}</p>
              </div>
              <div class="card-body ">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Opps!</strong> Algo salió mal, verifique los errores a continuación.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::model($user, ['route' => ['users.update', encrypt($user->id)], 'method'=>'PATCH']) !!}

                    <div class="form-group">
                        <strong>*Nombre:</strong>
                        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control', 'onkeyup' => 'javascript:this.value=this.value.toUpperCase()')) !!}
                    </div>
                    <div class="form-group">
                        <strong>*Email:</strong>
                        {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        <strong>Contraseña:</strong>
                        {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        <strong>Confirma la Contraseña:</strong>
                        {!! Form::password('password_confirmation', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        <strong>Role:</strong>
                        <br/>
                        
                        {!! Form::select('roles[]', $roles, $userRole, array('class' => 'form-control','multiple')) !!}

                        
                    </div>
                    <button type="submit" class="btn btn-primary">Agregar</button>
                {!! Form::close() !!}
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script src = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>
  <script>
        $( document ).ready(function() {

            //alert('alicia');
            $('#mltislct').multiselect();
        });
  </script>

@endsection

