@extends('layouts.app', ['activePage' => 'users', 'titlePage' => __('Usuario')])

@section('content')
<div class="content">
  <div class="container-fluid">
  <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
              <div class="card-icon">
                <i class="material-icons">peopleoutline</i>
              </div>
              <p class="card-category">Usuarios Totales</p>
              <h3 class="card-title">{{count($dataTotal)}}
              </h3>
            </div>
            <div class="card-footer">
              <div class="stats">
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i class="material-icons">peopleoutline</i>
              </div>
              <p class="card-category">Usuarios Activos</p>
              <h3 class="card-title">{{count($dataActive)}}</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
              <div class="card-icon">
                <i class="material-icons">peopleoutline</i>
              </div>
              <p class="card-category">Permisos Eliminados</p>
              <h3 class="card-title">{{count($dataInactive)}}</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
         
        </div>
    </div>

  </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Catálogo de de Usuarios</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            @can('role-create')
                    <span class="float-right">
                        <a class="btn btn-primary" href="{{ route('users.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo Usuario</a>
                    </span>
            @endcan
            <table id="users-table" class="table table-hover table-striped table-bordered dt-responsive"></table>
            
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Delete Model -->
<div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <h4 class="modal-title" id="mySmallModalLabel">Inhabilitar Usuario</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Por favor, confirme que desea inhabilitar el usuario: <b><span id="fav-title"></span></b>?
          <hr>
          @can('role-delete')
            {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', 0],'style'=>'display:inline']) !!}
            <input name="id_user" id="id_user" type="hidden" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="submit" value="delete" class="btn btn-success">Aceptar</button>
            {!! Form::close() !!}
          @endcan
        </div>
      </div>
    </div>
  </div>
  <!-- /.Delete Model -->

  <!-- Restaurar Model -->
  <div id="restore" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <h4 class="modal-title" id="mySmallModalLabel">Habilitar Usuario </h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Por favor, confirme que desea habilitar el Usuario: <b><span id="favs-title"></span></b>?
          <hr>
          {!! Form::open(['method' => 'POST','route' => ['users/restore'],'style'=>'display:inline']) !!}
            {{csrf_field()}}
            <input name="id_user" id="id_user" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="submit" value="delete" class="btn btn-success">Aceptar</button>
            {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
  <!-- /.Restaurar Model -->


<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script>
  $( document ).ready(function() {
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      $("#fav-title").html($(e.relatedTarget).data('title'));
      var id_user = $(e.relatedTarget).data('id');
      modal.find('.modal-body #id_user').val(id_user);
    });

    $('#restore').on('show.bs.modal', function (e) {
      var modal = $(this);
      $("#favs-title").html($(e.relatedTarget).data('tit'));
      var id_user = $(e.relatedTarget).data('id');      
      modal.find('.modal-body #id_user').val(id_user);
    });

  });

  $(function () {
    $('#users-table').DataTable({
      processing: true,
      serverSide: true,
      type: 'GET',
      ajax: "{{ route('get.users') }}",
      language: {
        "search": "Buscar",
        "lengthMenu": "Mostar _MENU_ registros por página",
        "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
        "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
        "infoEmpty": "Registros no encontrados",
        "infoFiltered": "(Filtrado en _MAX_ registros totales)",
        "paginate": {
          "previous": "Anterior",
          "next": "Siguiente",
        },
      },
      columns: [
        { data: 'id', name: 'id', title: '#' },
        { data: 'name', name: 'name', title: 'Nombre' },
        { data: 'email', name: 'email', title: 'Email' },
        {
          data: 'approved', name: 'approved', title: 'Estado', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<button class="btn btn-success btn-round"> <i class="fa fa-check" aria-hidden="true"></i> Activos</button>';
            } else {
              return '<button class="btn btn-danger btn-round"> <i class="fa fa-times-circle" aria-hidden="true"></i> Inactivo</button>';
            }
          }
        },
        { data: 'created_at', name: 'created_at', title: 'Creada' },
        { data: 'updated_at', name: 'updated_at', title: 'Actualizada' },
        { data: 'action', name: 'action', title: 'Acciones', orderable: false, searchable: false }
      ]
    });
  });
</script>
@endsection

