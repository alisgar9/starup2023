@extends('layouts.app', ['activePage' => 'users', 'titlePage' => __('Ver Usuario')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">

    

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Ver Usuario '.$user->name)  }}</h4>
              </div>
              <div class="card-body ">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Opps!</strong> Algo salió mal, verifique los errores a continuación.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                    <div class="form-group">
                        <strong>*Nombre:</strong>
                        {!! Form::text('name', $user->name, array('placeholder' => 'Name','class' => 'form-control', 'readonly' =>'readonly')) !!}
                    </div>
                    <div class="form-group">
                        <strong>*Email:</strong>
                        {!! Form::text('email', $user->email, array('placeholder' => 'Email','class' => 'form-control', 'readonly' =>'readonly')) !!}
                    </div>
                    <div class="form-group">
                        <strong>Role:</strong>
                        <br/>
                        
                        {!! Form::select('roles[]', $roles, $userRole, array('class' => 'form-control','multiple', 'readonly' =>'readonly')) !!}

                        
                    </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script src = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>
  <script>
        $( document ).ready(function() {

            //alert('alicia');
            $('#mltislct').multiselect();
        });
  </script>

@endsection



