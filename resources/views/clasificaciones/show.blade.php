@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Clasificaciones<small>Catálogo de Clasificaciones de Entidad</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{ url('clasificaciones') }}">Clasificaciones de Entidad</a></li>
    <li class="active">Detalle</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="fa fa-user"></i> {{ $clasificacion->descripcion }}
        <small class="pull-right">Fecha: {{ \Carbon\Carbon::parse($user->from_date)->format('d/m/Y')}}</small>
      </h2>
    </div>
    <!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      Nombre:
      <address>
        <strong>{{ $clasificacion->descripcion }}</strong><br>
      </address>
    </div>
    
    <div class="col-sm-4 invoice-col">
      <b>Clave #{{ $clasificacion->id }}</b><br>
      <br>
      <b>Fecha de Alta:</b> {{ $clasificacion->created_at }}<br>
      <b>Fecha de Actualización:</b> {{ $clasificacion->updated_at }}<br>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  
  </br>
  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="#" class="btn btn-info" onClick="window.print()"><i class="fa fa-print"></i> Imprimir</a>
    </div>
  </div>
</section>

<!-- Main content -->
<section class="content">




</section>
@endsection
