@extends('adminlte::layouts.app')
<link href="{{ URL::asset('/css/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Agregar Entidad<small>Control de Entidades</small></h1>
     <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="{{ url('entidades') }}">Entidades</a></li>
        <li class="active">Agregar</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> ¡Alerta!</h5>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
    </div>
    @endif


      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Nueva Entidad</h3>
              <p>Los campos marcados con <b>*</b> son requeridos</p>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {{ Form::open(array('url' => 'entidades')) }}
 <div class="box-body">
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('nombre', 'Nombre(Alias) *') }}
            {!! Form::text('nombre', null, [
                    'class'                         => 'form-control',
                    'required'                      => 'required',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('razonsocial', 'Razón Social *') }}
            {!! Form::text('razonsocial', null, [
                    'class'                         => 'form-control',
                    'required'                      => 'required',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('nombrecontacto', 'Nombre de Contacto ') }}
            {!! Form::text('nombrecontacto', null, [
                    'class'                         => 'form-control',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('email', 'Correo Electrónico * ') }}
            {{ Form::email('email', '', array('class' => 'form-control', 'requerid' => 'requerid')) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('email', 'Correos Electrónicos para Con Copia') }}
            {!! Form::text('email_cc',old('email_cc'), [
                                'data-role' => 'tagsinput',
                                'class' => 'form-control'
                                ]) !!}
            
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('direccion', 'Dirección') }}
            {!! Form::text('direccion', null, [
                    'class'                         => 'form-control',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('telefono', 'Teléfono ') }}
            {!! Form::text('telefono', null, [
                    'class'                         => 'form-control',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('taxid', 'Tax ID') }}
            {!! Form::text('taxid', null, [
                    'class'                         => 'form-control',
                    ]) !!}
        </div>
    </div>

    <div class="col-md-6">
                    <div class="form-group">
                    {{ Form::label('id_clasificacion', 'Seleccione tipos de Clasificación *') }}<br>
                        {{ Form::select('id_clasificacion[]',$clasificaciones,null,[
                                'multiple' =>'multiple','requerid' => 'requerid', 'id' =>'demo'],array('name'=>'id_clasificacion[]'))}}
                                
                        
                    </div>        
    </div> 
    <div class="col-md-12">
              <h3 class="box-title">Detalle Bancario </h3>
              <hr class="hr-primary" />
    </div> 
    <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('cuentabancaria', 'Cuenta Bancaria') }}
                        {!! Form::text('cuentabancaria', null, [
                        'class' => 'form-control',]) !!}
                    </div>
    </div>
    <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('cinterbancaria', 'Clabe Interbancaria') }}
                        {!! Form::text('cinterbancaria', null, [
                        'class' => 'form-control', 'size' => '18', 'maxlength' => '18']) !!}
                    </div>
    </div> 
    <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('id_banco', 'Banco') }}
                        {{ Form::select('id_banco',$banco,null,[
                        'class' => 'form-control','placeholder' => 'seleccione uno...'],array('name'=>'banco[]'))}}
                    </div>
    </div> 
    <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('id_banco_inter', 'Banco intermediario') }}
                        {{ Form::select('id_banco_inter',$banco,null,[
                        'class' => 'form-control','placeholder' => 'seleccione uno...'],array('name'=>'banco[]'))}}
                    </div>
    </div>
    <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('swift', 'Swift') }}
                        {!! Form::text('swift', null, [
                        'class' => 'form-control', 'size' => '11', 'maxlength' => '11']) !!}
                    </div>
    </div>

    
    
      </div>

 <div class="box-footer">
    {{ Form::button('<i class="fa fa-save" aria-hidden="true"></i> Guardar', ['class' => 'btn btn-primary', 'type' => 'submit']) }}

    </div>

    {{ Form::close() }}

            
          </div>
          <!-- /.box -->

        

        


        </div>
        <!--/.col (left) -->
       
      </div>
      <!-- /.row -->
</section>
<!-- /.content -->


@endsection
@yield('content')
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-angular.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('#demo').multiselect();
    });
</script> 

   
@stop
@yield('scripts')
