@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Empresas<small>Catálogo de Empresas</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Empresas</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">
  @if(session()->has('flash_message'))
  <div class="custom-alerts alert alert-success fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Éxito!</h4>
    {{ session()->get('flash_message') }}
  </div>
  @endif

  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-building-o"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Empresas</span>
          <span class="info-box-number">{{ count($empresas) }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-upload"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Alta Empresa XML</span>
          <span class="info-box-number">{{ count($countXML) }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-pencil"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Alta Empresa Manual</span>
          <span class="info-box-number">{{ count($countManual) }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Lista de Empresas</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="empresas-table" class="table table-striped table-bordered dt-responsive"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      @can('Crear')
      <a href="{{ URL::to('empresas/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Agregar Empresa</a>
      @endcan
    </div>
    <!-- /.col -->
  </div>

  <!-- Delete Model -->
  <div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Eliminar Empresa</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Por favor, confirme que desea eliminar la empresa: <b><span id="fav-title"></span></b>?
          <hr>
          <form action="{{ route('empresas.destroy','test') }}" id="delForm" method="post">
            {{method_field('delete')}} {{csrf_field()}}
            <input name="id_empresa" id="id_emp" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="submit" value="delete" class="btn btn-success">Aceptar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Delete Model -->
</section>

<script>
  $(document).ready(function () {
    // se activa cuando el modal está a punto de ser mostrado.
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      // obtener el atributo idEmpresa del elemento pulsado.
      $("#fav-title").html($(e.relatedTarget).data('title'));
      var idEmpresa = $(e.relatedTarget).data('idemp');
      modal.find('.modal-body #id_emp').val(idEmpresa);
    });
  });

  $(function () {
    $('#empresas-table').DataTable({
      processing: true,
      serverSide: true,
      type: 'GET',
      ajax: "{{ route('get.empresas') }}",
      language: {
        "search": "Buscar",
        "lengthMenu": "Mostar _MENU_ registros por página",
        "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
        "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
        "infoEmpty": "Registros no encontrados",
        "infoFiltered": "(Filtrado en _MAX_ registros totales)",
        "paginate": {
          "previous": "Anterior",
          "next": "Siguiente",
        },
      },
      columns: [
        { data: 'id', name: 'id', title: '#' },
        { data: 'empresa', name: 'empresa', title: 'Empresa' },
        {
          data: 'alta', name: 'alta', title: 'Tipo Alta', "render": function (data, type, full, meta) {
            return '<span class="pull-center badge bg-blue">'+data+'</span>';
          }
        },
        {
          data: 'estado', name: 'estado', title: 'Estado', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<div class="iradio_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Habilitado';
            } else {
              return '<div class="iradio_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Eliminado';
            }
          }
        },
        { data: 'created_at', name: 'created_at', title: 'Creada' },
        { data: 'updated_at', name: 'updated_at', title: 'Actualizada' },
        { data: 'action', name: 'action', title: 'Acciones', orderable: false, searchable: false }
      ]
    });
  });
</script>
@endsection