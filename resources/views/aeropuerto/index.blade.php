@extends('layouts.app', ['activePage' => 'airports', 'titlePage' => __('Airports')])

@section('content')


<div class="content">
  <div class="container-fluid">
  <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
               <i class="fa-solid fa-plane-up"></i>
              </div>
              <p class="card-category">Airports</p>
              <h3 class="card-title">{{$aeropuertos}}
              </h3>
            </div>
            <div class="card-footer">
              <div class="stats">
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i class="fa-solid fa-plane-up"></i>
              </div>
              <p class="card-category">Enabled Airports</p>
              <h3 class="card-title">{{$aeropuertosEnabled}}</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
              <div class="card-icon">
                <i class="fa-solid fa-plane-up"></i>
              </div>
              <p class="card-category">Eliminated Airports</p>
              
              <h3 class="card-title">{{$aeropuertosDisabled}}
              <span class="text-success text-sm font-weight-bolder"><a href="#" data-filter=".property-sale" class="click text-success">See Deleted </a></span></h3>
            </div>
            <div class="card-footer">
              <div class="stats">
              </div>
            </div>
          </div>
        </div>
    </div>

  </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">List of Airports</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p><i class="fa fa-check text-white" aria-hidden="true"></i> {{ \Session::get('success') }}</p>
                </div>
            @endif
            @can('role-create')
                    <span class="float-right">
                        <a href="{{ URL::to('airports/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Airport</a>
                    </span>
            @endcan
              <table id="areas-table" class="table table-striped table-bordered dt-responsive"></table>
            
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <!-- Delete Model -->
  <div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Delete Airport</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
        ¿Please confirm that you want to delete the Airport: <b><span id="fav-title"></span></b>?
          <hr>
          <form action="{{ route('airports.destroy','1') }}" id="delForm" method="post">
            {{method_field('delete')}} {{csrf_field()}}
            <input name="idstate" id="state" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" value="delete" class="btn btn-success">Accept</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Delete Model -->
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script>
  $(document).ready(function () {
    // se activa cuando el modal está a punto de ser mostrado.
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      // obtener el atributo idEmpresa del elemento pulsado.
      $("#fav-title").html($(e.relatedTarget).data('title'));
      var idEmpresa = $(e.relatedTarget).data('puerto');
      modal.find('.modal-body #state').val(idEmpresa);
    });
  });

  $(function () {
    $('.click').click(function () {
      $("#example").dataTable().fnDestroy();
      loadTable('0'); 
    });
  });
  
  $(function () {
    loadTable('1');
  });

  function loadTable(eliminados) {
    $('#areas-table').DataTable({
      processing: true,
      serverSide: true,
      destroy: true,
      type: 'GET',
      ajax: "{{ route('get.airports') }}?showDeleted="+eliminados,
      language: {
        "search": "Search",
        "lengthMenu": "Show _MENU_ per page",
        "zeroRecords": "Sorry, we can't find what you're looking for",
        "info": "Showing page _PAGE_ of _PAGES_ of _TOTAL_ records",
        "infoEmpty": "Records not found",
        "infoFiltered": "(Filtered in _MAX_ total records)",
        "paginate": {
          "previous": "Previous",
          "next": "Next",
        },
      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex', title: '#'},
        { data: 'nombre', name: 'nombre', title: 'Name' },
        { data: 'paisDesc', name: 'paisDesc', title: 'Country' },
        {
          data: 'activo', name: 'activo', title: 'Active', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<div class="iradio_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Enabled';
            } else {
              return '<div class="iradio_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Remove';
            }
          }
        },
        { data: 'created_at', name: 'created_at', title: 'Created' },
        { data: 'updated_at', name: 'updated_at', title: 'Updated' },
        { data: 'action', name: 'action', title: 'Actions', orderable: false, searchable: false }
      ]
    });
  }
</script>
@endsection
