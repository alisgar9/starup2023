@extends('layouts.app', ['activePage' => 'airports', 'titlePage' => __('Add Airport')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-8 ml-auto mr-auto">

    

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Add Airport') }}</h4>
                <p class="card-category">{{ __('*All fields are required.') }}</p>
              </div>
              <div class="card-body ">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Opps!</strong> Something went wrong, check the errors below.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{ Form::open(array('url' => 'airports')) }}
                  <div class="box-body">
                          <div class="form-group">
                              {{ Form::label('nombre', 'State Name *') }}
                              {!! Form::text('nombre', null, [
                              'class' => 'form-control',
                              'required' => 'required',
                              ]) !!}
                          </div>

                          <div class="form-group">
                                          {{ Form::label('pais', 'Country *') }}
                                          {{ Form::select('pais',$pais,null,[
                                          'class' => 'form-control','required' => 'required','placeholder' => 'seleccione uno...'],array('name'=>'cargos[]'))}}
                                          
                          </div>   
                          <div class="form-group">
                              {{ Form::label('clave', 'State key') }}
                              {!! Form::text('clave', null, [
                              'class' => 'form-control',
                              ]) !!}
                          </div>     

                  </div>
                  <div class="box-footer">
                      {{ Form::button('<i class="fa fa-save" aria-hidden="true"></i> Save', ['class' => 'btn btn-primary', 'type' => 'submit']) }}
                  </div>
                {{ Form::close() }}
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
<script>
  
  
</script>
@endsection






