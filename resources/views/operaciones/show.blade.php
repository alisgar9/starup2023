@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Operations<small>Operations Registry</small></h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('operaciones') }}">Operations</a></li>
        <li class="active">View</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> House (HBL) {{$operacion->hbl}}</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            {{ Form::label('operacion', 'Operation Type') }}<br />
                            <i class="fa fa-check" aria-hidden="true"></i>
                            @if ($operacion->operacion === 1)
                                Import
                            @elseif ($operacion->operacion === 2)
                                Export 
                            @elseif ($operacion->operacion === 3)
                                Domestic
                            @else
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Not Apply 
                            @endif
                            <br>
                        </div>    

                     </div>
                    <div class="col-md-12">
                        <div class="form-group">
                        <div class="col-md-6">
                            {{ Form::label('tipo_operacion', 'Operation ') }}  <br>  
                            <input type="radio" checked class="form-radio">
                            <label> 
                                @if ($operacion->tipo_operacion === 1)
                                    <i class="fa fa-ship" aria-hidden="true"></i> Ocean
                                @elseif ($operacion->tipo_operacion === 2)
                                    <i class="fa fa-plane" aria-hidden="true"></i> Air
                                @elseif ($operacion->tipo_operacion === 3)    
                                    <i class="fa fa-bus" aria-hidden="true"></i> In land
                                @else
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Not Apply 
                                @endif
                            </label>
                            </div>
                            <div class="col-md-6">
                            {{ Form::label('hbl', 'Operation Profile') }}   <br> 
                            <input type="radio" checked class="form-radio">
                            <label> 
                                @if ($operacion->tipo_operacion === 1)
                                    @if ($operacion->operacion_detail === 1)
                                        LCL
                                    @elseif ($operacion->operacion_detail === 2)  
                                        FCL
                                    @else    
                                        Not Apply
                                    @endif    
                                @elseif ($operacion->tipo_operacion === 3)    
                                    @if ($operacion->operacion_detail === 1)
                                        Nacional
                                    @elseif ($operacion->operacion_detail === 2)  
                                      Internacional
                                    @else
                                      Not Apply
                                    @endif  

                                @else
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Not Apply 
                                @endif
                            </label>
                            </div>
                            
                            
                        </div>
                    </div>

                    <div class="col-md-12">
                            <div class="col-md-12"><br>
                                {{ Form::label('cy', 'Container Yard') }}
                            </div>    
                            <div class="form-group">
                            @if ($operacion->tipo_operacion === 1) 
                                <div class="col-md-4">    
                                <input type="radio" name="cy" readonly id="cycy" {{$operacion->cy ==1 ? 'checked='.'"'.'checked'.'"' : '' }} class="form-radio"> <label for="cycy">CY - CY</label>
                                </div>
                            @elseif ($operacion->tipo_operacion === 2)    
                                <div class="col-md-4"> 
                                <input type="radio" name="cy" readonly id="cydoor" {{$operacion->cy ==2 ? 'checked='.'"'.'checked'.'"' : '' }} class="form-radio"> <label for="cydoor">CY - Door </label>
                                </div>

                                <div class="col-md-4" id="div-cydoor"> 

                                    @if($operacion->cy == 2)
                                    <div class="col-md-12">
                                        {{ Form::label("cdt", "CY Door Type ") }}
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                        @if($operacion->cy_door == 1)
                                            All Truck
                                        @elseif($operacion->cy_door == 2)
                                            Train - Truck
                                        @elseif($operacion->cy_door == 3)
                                            Train + Ramp
                                        @else
                                
                                        @endif


                                            
                                        </div>
                                    </div>
                                    @endif

                                </div>
                            @else
                                
                            @endif
                            <div class="col-md-12">
                                    <div class="col-md-3">
                                <i class="fa fa-file" aria-hidden="true"></i>
                                    {{ Form::label('po', 'PO') }}<br>
                                    {{json_decode($operacion->PO)}}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::label('pi', 'PI') }}<br>
                                    {{json_decode($operacion->PI)}}
                                </div>
                                <div class="col-md-2">
                                    {{ Form::label('invoice', '# Invoice') }}<br>
                                    {{$operacion->factura}}
                                </div>
                                <div class="col-md-2">
                                    {{ Form::label('SC', '# SC') }}<br>
                                    {{$operacion->sc}}
                                </div>
                                <div class="col-md-2">
                                    {{ Form::label('telex', 'Telex') }}<br>
                                    {{$operacion->telex}}
                                </div>
                            </div>

                                <br>
                            </div>
                        </div>
                    <div class="col-md-12">
                        <hr size="30">
                    </div>
                <div class="col-md-12">            
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>House (HBL)</label><br>
                            {{$operacion->hbl}}
                        </div>
                    </div>   
                    <div class="col-md-3">    
                        <div class="form-group">
                            <label>Master (MBL)</label><br>
                            {{$operacion->mbl}}
                        </div>     
                    </div>
                    @if ($operacion->tipo_operacion === 1) 
                    <div class="col-md-3">    
                        <div class="form-group">
                            <label>House Release Mode</label><br>
                            {{isset($operacion->libhblDesc->descripcion)?$operacion->libhblDesc->descripcion:null}}
                        </div>     
                    </div>

                    <div class="col-md-3">    
                        <div class="form-group">
                            <label>Master Release Mode</label><br>
                            {{isset($operacion->libmblDesc->descripcion)?$operacion->libmblDesc->descripcion:null}}
                        </div>     
                    </div> 
                    @endif
                </div>
                <div class="col-md-12">    
                    <div class="col-md-3">    
                        <div class="form-group">
                            <label>ETD</label><br>
                            {{date('Y-m-d', strtotime($operacion->ETD))}}
                        </div>     
                    </div>

                    <div class="col-md-3">    
                        <div class="form-group">
                            <label>ETA</label><br>
                            {{date('Y-m-d', strtotime($operacion->ETA))}}
                        </div>     
                    </div>
                    <div class="col-md-3">    
                        <div class="form-group">
                            {{ Form::label('ATD', 'ATD *') }}<br>
                            {{date('Y-m-d', strtotime($operacion->ATD))}}
                        </div>
                    </div>
                    <div class="col-md-3">    
                        <div class="form-group">
                            {{ Form::label('ATA', 'ATA *') }}<br>
                            {{date('Y-m-d', strtotime($operacion->ATA))}}
                        </div>     
                    </div> 
                    @if ($operacion->tipo_operacion === 1 || $operacion->tipo_operacion === 3)
                    <div class="col-md-3">    
                        <div class="form-group">
                                @if ($operacion->tipo_operacion === 1)    
                                    {{ Form::label('vessel', 'Vessel *') }}
                                @else
                                    {{ Form::label('vessel', 'Unit No. *') }}
                                @endif  <br>
                            {{$operacion->vessel}}
                        </div>     
                    </div> 
                    @endif 
                    @if ($operacion->tipo_operacion === 1 || $operacion->tipo_operacion === 2)
                    <div class="col-md-3">    
                        <div class="form-group">
                                @if ($operacion->tipo_operacion === 1)    
                                    {{ Form::label('vogage',  'Vogage *') }}
                                @else
                                {{ Form::label('vogage',  'Flight *' ) }}
                                @endif    <br>
                                {{$operacion->vogage}}
                        </div>     
                    </div> 
                    @endif
                </div>
                <div class="col-md-12">    
                    <div class="col-md-3">    
                        <div class="form-group">
                        <label for="POL" class="control-label">POL </label><br>
                        {{$operacion->POLdesc->puerto}}
                        </div>     
                    </div> 
                    <div class="col-md-3">    
                        <div class="form-group">
                        <label for="POD" class="control-label">POD</label><br>
                        {{$operacion->PODdesc->puerto}}
                        </div>     
                    </div> 
                    <div class="col-md-3">    
                        <div class="form-group">
                            <label>Final Dest.</label><br>
                            {{$operacion->final_dest}}
                        </div>     
                    </div>
                    <div class="col-md-3">    
                        <div class="form-group">
                        <label>INCOTERM</label><br>
                            {{ isset($operacion->incoDesc->nombre)?$operacion->incoDesc->nombre:null }}

                        </div>     
                    </div>
                </div>    
                <div class="col-md-12">     
                    <div class="col-md-3">    
                        <div class="form-group">
                            <label>Shipper</label><br>
                            {{isset($operacion->shipperDesc->nombre)?$operacion->shipperDesc->nombre:null}}
                            
                        </div>     
                    </div>
                    <div class="col-md-3">    
                        <div class="form-group">
                            <label>Consignee</label><br>
                            {{isset($operacion->consigneeDesc->nombre)?$operacion->consigneeDesc->nombre:null}}
                        </div>     
                    </div>
                    <div class="col-md-3">    
                        <div class="form-group">
                            <label>Customs Broker</label><br>
                            {{isset($operacion->agenteDesc->nombre)?$operacion->agenteDesc->nombre:null}}
                        </div>     
                    </div>
                    <div class="col-md-3">    
                        <div class="form-group">
                                
                            {{ Form::label('Shipping Agents', 'Shipping Agents') }}<br>
                            {{isset($operacion->AEnDesc->nombre)?$operacion->AEnDesc->nombre:null}}
                        </div>     
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-3">    
                        <div class="form-group">
                                @if ($operacion->tipo_operacion === 1)
                                    {{ Form::label('shipping_line', 'Shipping Line') }}
                                @elseif ($operacion->tipo_operacion === 2)
                                    {{ Form::label('shipping_line', 'Air Line ') }}
                                @elseif ($operacion->tipo_operacion === 3)
                                    {{ Form::label('shipping_line', 'In Land Carrier') }}
                                @else
                                    {{ Form::label('shipping_line', 'Not Apply ') }}
                                @endif <br>
                            {{isset($operacion->slineDesc->nombre)?$operacion->slineDesc->nombre:null}}
                        </div>     
                    </div>
                    
                    <div class="col-md-3">    
                            <div class="form-group">
                            {{ Form::label('terminal', 'Terminal *') }}<br>
                            {{$operacion->terminal}}
                            
                            </div>     
                        </div>
                     
                    @if ($operacion->tipo_operacion === 1)
                    <div class="col-md-3">    
                        <div class="form-group">
                            <label>House Release Date</label><br>
                            @if($operacion->house_release_date)
                            {{date('Y-m-d', strtotime($operacion->house_release_date))}}
                            @endif
                        </div>     
                    </div>
                    <div class="col-md-3">    
                        <div class="form-group">
                            <label>Master Release Date</label><br>
                            @if($operacion->master_release_date)
                            {{date('Y-m-d', strtotime($operacion->master_release_date))}}
                            @endif
                        </div>     
                    </div>
                    @endif
                </div>
                <div class="col-md-12">    
                    <div class="col-md-3">    
                        <div class="form-group">
                            <label>Commodity</label><br>
                            {{$operacion->commodity}}
                        </div>     
                    </div>
                    @if ($operacion->tipo_operacion === 1 || $operacion->tipo_operacion === 2)
                    <div class="col-md-2">    
                        <div class="form-group">
                            <label>Free dem days Customer</label><br>
                            {{$operacion->free_demurrages_days_cust}}
                        </div>     
                    </div>
                    <div class="col-md-2">    
                        <div class="form-group">
                            <label>Free dem days SL</label><br>
                            {{$operacion->free_demurrages_days_sl}}                          
                        </div>     
                    </div>
                    @endif

                        <div class="col-md-2">    
                            <div class="form-group">
                            <label>Demurrage Price</label><br>
                            {{$operacion->precio_demora}}                                
                            </div>     
                        </div>
                        <div class="col-md-2">
                                <div class="form-group">
                                    {{ Form::label('dp', 'Demurrage % IVA *') }}<br/>
                                    {{$operacion->iva}}
                                    
                                </div>
                            </div>


                    <div class="col-md-12">
                       <div class="col-md-6">    
                            <div class="form-group">
                            <label>Comments</label><br>
                            {{$operacion->comments}}
                            </div>     
                        </div>
                    </div>
                    
                </div>    
                </div>
                
                <div class="row">
                        <div class="col-md-12">
                        <div class="box box-primary">
                        <div class="col-md-6">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-archive" aria-hidden="true"></i>  
                                @if ($operacion->tipo_operacion === 1)
                                    Container Details
                                @elseif ($operacion->tipo_operacion === 2)
                                    Cargo Details
                                @elseif ($operacion->tipo_operacion === 3) 
                                    Units Details
                                @endif </h3>
                            </div></div>
                        
                            
                            <!-- /.box-header -->
                            <div class="box-body">
                            <table id="usuarios-table" class="table table-striped table-bordered dt-responsive nowrap"></table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                        
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- termina  getdata detalle de contenedor --> 
                    

            </div>
            <!-- /.box -->

            

        </div>
        <!--/.col (left) -->

    </div>
    <!-- /.row -->
    
</section>
<!-- /.content -->
@endsection
@yield('content')
@section('scripts')
<script type="text/javascript">

  $(function () {
    $('#usuarios-table').DataTable({
      processing: true,
      serverSide: true,
      type: 'GET',
      ajax: "{{ route('get.container.details') }}?showDeleted="+{{$operacion->id}},
      language: {
        "search": "Search",
        "lengthMenu": "Show _MENU_ records per page",
        "zeroRecords": "Sorry, we can't find what you're looking for",
        "info": "Showing page _PAGE_ of _PAGES_ of _TOTAL_ records",
        "infoEmpty": "Records not found",
        "infoFiltered": "(Filtered in _MAX_ total records)",
        "paginate": {
          "previous": "Previous",
          "next": "Next",
        },
      },
      columns: [
        @if($operacion->tipo_operacion === 1)
         { data: 'container_number', name: 'container_number', title: '# Ctnr' },
        @elseif($operacion->tipo_operacion === 3)
         { data: 'container_number', name: 'container_number', title: '# Box' },
        @endif
        @if($operacion->tipo_operacion === 1)
        { data: 'container_seal', name: 'container_seal', title: 'Ctnr Seal' },
        { data: 'mark', name: 'mark', title: 'Marks' },
        { data: 'tipo_operacion_desc', name: 'tipo_operacion_desc', title: 'Type Ctnr' },
        @endif
        { data: 'weight', name: 'weight', title: 'Weight (Kgs)' },
        @if($operacion->tipo_operacion === 2)
        { data: 'chargable_weight', name: 'chargable_weight', title: 'Chargable Weight (Kgs)' },
        @endif
        { data: 'volume', name: 'volume', title: 'Volume (CBM)' },
      ]
    });


});

</script>

@stop
@yield('scripts')