@extends('adminlte::layouts.app')
@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Operations<small>Operations Registry</small></h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('operaciones') }}">Operations</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

@if (\Session::has('msg'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif

@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif


    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> ¡Alerta!</h5>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if(session()->has('flash_message'))
  <div class="alert alert-success alert-dismissible fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Success!</h4>
    {{ session()->get('flash_message') }}
  </div>
  @endif

  @if(session()->has('error_message'))
  <div class="alert alert-danger alert-dismissible fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
    {{ session()->get('error_message') }}
  </div>
  @endif

    

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Operation</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                {{Form::model($operacion, array('route' => array('operaciones.update', $operacion->id), 'method' => 'PUT'))}}
                <div class="box-body">
                <br>
                <div class="col-md-12">
                    <h4><i class="fa fa-bell" aria-hidden="true"></i> Fields Marked With <b>*</b> Are Required</h4>
                </div>    
                <br>
                    <div class="col-md-12">
                        <div class="form-group">
                        <div class="col-md-6">
                            {{ Form::label('hbl', 'Operation') }}  <br>  
                            <input type="radio" checked class="form-radio">
                            <label> 
                                @if ($operacion->tipo_operacion === 1)
                                    <i class="fa fa-ship" aria-hidden="true"></i> Ocean
                                @elseif ($operacion->tipo_operacion === 2)
                                    <i class="fa fa-plane" aria-hidden="true"></i> Air
                                @elseif ($operacion->tipo_operacion === 3)    
                                    <i class="fa fa-bus" aria-hidden="true"></i> In land
                                @else
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Not Apply 
                                @endif
                            </label>
                            </div>
                            <div class="col-md-6">
                            {{ Form::label('hbl', 'Operation Profile') }}   <br> 
                            <input type="radio" checked class="form-radio">
                            <label> 
                                @if ($operacion->tipo_operacion === 1)
                                    @if ($operacion->operacion_detail === 1)
                                        LCL
                                    @elseif ($operacion->operacion_detail === 2)  
                                        FCL
                                    @else    
                                        Not Apply
                                    @endif    
                                @elseif ($operacion->tipo_operacion === 3)    
                                    @if ($operacion->operacion_detail === 1)
                                        National
                                    @elseif ($operacion->operacion_detail === 2)  
                                        International 
                                    @else
                                      Not Apply
                                    @endif  

                                @else
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Not Apply 
                                @endif
                            </label>
                            </div>
                            
                            
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            {{ Form::label('operacion', 'Operation  Type *') }}
                        </div>    
                        <div class="form-group">
                            <div class="col-md-4">    
                            <input type="radio" name="operacion" value="1" {{($operacion->operacion == '1')? "checked" : "" }} required id="import" class="form-radio"> <label for="import">Import</label>
                            </div>
                            <div class="col-md-4"> 
                            <input type="radio" name="operacion" value="2" {{($operacion->operacion == '2')? "checked" : "" }} id="export" class="form-radio"> <label for="export">Export </label>
                            </div>
                            <div class="col-md-4"> 
                            <input type="radio" name="operacion" value="3" {{($operacion->operacion == '3')? "checked" : "" }} id="domestic" class="form-radio"> <label for="domestic">Domestic</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                            <div class="col-md-12"><br>
                                {{ Form::label('cy', 'Service Mode *') }}
                            </div>    
                            <div class="form-group">
                                <div class="col-md-4">    
                                <input type="radio" name="cy" value="1" required id="cycy" {{$operacion->cy ==1 ? 'checked='.'"'.'checked'.'"' : '' }} class="form-radio"> <label for="cycy">CY - CY</label>
                                </div>
                                <div class="col-md-4"> 
                                <input type="radio" name="cy" value="2" id="cydoor" {{$operacion->cy ==2 ? 'checked='.'"'.'checked'.'"' : '' }} class="form-radio"> <label for="cydoor">CY - Door </label>
                                </div>
                                <div class="col-md-4" id="div-cydoor"> 

                                    @if($operacion->cy == 2)
                                    <div class="col-md-12">
                                        {{ Form::label("cdt", "CY Door Type *") }}
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                        {!! Form::select('cy_door', array('' => 'Choose one...','1' => 'All Truck', '2' => 'Train - Truck', '3' => 'Train + Ramp'), $operacion->cy_door, ['class' => 'form-control', 'id' => 'cdt']); !!}
                                            
                                        </div>
                                    </div>
                                    @endif

                                </div>
                                <br>
                            </div>
                        </div>

                    <div class="col-md-12">
                        <hr size="30">
                    </div>
                    <div class="col-md-12">
                        @if($typeShareuser->type == 1)
                            <div class="col-md-12">
                            <i class="fa fa-share" aria-hidden="true"></i>
                                {{ Form::label('share', 'Share Operation ') }}<br>
                                    {{ Form::select('share[]',$operadores,$shareuser,[
                                        'multiple' =>'multiple', 'id' =>'demo'],array('name'=>'share[]'))}}
                                        <br/><br/><br/>
                            </div>
                        @endif
                    </div>

                     
                    <div class="col-md-12">
                        <div class="col-md-3">
                        <i class="fa fa-file" aria-hidden="true"></i>
                            {{ Form::label('po', 'PO') }}
                            {!! Form::text('PO',json_decode($operacion->PO), [
                                'data-role' => 'tagsinput',
                                'class' => 'form-control',
                                ]) !!}
                        </div>
                        <div class="col-md-3">
                            {{ Form::label('pi', 'PI') }}
                            {!! Form::text('PI',json_decode($operacion->PI), [
                                'data-role' => 'tagsinput',
                                'class' => 'form-control',
                                ]) !!}
                        </div>
                        <div class="col-md-3">
                            {{ Form::label('invoice', '# Invoice') }}
                            {!! Form::text('factura',old('factura'), [
                                'class' => 'form-control',
                                'maxlength' =>'255', 'size'=>'255'
                                ]) !!}
                        </div>
                        <div class="col-md-3">
                            {{ Form::label('sc', 'SC') }}
                            {!! Form::text('sc',old('sc'), [
                                'class' => 'form-control',
                                'maxlength' =>'255', 'size'=>'255'
                                ]) !!}
                        </div>
                        <br/>    
                    </div>
                    <div class="col-md-12">       
                        <div class="col-md-3">
                            <div class="form-group">

                                @if ($operacion->tipo_operacion === 1 || $operacion->tipo_operacion === 3)
                                    {{ Form::label('hbl', 'House (HBL)') }}
                                @else
                                    {{ Form::label('hbl', 'House (HWB)') }}
                                @endif    
                                {!! Form::text('hbl',null, [
                                'class' => 'form-control',
                                'readonly' => 'readonly'
                                ]) !!}
                            </div>
                        </div>   
                        <div class="col-md-3">    
                            <div class="form-group">
                                @if ($operacion->tipo_operacion === 1 || $operacion->tipo_operacion === 3)
                                    {{ Form::label('mbl', 'MBL (MBL) ') }}
                                @else
                                    {{ Form::label('mbl', 'MBL (MWB) ') }}
                                @endif
                                
                                {!! Form::text('mbl', null, [
                                'class' => 'form-control',
                                ]) !!}

                                
                            </div>     
                        </div> 
                        @if ($operacion->tipo_operacion === 1)
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('house_release', 'House Release Mode') }}
                                {{ Form::select('house_release',$LHBL,null,[
                            'class' => 'form-control','placeholder' => 'seleccione uno...'],array('name'=>'LHBL[]'))}}
                            </div>     
                        </div>

                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('master_release', 'Master Release Mode') }}
                                {{ Form::select('master_release',$LMBL,null,[
                            'class' => 'form-control','placeholder' => 'seleccione uno...'],array('name'=>'LMBL[]'))}}
                            </div>     
                        </div>
                        @endif
                    </div>
                    <div class="col-md-12">        
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('ETD', 'ETD *') }}
                                {!! Form::date('ETD', date('Y-m-d', strtotime($operacion->ETD)), [
                                    'class' => 'form-control',
                                    'required' => 'required',]) !!}
                            </div>     
                        </div>

                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('ETA', 'ETA *') }}
                                {!! Form::date('ETA', date('Y-m-d', strtotime($operacion->ETA)), [
                                    'class' => 'form-control',
                                    'required' => 'required',]) !!}
                            </div>     
                        </div>
                        <div class="col-md-3">    
                                                    <div class="form-group">
                                                        {{ Form::label('ATD', 'ATD *') }}
                                                        @if($operacion->ATD == NULL)
                                                        {!! Form::date('ATD', NULL, [
                                                            'class' => 'form-control',
                                                            'required' => 'required',]) !!}
                                                        @else
                                                        {!! Form::date('ATD', date('Y-m-d', strtotime($operacion->ATD)), [
                                                            'class' => 'form-control',
                                                            'required' => 'required',]) !!}
                                                        @endif
                                                        
                                                    
                                                    </div>     
                                                </div>

                                                <div class="col-md-3">    
                                                    <div class="form-group">
                                                        {{ Form::label('ATA', 'ATA *') }}
                                                        @if($operacion->ATA == NULL)
                                                        {!! Form::date('ATA', NULL, [
                                                            'class' => 'form-control',
                                                            'required' => 'required',]) !!}
                                                        @else
                                                        {!! Form::date('ATA', date('Y-m-d', strtotime($operacion->ATA)), [
                                                            'class' => 'form-control',
                                                            'required' => 'required',]) !!}
                                                        @endif
                                                        
                                                        
                                                    
                                                    </div>     
                                                </div>
                        @if ($operacion->tipo_operacion === 1 || $operacion->tipo_operacion === 3) 
                        <div class="col-md-3">    
                            <div class="form-group">
                                    
                                
                                @if ($operacion->tipo_operacion === 1)    
                                    {{ Form::label('vessel', 'Vessel') }}
                                @else
                                    {{ Form::label('vessel', 'Unit No.') }}
                                @endif  

                                {!! Form::text('vessel', null, [
                                'class' => 'form-control'
                                ]) !!}
                            </div>     
                        </div>
                        @endif 
                        @if ($operacion->tipo_operacion === 1 || $operacion->tipo_operacion === 2)
                        <div class="col-md-3">    
                            <div class="form-group">
                                @if ($operacion->tipo_operacion === 1)    
                                    {{ Form::label('vogage',  'Vogage') }}
                                @else
                                {{ Form::label('vogage',  'Flight' ) }}
                                @endif     
                                {!! Form::text('vogage', null, [
                                'class' => 'form-control'
                                ]) !!}
                            </div>     
                        </div>
                        @endif
                    </div> 
                    
                    <div class="col-md-12">     
                        <div class="col-md-3">    
                            <div class="form-group">
                            <label for="POL" class="control-label">
                                @if ($operacion->tipo_operacion == 1 || $operacion->tipo_operacion == 2)
                                    Select POL *
                                @else
                                    Select Origin *
                                @endif 
                                
                            </label>
                            <input id="basics" type="text" value="{{$Pol_Desc}}"
                             class="form-control" placeholder="Search" />
                            <input type="hidden" id="POL" name="POL" value="{{$operacion->POL}}">
                            </div>     
                        </div> 
                        <div class="col-md-3">    
                            <div class="form-group">
                            <label for="POD" class="control-label">
                                @if ($operacion->tipo_operacion == 1 || $operacion->tipo_operacion == 2)
                                    Select POD *
                                @else
                                    Select Destination *
                                @endif
                            </label>
                            <input id="podsearch" type="text" value="{{$Pod_Desc}}" class="form-control" placeholder="Search" />
                            <input type="hidden" id="POD" name="POD" value="{{$operacion->POD}}">
                            </div>     
                        </div> 
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('final_dest', 'Final Dest. ') }}
                                {!! Form::text('final_dest', null, [
                                'class' => 'form-control',
                                ]) !!}
                            </div>     
                        </div> 
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('INCOTERM', 'INCOTERM ') }}
                                {{ Form::select('INCOTERM',$Incoterm,null,[
                            'class' => 'form-control','placeholder' => 'seleccione uno...'],array('name'=>'Incoterm[]'))}}
                            </div>     
                        </div> 
                        
                    </div>   
                    <div class="col-md-12">
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('shipper', 'Shipper *') }}
                                <input id="shipper_desc"  type="text"  requerid value="{{isset($operacion->shipperDesc->nombre)?$operacion->shipperDesc->nombre:null}}" class="form-control" placeholder="Buscar Shipper" />
                                <input type="hidden" id="shipper" requerid name="shipper" value="{{$operacion->shipper}}">
                                
                            </div>     
                        </div>
                        
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('consignee', 'Consignee *') }}
                                <input id="consignee_desc"  requerid="requerid" type="text" value="{{isset($operacion->consigneeDesc->nombre)?$operacion->consigneeDesc->nombre:null}}" class="form-control" placeholder="Buscar Consignee" />
                                <input type="hidden" id="consignee" requerid="requerid" name="consignee" value="{{$operacion->consignee}}">
                            </div>     
                        </div>
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('agente', 'Customs Broker') }}
                                <input id="agente_desc"  type="text" value="{{ isset($operacion->agenteDesc->nombre)?$operacion->agenteDesc->nombre:null}}" class="form-control" placeholder="Buscar Agente" />
                                <input type="hidden" id="agente" name="agente" value="{{$operacion->agente}}">
                            </div>     
                        </div>
                        <div class="col-md-3">    
                            <div class="form-group">
                            {{ Form::label('agente_envio', 'Forwarder Agent *') }}
                                <input id="agente_envio_desc"  type="text" value="{{ isset($operacion->AEnDesc->nombre)?$operacion->AEnDesc->nombre:null}}" class="form-control" placeholder="Search Shipping Agents" />
                                <input type="hidden" id="agente_envio" name="agente_envio" value="{{$operacion->agente_envio}}"> 
                            </div>     
                        </div>
                        
                    </div>
                        
                    <div class="col-md-12">
                        <div class="col-md-3">    
                            <div class="form-group">
                                @if ($operacion->tipo_operacion === 1)
                                    {{ Form::label('shipping_line', 'Shipping Line ') }}
                                @elseif ($operacion->tipo_operacion === 2)
                                    {{ Form::label('shipping_line', 'Air Line  ') }}
                                @elseif ($operacion->tipo_operacion === 3)
                                    {{ Form::label('shipping_line', 'In Land Carrier ') }}
                                @else
                                    {{ Form::label('shipping_line', 'Not Apply ') }}
                                @endif 
                                <input id="shipping_line_desc"  type="text" value="{{ isset($operacion->slineDesc->nombre)?$operacion->slineDesc->nombre:null}}" class="form-control" placeholder="Shipping Line" />
                                <input type="hidden" id="shipping_line"name="shipping_line" value="{{$operacion->shipping_line}}">
                            </div>     
                        </div>
                        <div class="col-md-3">    
                            <div class="form-group">
                            {{ Form::label('terminal', 'Terminal ') }}
                            {!! Form::text('terminal', null, [
                                'class' => 'form-control']) !!}
                            
                            </div>     
                        </div>
                    @if ($operacion->tipo_operacion === 1)
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('', 'House Release Date') }}
                                {!! Form::date('house_release_date', isset($operacion->master_release_date) ? date('Y-m-d', strtotime($operacion->house_release_date)) : NULL, [
                                    'class' => 'form-control',
                                    ]) !!}
                            
                            </div>     
                        </div>
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('master_release_date', 'Master Release Date') }}
                                {!! Form::date('master_release_date', isset($operacion->master_release_date) ? date('Y-m-d', strtotime($operacion->master_release_date)) : NULL, [
                                    'class' => 'form-control',
                                    ]) !!}
                            
                            </div>     
                        </div>
                    @endif    
                        
                        <div class="col-md-6">    
                            <div class="form-group">
                                {{ Form::label('commodity', 'Commodity') }}
                                {!! Form::textarea('commodity', null, ['class'=>'form-control',
                                    'rows' => '2']) !!}
                                
                            </div>     
                        </div>
                    </div>
                    @if ($operacion->tipo_operacion === 1 || $operacion->tipo_operacion === 2)
                    <div class="col-md-12">    
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('free_demurrages_days_cust', 'Free dem days Customer *') }}
                                {!! Form::number('free_demurrages_days_cust', null, [
                                'class' => 'form-control', 'requerid' => 'requerid'
                                ]) !!}
                            
                            </div>     
                        </div>
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('free_demurrages_days_sl', 'Free dem days SL *') }}
                                {!! Form::number('free_demurrages_days_sl', null, [
                                'class' => 'form-control', 'requerid' => 'requerid'
                                ]) !!}
                            
                            </div>     
                        </div>
                        
                        @endif 

                        <div class="col-md-4">

                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('dp', 'Demurrage Price *') }}
                                    {!! Form::number('precio_demora', null, ['class'=>'form-control','required' => 'required', 'step'=>'0.01']) !!}
                                    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('dp', 'Demurrage % IVA *') }}
                                    {!! Form::number('iva', null, ['class'=>'form-control','required' => 'required', 'step'=>'0.01']) !!}
                                    
                                </div>
                            </div>         
                            
                              
                        </div>

                        <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('booking', 'Booking') }}
                                {!! Form::text('booking', null, ['class'=>'form-control','rows' => '3']) !!}
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('origin_contry', 'Origin Contry') }}
                                {{ Form::select('origin_contry',$pais,null,[
                                        'class' => 'form-control','placeholder' => 'seleccione uno...'],array('name'=>'cargos[]'))}}
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                            {{ Form::label('destination_contry', 'Destination Contry') }}
                                {{ Form::select('destination_contry',$pais,null,[
                                        'class' => 'form-control','placeholder' => 'seleccione uno...'],array('name'=>'cargos[]'))}}
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('booking_status', 'Booking Status') }}
                                {!! Form::text('booking_status', null, ['class'=>'form-control','rows' => '3']) !!}
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('expense_bl', 'Expense BL') }}
                                {!! Form::text('expense_bl', null, ['class'=>'form-control','rows' => '3']) !!}
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('expense_contr', 'Expense Contr') }}
                                {!! Form::text('expense_contr', null, ['class'=>'form-control','rows' => '3']) !!}
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('tarifa', 'Tariff') }}
                                {!! Form::text('tarifa', null, ['class'=>'form-control','rows' => '3']) !!}
                                
                            </div>
                        </div>
                    </div>
                        <div class="col-md-12">
                       <div class="col-md-6">    
                            <div class="form-group">
                                {{ Form::label('comments', 'Comments') }}
                                {!! Form::textarea('comments', null, ['class'=>'form-control','rows' => '3']) !!}
                                
                            </div>     
                        </div>
                    </div>
                    </div>
                   

                       
                    
                </div>
                <div class="box-footer">
                    {{ Form::button('<i class="fa fa-save" aria-hidden="true"></i> Save', ['class' => 'btn btn-primary', 'type' => 'submit']) }}
                    <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-title="10" data-target="#fileAdd"><i class="fa fa-paperclip" aria-hidden="true"></i> Upload File(s) HBL/MBL</button>

                </div>
                {{ Form::close() }}

               

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Return Empty Container</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      ¿Confirm empty container return date: <b><span id="fav-title"></span> <b> to ETA:</b> <span id="fav-desc"></span></b>?
          <hr>
          <form id="editar-detalle-contenedor" action='{{ action("OperationDetails@emptyContainer") }}'  method="post">
            {{csrf_field()}}
            <div class="col-md-12">    
                            <div class="form-group">
                                {{ Form::label('empty_container', 'Date *') }}
                                {!! Form::date('empty_container', null, [
                                    'class' => 'form-control',
                                    'required' => 'required',]) !!}
                            </div>     
                        </div> 
            <input name="idContainer" id="container" type="hidden" />
            <input name="NContainer" id="NContainer" type="hidden" />
            <input name="ETD" id="ETD" type="hidden" />
            <input name="ETA" id="ETA" type="hidden" />
            <input name="Nhbl" id="Nhbl" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>


                <!-- inicia  getdata detalle de contenedor -->
                <div class="row">
                        <div class="col-md-12">
                        <div class="box box-primary">
                        <div class="col-md-6">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-archive" aria-hidden="true"></i> 
                                @if ($operacion->tipo_operacion === 1)
                                    Container Details
                                @elseif ($operacion->tipo_operacion === 2)
                                    Cargo Details
                                @elseif ($operacion->tipo_operacion === 3) 
                                    Units Details
                                @endif     
                            </h3>
                            </div></div>
                        <div class="col-md-6 text-right">
                        <br/>

                                @if ($operacion->tipo_operacion === 1)
                                    @if ($operacion->operacion_detail != 1)
                                    <button id="myBtn" class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Add Container Detail</button>
                                    @endif    
                                @elseif ($operacion->tipo_operacion === 3)    
                                    <button id="myBtn" class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Add Container Detail</button>

                                @else
                                    
                                @endif

                            
                        </div>
                            
                            <!-- /.box-header -->
                            <div class="box-body">
                            <table id="usuarios-table" class="table table-striped table-bordered dt-responsive nowrap"></table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                        
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- termina  getdata detalle de contenedor --> 
                    

            </div>
            <!-- /.box -->

        </div>
        <!--/.col (left) -->

    </div>
    <!-- /.row -->


    <!-- Add Model -->
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- header modal -->
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="mySmallModalLabel">Add Detail</h4>
            </div>

            <!-- body modal -->
            <div class="modal-body text-center">
                
                <form action="{{ action('OperationDetails@store') }}" method="POST">
                {{csrf_field()}}
                <div class="box-body">
                    <div class="col-md-12">
                            <h3 class="box-title">Details </h3>
                            <hr class="hr-primary" />
                            <p>Fields marked with <b>*</b> are required</p>
                    </div>
                    @if ($operacion->tipo_operacion === 1 || $operacion->tipo_operacion === 3) 
                    <div class="col-md-6">
                                    <div class="form-group">
                                        @if ($operacion->tipo_operacion === 1)
                                            {{ Form::label('container_number', '# Ctnr *') }}
                                        @else
                                            {{ Form::label('container_number', '# Box *') }}
                                        @endif    
                                        {!! Form::text('container_number', null, [
                                        'class' => 'form-control', 'size' => '15', 'maxlength' => '18', 'requerid' => 'requerid']) !!}
                                    </div>
                    </div>
                    @endif
                    @if ($operacion->tipo_operacion === 1)
                    <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('container_seal', 'Ctnr Seal') }}
                                        {!! Form::text('container_seal', null, [
                                        'class' => 'form-control', 'size' => '15', 'maxlength' => '15']) !!}
                                    </div>
                    </div> 
                    <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('mark', 'Marks') }}
                                        {!! Form::text('mark', null, [
                                        'class' => 'form-control', 'size' => '50', 'maxlength' => '50']) !!}
                                    </div>
                    </div> 
                    <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('container_type', 'Type Ctnr') }}
                                        {{ Form::select('container_type',$contenedores,null,[
                                        'class' => 'form-control', 'placeholder' => 'seleccione uno...'],array('name'=>'contenedores[]'))}}
                                    </div>
                    </div> 
                    @endif
                    <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('weight', 'Weight (Kgs) *') }}
                                        {!! Form::number('weight', null, [
                                        'class' => 'form-control', 'size' => '11', 'requerid' => 'requerid', 'maxlength' => '11','step' => 'any']) !!}
                                    </div>

                    </div>
                    @if ($operacion->tipo_operacion === 2)
                    <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('chargable_weight', 'Chargable Weight (Kgs) *') }}
                                        {!! Form::number('chargable_weight', null, [
                                            'id' => 'weight','class' => 'form-control', 'size' => '11', 'requerid' => 'requerid', 'maxlength' => '11','step' => 'any']) !!}
                                    </div>

                    </div>
                    @endif
                    <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('volume', 'Volume  (CBM) *') }}
                                        {!! Form::number('volume', null, ['placeholder' => '0.00',
                                        'class' => 'form-control', 'size' => '11', 'requerid' => 'requerid', 'maxlength' => '11','step' => 'any']) !!}
                                    </div>

                    </div>

                    
                    
                    </div>

                <div class="box-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    {{ Form::hidden('id_entidad', encrypt($operacion->id), array('id' => $operacion->id)) }}
                    {{ Form::hidden('tipo_operacion', $operacion->tipo_operacion) }}
                    {{ Form::button('<i class="fa fa-save" aria-hidden="true"></i> Save', ['class' => 'btn btn-primary', 'type' => 'submit']) }}

                    </div>

                   </form>

                
            </div>
        </div>
        </div>
    </div>
  <!-- /.Add Model -->

  <!-- edit Model -->
  <div id="myModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- header modal -->
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="mySmallModalLabel">Edit Detail</h4>
            </div>

            <!-- body modal -->
            <div class="modal-body text-center">

            <form id="editar-detalle-contenedor" action='{{ action("OperationDetails@update") }}' method="post">
                {{csrf_field()}}
                <div class="box-body">
                    <div class="col-md-12">
                            <h3 class="box-title">Details </h3>
                            <hr class="hr-primary" />
                            <p>Fields marked with <b>*</b> are required</p>
                    </div> 
                    @if ($operacion->tipo_operacion === 1 || $operacion->tipo_operacion === 3)
                    <div class="col-md-6">
                                    <div class="form-group">
                                    @if ($operacion->tipo_operacion === 1)
                                        {{ Form::label('container_number', '# Ctnr *') }}
                                    @else
                                        {{ Form::label('container_number', '# Box *') }}
                                    @endif     
                                        {!! Form::text('container_number', null, [
                                            'id' => 'container_number','class' => 'form-control', 'size' => '15', 'maxlength' => '18', 'requerid' => 'requerid']) !!}
                                    </div>
                    </div>
                    @endif
                    @if ($operacion->tipo_operacion === 1)
                    <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('container_seal', 'Ctnr Seal') }}
                                        {!! Form::text('container_seal', null, [
                                            'id' => 'container_seal','class' => 'form-control', 'size' => '15', 'maxlength' => '15']) !!}
                                    </div>
                    </div> 
                    <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('mark', 'Marks') }}
                                        {!! Form::text('mark', null, [
                                            'id' => 'mark','class' => 'form-control', 'size' => '50', 'maxlength' => '50']) !!}
                                    </div>
                    </div> 
                    <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('container_type', 'Type Ctnr') }}
                                        {{ Form::select('container_type',$contenedores,null,[
                                            'id' => 'container_type','class' => 'form-control','placeholder' => 'seleccione uno...'],array('name'=>'contenedores[]'))}}
                                    </div>
                    </div> 
                    @endif
                    
                    <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('weight', 'Weight (Kgs) *') }}
                                        {!! Form::number('weight', null, [
                                            'id' => 'weight','class' => 'form-control', 'size' => '11', 'requerid' => 'requerid', 'maxlength' => '11', 'step' => 'any']) !!}
                                    </div>

                    </div>
                    @if ($operacion->tipo_operacion === 2)
                    <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('chargable_weight', 'Chargable Weight (Kgs) *') }}
                                        {!! Form::number('chargable_weight', null, [
                                            'id' => 'weight','class' => 'form-control', 'size' => '11', 'requerid' => 'requerid', 'maxlength' => '11', 'step' => 'any']) !!}
                                    </div>

                    </div>
                    @endif
                    <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('volume', 'Volume (CBM) *') }}
                                        {!! Form::number('volume', null, [
                                            'id' => 'volume','class' => 'form-control', 'size' => '11', 'requerid' => 'requerid', 'maxlength' => '11', 'step' => 'any', 'placeholder' => '0.00']) !!}
                                    </div>

                    </div>

                    
                    
                    </div>

                <div class="box-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    {{ Form::hidden('id_detailo', null, array('id' => 'id_detailo')) }}
                    {{ Form::hidden('tipo_operacion', $operacion->tipo_operacion) }}
                    {{ Form::button('<i class="fa fa-save" aria-hidden="true"></i> Save', ['class' => 'btn btn-primary', 'id' => 'editar','type' => 'submit']) }}
                    </div>

                    </form>
                
            </div>
        </div>
        </div>
    </div>
    
  <!-- /.edit Model -->

  <!-- Delete Model -->
  <div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Remove Detail Container/Box</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">

            @if($operacion->tipo_operacion === 1 && $operacion->operacion_detail != 1)
                ¿Please Confirm what to do with the Container: <b><span id="fav-title"></span></b>?
            @elseif($operacion->tipo_operacion === 3) 
                ¿Please Confirm what to do with the Box: <b><span id="fav-title"></span></b>?
            @else
                Action not allowed                           
            @endif    

          <form action='{{ action("OperationDetails@destroy") }}' id="delForm" method="post">
            {{csrf_field()}}
            <div class="form-group">
                        <div class="col-md-12">{{ Form::label('select', 'Select Option*') }}</div>
                        <div class="col-md-6">
                            <input type="radio" value="{{encrypt('3')}}" name="status" required class="form-radio">
                            <label> Cancel </label>
                        </div>
                        <div class="col-md-6">
                            <input type="radio" value="{{encrypt('0')}}" name="status" class="form-radio">
                            <label> Remove </label>
                        </div>
            </div> 
            <div class="form-group">
            <hr/>
            {{ Form::label('comments', 'Comments ') }}
            {!! Form::textarea('comments', null, ['class'=>'form-control','rows' => '2']) !!}
            </div>           
            <hr>
            <input name="id_detail_contr" id="id_emp" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            @if($operacion->tipo_operacion === 1 && $operacion->operacion_detail != 1)
                <button type="submit" value="delete" class="btn btn-success">Save</button>
            @elseif($operacion->tipo_operacion === 3) 
                <button type="submit" value="delete" class="btn btn-success">Save</button>
            @else
                                            
            @endif    
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Delete Model -->

  <!-- add file Model -->

  <div id="fileAdd" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- header modal -->
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="mySmallModalLabel">Add Files</h4>
            </div>

            <!-- body modal -->
            <div class="modal-body text-center">

            <form id="editar-detalle-contenedor" action='{{ action("OperacionController@files") }}' enctype="multipart/form-data" method="post">
                {{csrf_field()}}
                <div class="box-body">
                    <div class="col-md-12">
                            <h3 class="box-title">Select Files  </h3>
                            <hr class="hr-primary" />
                            <p>Fields marked with <b>*</b> are required</p>
                            <p>*Only files with PDF </p>
                    </div> 
                    <br/>
                    <div class="form-group">
                        <div class="col-md-6">
                            <div class="col-md-12">
                            @if($operacion->file_hbl != '')
                            <b><a href="{{url('operaciones/view').'/'.encrypt($operacion->file_hbl)}}"><i class="fa fa-file-pdf-o fa-3" aria-hidden="true"></i> View File </a></b><br/>
                            @endif
                            </div>               
                            <label class="file-upload btn btn-primary">
                                Browse for file HBL... <input type="file" name="file_hbl" requerid="requerid" accept="application/pdf" />
                            </label>
                        </div>
                    
                        <div class="col-md-6">
                        <div class="col-md-12">
                            @if($operacion->file_mbl != '')
                            <b><a href="{{url('operaciones/view').'/'.encrypt($operacion->file_mbl)}}"><i class="fa fa-file-pdf-o fa-3" aria-hidden="true"></i> View File </a></b><br/>
                            @endif
                            </div>   
                            <label class="file-upload btn btn-primary">
                                Browse for file MBL ... <input type="file" name="file_mbl" requerid="requerid" accept="application/pdf" />
                            </label>
                        </div>
                    </div>
                    

                <div class="box-footer text-right">
                        <div class="col-md-12">
                        <br>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            {{ Form::hidden('operation', encrypt($operacion->id)) }}
                            {{ Form::hidden('hbl', $operacion->hbl) }}
                            {{ Form::button('<i class="fa fa-save" aria-hidden="true"></i> Save', ['class' => 'btn btn-primary', 'id' => 'editar','type' => 'submit']) }}
                        </div>
                </div>

                    </form>
                
            </div>
        </div>
        </div>
    </div>
  <!-- /.add file Model -->

<!-- Deleteaaa  Model -->
<div id="DeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Removes</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">

            @if($operacion->tipo_operacion === 1 && $operacion->operacion_detail != 1)
                ¿Please Confirm what to do with the Container: <b><span id="fav-title"></span></b>?
            @elseif($operacion->tipo_operacion === 3) 
                ¿Please Confirm what to do with the Box: <b><span id="fav-title"></span></b>?
            @else
                Action not allowed                           
            @endif    

          <form action='{{ action("OperationDetails@destroy") }}' id="delForm" method="post">
            {{csrf_field()}}
            <div class="form-group">
                        <div class="col-md-12">{{ Form::label('select', 'Select Option*') }}</div>
                        <div class="col-md-6">
                            <input type="radio" value="{{encrypt('3')}}" name="status" required class="form-radio">
                            <label> Cancel </label>
                        </div>
                        <div class="col-md-6">
                            <input type="radio" value="{{encrypt('0')}}" name="status" class="form-radio">
                            <label> Remove </label>
                        </div>
            </div> 
            <div class="form-group">
            <hr/>
            {{ Form::label('comments', 'Comments ') }}
            {!! Form::textarea('comments', null, ['class'=>'form-control','rows' => '2']) !!}
            </div>           
            <hr>
            <input name="id_detail_contr" id="id_emp" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            @if($operacion->tipo_operacion === 1 && $operacion->operacion_detail != 1)
                <button type="submit" value="delete" class="btn btn-success">Save</button>
            @elseif($operacion->tipo_operacion === 3) 
                <button type="submit" value="delete" class="btn btn-success">Save</button>
            @else
                                            
            @endif    
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Delete Model -->

<!-- return empty container Model -->
<div id="confirmReturnContainer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Return Empty Container</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Confirm empty container return date: <b><span id="fav-title"></span> <b> to ETA:</b> <span id="fav-desc"></span></b>?
          <hr>
          <form id="editar-detalle-contenedor" action='{{ action("OperationDetails@emptyContainer") }}'  method="post">
            {{csrf_field()}}
            <div class="col-md-12">    
                            <div class="form-group">
                                {{ Form::label('empty_container', 'Date *') }}
                                {!! Form::date('empty_container', null, [
                                    'class' => 'form-control',
                                    'required' => 'required',]) !!}
                            </div>     
                        </div> 
            <input name="idContainer" id="container" type="hidden" />
            <input name="NContainer" id="NContainer" type="hidden" />
            <input name="ETD" id="ETD" type="hidden" />
            <input name="ETA" id="ETA" type="hidden" />
            <input name="Nhbl" id="Nhbl" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" value="delete" class="btn btn-success">Ok</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.return empty container Model -->



    
</section>
<!-- /.content -->
@endsection
@section('scripts')
<script src="{{ URL::asset('/js/file-upload.js') }}"></script>
<script src="{{ URL::asset('/js/bootstrap-tagsinput.min.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">
$(document).ready(function(){

    $('#demo').multiselect();


    $('.file-upload').file_upload();


    $('#shipper_desc').change(function () { 
        $("#shipper").val(''); 
    });

    $('#shipper_desc').change(function () { 
        $("#shipper").val(''); 
    });

    $('#shipping_line_desc').change(function () { 
        $("#shipping_line").val(''); 
    });

    $('input[name="cy"]').click(function(){
            var inputValue = $(this).attr("value");
            $("#div-cydoor" ).empty();
            if(inputValue == 2){
                $("#div-cydoor" ).append('<div class="col-md-12">{{ Form::label("cdt", "CY Door Type *") }}</div><div class="form-group"><div class="col-md-6">{!! Form::select("cy_door", array("" => "Choose one...","1" => "All Truck", "2" => "Train - Truck", "3" => "Train + Ramp"), $operacion->cy_door, ["class" => "form-control", "id" => "cdt"]); !!}</div></div>');
            }else{ $("#div-cydoor" ).empty(); }
    });

    

        /** Autocomplete **/

    $('#basics').ready(function(){
        console.log({{$operacion->tipo_operacion}});
        puertoSearch("basics", "POL", {{$operacion->tipo_operacion}});
    });
    $('#podsearch').ready(function(){
        puertoSearch("podsearch", "POD", {{$operacion->tipo_operacion}});
    });

    $('#shipper_desc').ready(function(){
        entidadSearch("shipper", "shipper_desc");
    });

    $('#consignee_desc').ready(function(){
        entidadSearch("consignee", "consignee_desc");
    });

    $('#agente_desc').ready(function(){
        entidadSearch("agente", "agente_desc");
    });

    $('#agente_envio_desc').ready(function(){
        entidadSearch("agente_envio", "agente_envio_desc");
    });

    $('#shipping_line_desc').ready(function(){

            var valor = '';
            if({{$operacion->tipo_operacion}} == 1){
                valor = 'shipping_line';

            }else if({{$operacion->tipo_operacion}} == 2){
                valor = 'air_line';

            }else if({{$operacion->tipo_operacion}} == 3){
                valor = 'carrier_line';
            }else{
                valor = 'shipping_line';
            }   

            shlentidadSearch(valor, "shipping_line_desc");  
        
    });

    function shlentidadSearch(tipo,etiqueta){
        $("#"+etiqueta).easyAutocomplete({
            url: function(search) {
                return "{{route('autocomplete.entidad')}}?tipo=" + tipo +"&search=" + search;
            },

            getValue: "nombre",
            list: {
                    onSelectItemEvent: function() {
                        var dataID = $("#"+etiqueta).getSelectedItemData().id;
                        $("#shipping_line").val(dataID);
                    },
            match: {
                    enabled: true
                    }
            }
        });
    }


    function puertoSearch(etiqueta, valor, type){
            $("#"+etiqueta).easyAutocomplete({
                url: function(search) {
                    return "{{route('autocomplete.fetch')}}?search=" + search + "&type=" + type;
                },

                getValue: "puerto",
                list: {
                        onSelectItemEvent: function() {
                            var puerto = $("#" + etiqueta).getSelectedItemData().id;
                            $("#"+valor).val(puerto);
                        
                        },
                        match: {
                            enabled: true
                        }
                }
            });
    }

    function entidadSearch(tipo,etiqueta){
        $("#"+etiqueta).easyAutocomplete({
            url: function(search) {
                return "{{route('autocomplete.entidad')}}?tipo=" + tipo +"&search=" + search;
            },
        
            getValue: "nombre",
            list: {
                    onSelectItemEvent: function() {
                        var dataID = $("#"+etiqueta).getSelectedItemData().id;
                        $("#"+tipo).val(dataID);
                    },
            match: {
                     enabled: true
                    }
            }
        });
    }

    
   
  });
  $(function () {


    $('#usuarios-table').DataTable({
      processing: true,
      serverSide: true,
      type: 'GET',
      ajax: "{{ route('get.container.details') }}?operation="+{{$operacion->tipo_operacion}}+"&showDeleted="+{{$operacion->id}},
      language: {
        "search": "Search",
        "lengthMenu": "Show _MENU_ records per page",
        "zeroRecords": "Sorry, we can't find what you're looking for",
        "info": "Showing page _PAGE_ of _PAGES_ of _TOTAL_ records",
        "infoEmpty": "Records not found",
        "infoFiltered": "(Filtered in _MAX_ total records)",
        "paginate": {
          "previous": "Previous",
          "next": "Next",
        },
      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' }, 
        @if($operacion->tipo_operacion === 1)
         { data: 'container_number', name: 'container_number', title: '# Ctnr' },
        @elseif($operacion->tipo_operacion === 3)
         { data: 'container_number', name: 'container_number', title: '# Box' },
        @endif
        @if($operacion->tipo_operacion === 1)
        { data: 'container_seal', name: 'container_seal', title: 'Ctnr Seal' },
        { data: 'mark', name: 'mark', title: 'Marks' },
        { data: 'tipo_operacion_desc', name: 'tipo_operacion_desc', title: 'Type Ctnr' },
        @endif
        { data: 'weight', name: 'weight', title: 'Weight (Kgs)' },
        @if($operacion->tipo_operacion === 2)
        { data: 'chargable_weight', name: 'chargable_weight', title: 'Chargable Weight (Kgs)' },
        @endif
        { data: 'volume', name: 'volume', title: 'Volume (CBM)' },
        {
          data: 'activo', name: 'activo', title: 'Status', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<button type="button" class="btn btn-success"><i class="fa fa-check-circle-o"></i> Active</button>';
            }else if (data == 2) {
                return '<button type="button" class="btn btn-warning"><i class="fa fa-check-circle-o"></i> Return Empty</button>';
            }else if (data == 3) {
                return '<button type="button" class="btn btn-danger"><i class="fa fa-ban"></i> Cancel</button>';
            } 
            else {
                return '<button type="button" class="btn btn-danger"><i class="fa fa-ban"></i> Delete</button>';
            }
          }
        },
        { data: 'action', name: 'action', title: 'Action', orderable: false, searchable: false }
      ]
    });


        // se activa cuando el modal para Eliminar está a punto de ser mostrado.
        $('#confirmDeleteComment').on('show.bs.modal', function (e) {
       
            var modal = $(this);

            // obtener el atributo del elemento pulsado.
            $("#fav-title").html($(e.relatedTarget).data('title'));
            var idoperd = $(e.relatedTarget).data('idoped');
            modal.find('.modal-body #id_emp').val(idoperd);
            

        });

        // se activa cuando el modal para Retornar Contenedor está a punto de ser mostrado.
        $('#exampleModal').on('show.bs.modal', function (e) {
            var modal = $(this);

            // obtener el atributo idEmpresa del elemento pulsado.
            $("#fav-title").html($(e.relatedTarget).data('title'));
            $("#fav-desc").html($(e.relatedTarget).data('eta'));
            var idContaioner = $(e.relatedTarget).data('cont');
            var ncont = $(e.relatedTarget).data('ncont');
            var hbl = $(e.relatedTarget).data('title');
            var neta = $(e.relatedTarget).data('neta');
            var netd = $(e.relatedTarget).data('netd');
            modal.find('.modal-body #container').val(idContaioner);
            modal.find('.modal-body #NContainer').val(ncont);
            modal.find('.modal-body #Nhbl').val(hbl);
            modal.find('.modal-body #ETA').val(neta);
            modal.find('.modal-body #ETD').val(netd);
            

        });

        // se activa cuando el modal para Editar está a punto de ser mostrado.
        $('#myModalEdit').on('show.bs.modal', function (e) {
       
            var modal = $(this);
            

            // obtener el atributo del elemento pulsado.
            $("#fav-title").html($(e.relatedTarget).data('title'));
            var idoperd = $(e.relatedTarget).data('idoped');
            var cn = $(e.relatedTarget).data('cn');
            var cs = $(e.relatedTarget).data('cs');
            var m = $(e.relatedTarget).data('m');
            var ct = $(e.relatedTarget).data('ct');
            var w = $(e.relatedTarget).data('w');
            var v = $(e.relatedTarget).data('v');
            modal.find('.modal-body #id_operd').val(idoperd);
            modal.find('.modal-body #container_number').val(cn);
            modal.find('.modal-body #container_seal').val(cs);
            modal.find('.modal-body #mark').val(m);
            modal.find('.modal-body #weight').val(w);
            modal.find('.modal-body #volume').val(v);
            modal.find(".modal-body #container_type option[value='"+ct+"']").attr('selected', 'selected');
            modal.find('.modal-body #id_detailo').val(idoperd);
            

            

        });

        
});

</script>

@stop
@yield('scripts')

