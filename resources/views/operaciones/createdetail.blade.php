@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Operations<small>Operations Registry</small></h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('operaciones') }}">Operations</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

@if (\Session::has('msg'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif

    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> ¡Alert!</h5>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="col-md-6"> 
                        <h3 class="box-title"><i class="fa fa-plus" aria-hidden="true"></i> New Operation </h3>
                    </div>
                    <div class="col-md-6 text-right"> 
                      <h3 class="box-title"> Details Operation / Step <button type="button" class="btn btn-success btn-circle"> 2 </button></h3>
                    </div>     
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                {{ Form::open(array('url' => 'operaciones/addDetails')) }}


                <div class="box-body">
                
                
                    
                    
                    
                    
                    <div class="col-md-12">
                        <h3 class="box-title" id="details-text"><i class="fa fa-archive" aria-hidden="true"></i> Container Detail</h3>
                        <p>Fields marked with <b>*</b> are required.</p>
                    </div>
                    
                    <div class="row">
                                <div class="col-md-2 text-center" id="dt-ct-ct"><b># Ctnr *</b></div>
                                <div class="col-md-2 text-center" id="dt-ct-sl"><b>Ctnr Seal</b></div>
                                <div class="col-md-2 text-center" id="dt-ct-mk"><b>Marks</b></div>
                                <div class="col-md-1 text-center" id="dt-ct-tc"><b>Type Ctnr</b></div>
                                <div class="col-md-2 text-center" id="dt-ct-we"><b>Weight (Kgs) *</b></div>
                                <div class="col-md-2 text-center" id="dt-ct-cwe"><b>Chargable Weight (Kgs) *</b></div>
                                <div class="col-md-2 text-center" id="dt-ct-vo"><b>Volume (CBM) *</b></div>
                                <div class="col-md-1 text-center" id="dt-ct-ac"><b>Action</b></div>
                        </div>

                    <div class="input_fields_wrap">
                     
                    
                @if( old('container_number') )   

                    @for($i=0; $i <= count(old('container_number'))-1; $i++)

    



                    <div class="row" style="padding-top: 10px;padding-bottom: 10px;">
                                <div class="col-md-2" id="dt-ct-ct-input"><input name="container_number[]" type="text" maxlength="15" size="15" requerid="requerid" class="form-control" placeholder="# *" value="{{ old('container_number.'.$i) }}" ></div>
                                <div class="col-md-2" id="dt-ct-sl-input"><input name="container_seal[]"  type="text" maxlength="15" size="15" class="form-control" placeholder="Ctnr Seal" value="{{ old('container_seal.'.$i) }}"></div>
                                <div class="col-md-2" id="dt-ct-mk-input"><input name="mark[]" type="text" maxlength="50" size="50"  requerid="requerid" class="form-control" placeholder="Mark" value="{{ old('mark.'.$i) }}"></div>
                                <div class="col-md-1" id="dt-ct-tc-input">{{ Form::select('container_type[]',$contenedores,old('container_type.'.$i),[
                            'class' => 'form-control','placeholder' => 'Type Ctnr', 'requerid' => 'requerid'],array('name'=>'contenedores[]'))}}</div>
                                <div class="col-md-2" id="dt-ct-we-input"><input name="weight[]" requerid="requerid" type="number" class="form-control" step='any' placeholder="Weight *" value="{{ old('weight.'.$i) }}"></div>
                                <div class="col-md-2" id="dt-ct-cwe-input"><input name="chargable_weight[]" requerid="requerid" type="number" class="form-control" step='any' placeholder="*Chargable Weight *" value="{{ old('chargable_weight.'.$i) }}"></div>
                                <div class="col-md-2" id="dt-ct-vo-input"><input name="volume[]" requerid="requerid" type="number" class="form-control" step='any' placeholder='0.00' value="{{ old('volume.'.$i) }}"></div>
                                
                                
                        </div>
                    @endfor
                @endif
                
                @if( old('container_number') == NULL )
                     <div class="row" style="padding-top: 10px;padding-bottom: 10px;">
                                <div class="col-md-2" id="dt-ct-ct-input"><input name="container_number[]" type="text" maxlength="15" size="15" requerid="requerid" class="form-control" placeholder="# *" ></div>
                                <div class="col-md-2" id="dt-ct-sl-input"><input name="container_seal[]"  type="text" maxlength="15" size="15" class="form-control" placeholder="Ctnr Seal"></div>
                                <div class="col-md-2" id="dt-ct-mk-input"><input name="mark[]" type="text" maxlength="50" size="50"  requerid="requerid" class="form-control" placeholder="Mark"></div>
                                <div class="col-md-1" id="dt-ct-tc-input">{{ Form::select('container_type[]',$contenedores,null,[
                            'class' => 'form-control','placeholder' => 'Type Ctnr', 'requerid' => 'requerid'],array('name'=>'contenedores[]'))}}</div>
                                <div class="col-md-2" id="dt-ct-we-input"><input name="weight[]" requerid="requerid" type="number" class="form-control" placeholder="Weight *" step='any'></div>
                                <div class="col-md-2" id="dt-ct-cwe-input"><input name="chargable_weight[]" requerid="requerid" type="number" class="form-control" placeholder="*Chargable Weight *" step='any'></div>
                                <div class="col-md-2" id="dt-ct-vo-input"><input name="volume[]" requerid="requerid" type="number" class="form-control" step='any' placeholder='0.00'></div>
                                <div class="col-md-1 text-left">
                                  <span id="botonadd"> <button class="btn btn-primary add_field_button"><i class="fa fa-plus-circle" aria-hidden="true"></i> </button></span>              
                                </div>
                        </div>
                    @endif    
                        
                        

                    

                </div>
                <div class="box-footer  text-right">
                    
                {!! Form::hidden('operationID', $operationID) !!}
                {!! Form::hidden('tipo_operacion', decrypt($tipo_operacion)) !!}
                {!! Form::hidden('operacion_detail', $operacion_detail) !!}
                {!! Form::hidden('email_send', encrypt(1)) !!}
                {{ Form::button('<i class="fa fa-save" aria-hidden="true"></i> Save', ['class' => 'btn-lg btn-primary', 'type' => 'submit']) }}
                </div>
                {{ Form::close() }}
            </div>
            <!-- /.box -->

        </div>
        <!--/.col (left) -->

    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection
@yield('content')
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $("#botonadd" ).hide();
        var inputValue = "{{decrypt($tipo_operacion)}}";
        if(inputValue == 1){
                $('#dt-ct-ct').empty(); $('#dt-ct-ct').append("<b># Ctnr *</b>");
                $('#dt-ct-sl').empty(); $('#dt-ct-sl').append("<b>Ctnr Seal</b>");
                $('#dt-ct-mk').empty(); $('#dt-ct-mk').append("<b>Marks</b>");
                $('#dt-ct-tc').empty(); $('#dt-ct-tc').append("<b>Type Ctnr</b>");
                $('#dt-ct-ct').show(); 
                $('#dt-ct-sl').show();  
                $('#dt-ct-mk').show();
                $('#dt-ct-tc').show(); 
                $('#dt-ct-ct-input').show(); 
                $('#dt-ct-sl-input').show();
                $('#dt-ct-mk-input').show(); 
                $('#dt-ct-tc-input').show();
                $("[id='dt-ct-cwe']").hide(); 
                $("[id='dt-ct-cwe-input']").hide();
                

               
                    var tipo_detail = "{{decrypt($operacion_detail)}}";
                    if(tipo_detail == 2)
                    {
                        $("#botonadd").show();
                    }
                    else{
                        $("#botonadd").hide();  
                    }
            }
            else if(inputValue == 2){
                $('#dt-ct-ac').hide(); 
                $("#botonadd").hide(); 
                $('#dt-ct-ct').hide(); 
                $('#dt-ct-sl').hide(); 
                $('#dt-ct-mk').hide();
                $('#dt-ct-tc').hide(); 
                $('#dt-ct-ct-input').hide(); 
                $('#dt-ct-sl-input').hide();
                $('#dt-ct-mk-input').hide(); 
                $('#dt-ct-tc-input').hide(); 
                $('#dt-ct-cwe').show();
                $('#dt-ct-cwe-input').show();            
            }  
            else if(inputValue == 3){
                $("#botonadd").show(); 
                $('#dt-ct-ct').empty();
                $('#dt-ct-ct').show(); 
                $('#dt-ct-ct').append("<b># Box *</b>"); 
                $('#dt-ct-ct-input').show();
                $("[id='dt-ct-sl']").hide(); 
                $("[id='dt-ct-mk']").hide();
                $("[id='dt-ct-tc']").hide();
                $("[id='dt-ct-sl-input']").hide(); 
                $("[id='dt-ct-mk-input']").hide();
                $("[id='dt-ct-tc-input']").hide();
                $("[id='dt-ct-cwe']").hide(); 
                $("[id='dt-ct-cwe-input']").hide();

               
            }
            else{
                $("#botonadd" ).hide();
            }



        
        /** dynamic inputs details contenedor**/

        var max_fields = 15; //maximum input boxes allowed
            var wrapper = $(".input_fields_wrap"); //Fields wrapper
            var add_button = $(".add_field_button"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment

                //alert("alicia");
                
                var changediv = "{{decrypt($tipo_operacion)}}";
                if(changediv == 1){
                    $(wrapper).append('<div class="row"><div class="col-md-2" id="dt-ct-ct-input"><input name="container_number[]" requerid="requerid" type="text" class="form-control" placeholder="# Ctnr"></div><div class="col-md-2" id="dt-ct-sl-input"><input name="container_seal[]" type="text" class="form-control" placeholder="Ctnr Seal"></div><div  id="dt-ct-mk-input" class="col-md-2"><input name="mark[]" type="text" class="form-control" placeholder="Mark"></div><div id="dt-ct-tc-input" class="col-md-1">{{ Form::select("container_type[]",$contenedores,null,["class" => "form-control","placeholder" => "Type Ctnr"],array("name"=>"contenedores[]"))}}</div><div id="dt-ct-we-input" class="col-md-2"><input name="weight[]" step=any requerid="requerid" type="number" class="form-control" placeholder="Weight *"></div><div id="dt-ct-vo-input" class="col-md-2"><input name="volume[]" requerid="requerid" type="number" class="form-control" step="any" placeholder="0.00"></div><div style="cursor:pointer;margin-left: 15px;" class="remove_field btn btn-danger"><i class="fa fa-minus-circle" aria-hidden="true"></i></div></div>');
                }else if(changediv == 2){
                    $(wrapper).append('<div class="row"><div class="col-md-2"><input name="weight[]" requerid="requerid" type="number" class="form-control" step=any placeholder="Weight *"></div><div class="col-md-2" id="dt-ct-cwe-input"><input name="chargable_weight[]" requerid="requerid" type="number" class="form-control" placeholder="*Chargable Weight *"></div><div class="col-md-2"><input name="volume[]" requerid="requerid" type="number" class="form-control" step="any" placeholder="0.00"></div><div style="cursor:pointer;margin-left: 15px;" class="remove_field btn btn-danger"><i class="fa fa-minus-circle" aria-hidden="true"></i></div></div>');

                }else if(changediv == 3){
                    $(wrapper).append('<div class="row"><div class="col-md-2" id="dt-ct-ct-input"><input name="container_number[]" requerid="requerid" type="text" class="form-control" placeholder="# Box *"></div><div class="col-md-2" id="dt-ct-cwe-input"><input name="weight[]" step=any requerid="requerid" type="number" class="form-control" placeholder="*Weight *"></div><div id="dt-ct-vo-input" class="col-md-2"><input name="volume[]" requerid="requerid" type="number" class="form-control" step="any" placeholder="0.00"></div><div style="cursor:pointer;margin-left: 15px;" class="remove_field btn btn-danger"><i class="fa fa-minus-circle" aria-hidden="true"></i></div></div>');
                    
                }else{
                    $(wrapper).hide(); 
                }


                

    
            }
        });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
            })
            
        /** Fin dynamic text input**/ 

    });

</script>    
@stop
@yield('scripts')

