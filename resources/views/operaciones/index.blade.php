@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Operations<small>Operations Catalog</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Operations</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

  @if(session()->has('flash_message'))
  <div class="alert alert-success alert-dismissible fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Success!</h4>
    {{ session()->get('flash_message') }}
  </div>
  @endif

  @if(session()->has('Error'))
  <div class="alert alert-warning alert-dismissible fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Warning!</h4>
    {{ session()->get('Error') }}
  </div>
  @endif

  @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> ¡Alert!</h5>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif

  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-exchange"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Operations</span>
          <span class="info-box-number">{{ $operaciones }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-check"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Operations Enabled</span>
          <span class="info-box-number">{{ $operacionesEnabled }}</span>
          <a href="#" data-filter=".property-ok" class="click-ok"><span>View Enabled <i class="fa fa-arrow-circle-right"></i></span></a>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-trash"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Operations Deleted</span>
          <span class="info-box-number">{{ $operacionesDisabled }}</span>
          <a href="#" data-filter=".property-sale" class="click"><span>View Deleted <i class="fa fa-arrow-circle-right"></i></span></a>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    @if(Auth::user()->admin == 1 || Auth::user()->admin == 2)
    <!-- ./col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-share" aria-hidden="true"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Operations Shared</span>
          <span class="info-box-number">{{ $operacionesShared }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
    @endif 
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Operations List</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="operations-table" class="table table-striped table-bordered dt-responsive"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      @can('Crear')
      <a href="{{ URL::to('operaciones/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Add Operation</a>
      @endcan
    </div>
    <!-- /.col -->
  </div>

  <!-- Delete Model -->
  <div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Delete Operation</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Please confirm that you want to do with the operation: <b><span id="fav-title"></span></b>?
          <hr>
          <form action="{{ route('operaciones.destroy','1') }}" id="delForm" method="post">
            {{method_field('delete')}} {{csrf_field()}}
            <div class="form-group">
                        <div class="col-md-12">{{ Form::label('select', 'Select Option*') }}</div>
                        <div class="col-md-6">
                            <input type="radio" value="{{encrypt('3')}}" name="status" required class="form-radio">
                            <label> Cancel </label>
                        </div>
                        <div class="col-md-6">
                            <input type="radio" value="{{encrypt('0')}}" name="status" class="form-radio">
                            <label> Remove </label>
                        </div>
            </div> 
            <div class="form-group">
            <hr/>
            {{ Form::label('comments', 'Comments ') }}
            {!! Form::textarea('comments', null, ['class'=>'form-control','rows' => '2']) !!}
            </div>           
            <hr>

            <input name="idoperacion" id="operacion" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" value="delete" class="btn btn-success">Ok</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Delete Model -->


  <!-- hbl edit Model -->
  <div id="confirmHBLEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Edit HBL of Operation</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          Enter the new HBL of this operation: <b><span id="fav-title"></span></b>
          <hr>
          <form action='{{ action("OperacionController@updateHBL") }}' method="post">
            {{csrf_field()}}
            <input name="hbl" type="text" class="form-control" />
            <input name="operationid" id="operationid" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" value="delete" class="btn btn-success">Ok</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.hbl edit Model -->

  <!-- Telex edit Model -->
  <div id="confirmTelexComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Edit Telex of Operation</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          
          <form action='{{ action("OperacionController@telex") }}' method="post">
            {{csrf_field()}}
            <div class="form-group">
            ¿Please confirm if Telex exists for this operation 
            <b><span id="hbl"></span></b>?
            </div>
            <input name="operationid" id="operacionid" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" value="delete" class="btn btn-success">Accept</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Telex edit Model -->

  <!-- Restore Pre Operation edit Model -->
  <div id="confirmRestore" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Restore Pre-Operation</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
        ¿Please Confirm if you want to restore this pre operation?
          <form action='{{ action("OperacionController@restorePreOperation") }}' method="post">
            {{csrf_field()}}
            <input name="operacionid" id="operacionid" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" value="delete" class="btn btn-success">Restore</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Restore Pre Operation edit Model -->


</section>

<script>
  $(document).ready(function () {
    // se activa cuando el modal está a punto de ser mostrado.
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      // obtener el atributo idEmpresa del elemento pulsado.
      $("#fav-title").html($(e.relatedTarget).data('title'));
      var idEmpresa = $(e.relatedTarget).data('operacion');
      modal.find('.modal-body #operacion').val(idEmpresa);
    });

    $('#confirmTelexComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      $("#hbl").html($(e.relatedTarget).data('hbl'));
      var operacionID = $(e.relatedTarget).data('operacionid');
      console.log(operacionID);

      modal.find('.modal-body #operacionid').val(operacionID);
    });

    $('#confirmRestore').on('show.bs.modal', function (e) {
      var modal = $(this);

      var id = $(e.relatedTarget).data('operacionid');
      modal.find('.modal-body #operacionid').val(id);
    });


    $('#confirmHBLEdit').on('show.bs.modal', function (e) {
      var modal = $(this);

      // obtener el atributo idEmpresa del elemento pulsado.
      $("#fav-title").html($(e.relatedTarget).data('title'));
      var idEmpresa = $(e.relatedTarget).data('operacion');
      modal.find('.modal-body #operationid').val(idEmpresa);
    });
  });

  $(function () {
    $('.click').click(function () {
      $("#example").dataTable().fnDestroy();
      loadTable('0'); 
    });
    $('.click-ok').click(function () {
      $("#example").dataTable().fnDestroy();
      loadTable('1'); 
    });
  });
  
  $(function () {
    loadTable('1');
  });

  function loadTable(eliminados) {
    $('#operations-table').DataTable({
      processing: true,
      serverSide: true,
      destroy: true,
      type: 'GET',
      ajax: "{{ route('get.operaciones') }}?showDeleted="+eliminados,
      language: {
        "search": "Search",
        "lengthMenu": "Show _MENU_ per page",
        "zeroRecords": "Sorry, we can't find what you're looking for",
        "info": "Showing page _PAGE_ of _PAGES_ of _TOTAL_ records",
        "infoEmpty": "Records not found",
        "infoFiltered": "(Filtered in _MAX_ total records)",
        "paginate": {
          "previous": "Previous",
          "next": "Next",
        },
      },
      columns: [
        {data: 'DT_RowIndex', name: 'DT_RowIndex', title: '#'},
        { data: 'folio', name: 'folio', title: '#Tracking' },
        { data: 'hbl', name: 'hbl', title: 'HBL' },
        { data: 'mbl', name: 'mbl', title: 'MBL' },
        { data: 'tipo_operacion', name: 'tipo_operacion', title: 'Operation' },
        { data: 'consignee_name', name: 'consignee_name', title: 'Consignee' },
        { data: 'ETD', name: 'ETD', title: 'ETD' },
        { data: 'ETA', name: 'ETA', title: 'ETA' },
        {
          data: 'activo', name: 'activo', title: 'Status', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<div class="iradio_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Enabled';
            }else if (data == 2) {
              return '<small class="label pull-center bg-green"><i class="fa fa-check" aria-hidden="true"></i> Finish</small>';
            }else if (data == 3) {
              return '<small class="label pull-center bg-yellow"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</small>';
            }else if (data == 0) {
              return '<small class="label pull-center bg-red"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</small>';
            }else {
              return '<div class="iradio_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Deleted';
            }
          }
        },
        @if(Auth::user()->admin == 1 || Auth::user()->admin == 2)
        {
          data: 'share', name: 'share', title: 'Share', "render": function (data, type, full, meta) {
            if (data  > 0) {
              return '<i class="fa fa-share" aria-hidden="true"></i> shared';
            } else {
              return '';
            }
          }
        },
        @endif
        @if(Auth::user()->admin == 1 || Auth::user()->admin == 2)
        { data: 'creator', name: 'creator', title: 'Users' },
        @endif
        { data: 'telex', name: 'telex', title: 'Telex' },
        { data: 'action', name: 'action', title: 'Action', orderable: false, searchable: false }
      ]
    });
  }
</script>
</section>
@endsection
