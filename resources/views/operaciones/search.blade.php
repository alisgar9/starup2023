@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Filter<small> Container or Box</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Filter</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

  @if(session()->has('flash_message'))
  <div class="alert alert-success alert-dismissible fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Success!</h4>
    {{ session()->get('flash_message') }}
  </div>
  @endif

  @if(session()->has('error_message'))
  <div class="alert alert-danger alert-dismissible fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
    {{ session()->get('error_message') }}
  </div>
  @endif

  @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> ¡Alert!</h5>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif


  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-archive"></i></span>
        <div class="info-box-content">
          <span class="info-box-text"># Cntnr or Box</span>
          <span class="info-box-number">{{ $contenedores }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-clock-o"></i></span>
        <div class="info-box-content">
          <span class="info-box-text"># Cntnr or Box Enabled</span>
          <span class="info-box-number">{{ $contenedoreEnabled }}</span>
          <a href="#" onclick="mifuncion(this)" data-value="1" class="click"><span>View Box Enabled <i class="fa fa-arrow-circle-right"></i></span></a>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-refresh"></i></span>
        <div class="info-box-content">
          <span class="info-box-text"># Return Empty Cntnr or Box</span>
          <span class="info-box-number">{{ $contenedorReturnEmpty }}</span>
          <a href="#" onclick="mifuncion(this)" data-value="2" class="click"><span>View Return Empty <i class="fa fa-arrow-circle-right"></i></span></a>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-trash"></i></span>
        <div class="info-box-content">
          <span class="info-box-text"># Cntnr or Box Deleted</span>
          <span class="info-box-number">{{ $contenedoreDisabled }}</span>
          <a href="#" onclick="mifuncion(this)" data-value="0" class="click"><span>View Deleted <i class="fa fa-arrow-circle-right"></i></span></a>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Container or Box List</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body pre-scrollable">
          <table id="usuarios-table" class="table table-striped table-bordered dt-responsive"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </div>
    <!-- /.col -->
  </div>

  
   <!-- return empty container Model -->
   <div id="confirmContainerComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Return Empty Container</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Confirm empty container return date: <b><span id="fav-title"></span> <b> to ETA:</b> <span id="fav-desc"></span></b>?
          <hr>
          <form id="editar-detalle-contenedor" action='{{ action("OperationDetails@emptyContainer") }}'  method="post">
            {{csrf_field()}}
            <div class="col-md-12">    
                            <div class="form-group">
                                {{ Form::label('empty_container', 'Date *') }}
                                {!! Form::date('empty_container', null, [
                                    'class' => 'form-control',
                                    'required' => 'required',]) !!}
                            </div>     
                        </div> 
            <input name="idContainer" id="container" type="hidden" />
            <input name="NContainer" id="NContainer" type="hidden" />
            <input name="ETD" id="ETD" type="hidden" />
            <input name="ETA" id="ETA" type="hidden" />
            <input name="Nhbl" id="Nhbl" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" value="delete" class="btn btn-success">Ok</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.return empty container Model -->


</section>

<script>
  
  $(document).ready(function () {
    // se activa cuando el modal está a punto de ser mostrado.
    $('#confirmContainerComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      // obtener el atributo idEmpresa del elemento pulsado.
      $("#fav-title").html($(e.relatedTarget).data('title'));
      $("#fav-desc").html($(e.relatedTarget).data('eta'));
      var idContaioner = $(e.relatedTarget).data('cont');
      var ncont = $(e.relatedTarget).data('ncont');
      var hbl = $(e.relatedTarget).data('title');
      var neta = $(e.relatedTarget).data('neta');
      var netd = $(e.relatedTarget).data('netd');
      modal.find('.modal-body #container').val(idContaioner);
      modal.find('.modal-body #NContainer').val(ncont);
      modal.find('.modal-body #Nhbl').val(hbl);
      modal.find('.modal-body #ETA').val(neta);
      modal.find('.modal-body #ETD').val(netd);
    });
  });

  function mifuncion(elemento) {
    var data = $(elemento).data('value');
    $("#usuarios-table").dataTable().fnDestroy();
    loadTable(data); 
  }



  
  
  $(function () {
    loadTable('1');
  });

  function loadTable(eliminados) {
    $('#usuarios-table').DataTable({
      processing: true,
      serverSide: true,
      responsive: true,
      type: 'GET',
      ajax: "{{ route('get.containers') }}?showDeleted="+eliminados,
      language: {
        "search": "Search",
        "lengthMenu": "Show _MENU_ records per page",
        "zeroRecords": "Sorry, we can't find what you're looking for",
        "info": "Showing page _PAGE_ of _PAGES_ of _TOTAL_ records",
        "infoEmpty": "Records not found",
        "infoFiltered": "(Filtered in _MAX_ total records)",
        "paginate": {
          "previous": "Previous",
          "next": "Next",
        },
      },
      columns: [
        {
          data: 'freeDays', name: 'freeDays', title: 'Free Days', className: "text-center", "render": function (data, type, full, meta) {
            if (data <= 5) {
              return '<i class="fa fa-flag" style="color:red" aria-hidden="true"></i> ' + data + ' Day(s)';
            }
            else {
              return '<i class="fa fa-flag" style="color:green" aria-hidden="true"></i> ' + data + ' Day(s)';
            }
          }
        },
        { data: 'PI', name: 'PI', title: '# PI' },
        { data: 'PO', name: 'PO', title: '# PO' },
        { data: 'customer', name: 'customer', title: 'Customer' },
        { data: 'container_number', name: 'container_number', title: '# Ctnr' },
        { data: 'POL', name: 'POL', title: 'POL' },
        { data: 'POD', name: 'POD', title: 'POD' },
        { data: 'ETD', name: 'ETD', title: 'ETD Date' },
        { data: 'ETA', name: 'ETA', title: 'ETA Date' },
        { data: 'hbl', name: 'hbl', title: 'HBL' },
        { data: 'mbl', name: 'mbl', title: 'MBL' },
        { data: 'shippingline', name: 'shippingline', title: 'Line' },
        { data: 'empty_container', name: 'empty_container', title: 'Empty Return' },
        {
          data: 'activo', name: 'activo', title: 'Status', className: "text-center", "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<i class="fa fa-clock-o" aria-hidden="true"></i> Created';
            } 
            else if (data == 2) {
              return '<i class="fa fa-refresh" aria-hidden="true"></i> Empty Returned';
            }
            else {
              return '<i class="fa fa-trash" aria-hidden="true"></i> Deleted';
            }
          }
        },
        { data: 'action', name: 'action', title: 'Action', orderable: false, searchable: false }
      ]
    });
  }
</script>
</section>
@endsection
