@extends('adminlte::layouts.app')
<link href="{{ URL::asset('/css/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />





@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Operations<small>Operations Registry</small></h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('operaciones') }}">Operations</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

@if (\Session::has('msg'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif

    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> ¡Alert!</h5>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="col-md-6"> 
                        <h3 class="box-title"><i class="fa fa-plus" aria-hidden="true"></i> New Operation </h3>
                    </div>
                    <div class="col-md-6 text-right"> 
                      <h3 class="box-title"> Profile Operation / Step <button type="button" class="btn btn-success btn-circle"> 1 </button></h3>
                    </div>     
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                {{ Form::open(array('url' => 'operaciones')) }}

               

                <div class="box-body">
                
                <div class="col-md-12">
                    <h4><i class="fa fa-bell" aria-hidden="true"></i> Fields Marked With <b>*</b> Are Required</h4>
                </div>    
                <br>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="col-md-12">
                                {{ Form::label('operacion', 'Operation  Type *') }}
                            </div>    
                            <div class="form-group">
                                <div class="col-md-4">    
                                <input type="radio" name="operacion" value="1" required id="import" class="form-radio" {{old('operacion')==1 ? 'checked='.'"'.'checked'.'"' : '' }} > <label for="import" class="textsmall">Import</label>
                                </div>
                                <div class="col-md-4"> 
                                <input type="radio" name="operacion" value="2" id="export" class="form-radio" {{old('operacion')==2 ? 'checked='.'"'.'checked'.'"' : '' }}> <label for="export" class="textsmall">Export </label>
                                </div>
                                <div class="col-md-4"> 
                                <input type="radio" name="operacion" value="3" id="domestic" class="form-radio" {{old('operacion')==3 ? 'checked='.'"'.'checked'.'"' : '' }}> <label for="domestic" class="textsmall">Domestic</label>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">
                                {{ Form::label('hbl', 'Operation') }}
                            </div>    
                            <div class="form-group">
                                <div class="col-md-4">    
                                <input type="radio" name="tipo_operacion" value="1" required id="maritima" class="form-radio" {{old('tipo_operacion')==1 ? 'checked='.'"'.'checked'.'"' : '' }}> <label class="textsmall" for="maritima"><i class="fa fa-ship" aria-hidden="true"></i> Ocean</label>
                                </div>
                                <div class="col-md-4"> 
                                <input type="radio" name="tipo_operacion" value="2" id="aerea" class="form-radio" {{old('tipo_operacion')==2 ? 'checked='.'"'.'checked'.'"' : '' }}> <label class="textsmall" for="aerea"><i class="fa fa-plane" aria-hidden="true"></i> Air </label>
                                </div>
                                <div class="col-md-4"> 
                                <input type="radio" name="tipo_operacion" value="3" id="terrestre" class="form-radio" {{old('tipo_operacion')==3 ? 'checked='.'"'.'checked'.'"' : '' }}> <label class="textsmall" for="terrestre"><i class="fa fa-truck" aria-hidden="true"></i> In land</label>
                                </div>
                        </div>
                        <div class="col-md-12" id="div-maritima" style='display:none'></div>
                        <div class="col-md-12" id="div-terrestre" style='display:none'></div> 
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                {{ Form::label('cy', 'Service Mode *') }}
                            </div>    
                            <div class="form-group">
                                <div class="col-md-4">    
                                <input type="radio" name="cy" value="1" required id="cycy" {{old('cy')==1 ? 'checked='.'"'.'checked'.'"' : '' }} class="form-radio"> <label class="textsmall" for="cycy">CY - CY</label>
                                </div>
                                <div class="col-md-4"> 
                                <input type="radio" name="cy" value="2" id="cydoor" {{old('cy')==2 ? 'checked='.'"'.'checked'.'"' : '' }} class="form-radio"> <label class="textsmall" for="cydoor">CY - Door </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="div-cydoor"> </div>
                        </div>

                    </div>
                   
                
                </div>    
                <div class="col-md-12"></div>
                    
                <div class="col-md-12"><br></div>

                        

                        

                    <div class="col-md-12">
                        <hr size="30"><br/>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12">
                            <i class="fa fa-share" aria-hidden="true"></i>
                            {{ Form::label('share', 'Share Operation ') }}<br>
                            {{ Form::select('share[]',$operadores,null,[
                                'multiple' =>'multiple', 'id' =>'demo'],array('name'=>'share[]'))}}
                                <br/><br/><br/>
                        </div>
                    </div>
                     
                    <div class="col-md-12">
                        
                        <div class="col-md-3">
                        <i class="fa fa-file" aria-hidden="true"></i>
                            {{ Form::label('po', 'PO') }}<br>
                            {!! Form::text('PO',old('PO'), [
                                'data-role' => 'tagsinput',
                                'class' => 'form-control'
                                ]) !!}
                        </div>
                        <div class="col-md-3">
                            {{ Form::label('pi', 'PI') }}<br>
                            {!! Form::text('PI',old('PI'), [
                                'data-role' => 'tagsinput',
                                'class' => 'form-control'
                                ]) !!}
                                
                        </div>
                        <div class="col-md-3">
                            {{ Form::label('invoice', '# Invoice') }}
                            {!! Form::text('factura',old('factura'), [
                                'class' => 'form-control',
                                'maxlength' =>'255', 'size'=>'255'
                                ]) !!}
                        </div>
                        <div class="col-md-3">
                            {{ Form::label('sc', 'SC') }}
                            {!! Form::text('sc',old('sc'), [
                                'class' => 'form-control',
                                'maxlength' =>'255', 'size'=>'255'
                                ]) !!}
                        </div>
                        <br/>    
                    </div>
                    <div class="col-md-12">
                            <div class="col-md-10"> 
                                <br />
                                Enable the box, only if the operation does not have HBL (Option valid only to edit HBL).
                                <input type="radio" requerid name="pre_operation" value="1" id="pre_operation" {{old('pre_operation')==1 ? 'checked='.'"'.'checked'.'"' : '' }} class="form-radio"> <label for="pre_operation">Yes</label>
                                <input type="radio" requerid name="pre_operation" value="0" id="pre_operationNO" {{old('pre_operation')==0 ? 'checked='.'"'.'checked'.'"' : '' }} class="form-radio"> <label for="pre_operationNO">No Pre Operation</label>

                                <br /> 
                            </div>
                            <br /><br />
                            <hr />
                             
                    </div>    
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('hbl', 'House (HBL)',['id' => 'hbl-text']) }}
                                {!! Form::text('hbl',old('hbl'), [
                                'class' => 'form-control',
                                'maxlength' =>'25', 'size'=>'25'
                                ]) !!}
                            </div>
                        </div>   
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('mbl', 'MBL (MBL) ',['id' => 'mbl-text']) }}
                                {!! Form::text('mbl', null, [
                                'class' => 'form-control',
                                'maxlength' =>'25', 'size'=>'25'
                                ]) !!}

                                
                            </div>     
                        </div> 
                        <div class="col-md-3">    
                            <div class="form-group" id="view-HRM">
                                {{ Form::label('house_release', 'House Release Mode') }}
                                {{ Form::select('house_release',$LHBL,null,[
                            'class' => 'form-control','placeholder' => 'Choose one...'],array('name'=>'LHBL[]'))}}
                            </div>     
                        </div>

                        <div class="col-md-3" id="view-MRM">    
                            <div class="form-group">
                                {{ Form::label('master_release', 'Master Release Mode') }}
                                {{ Form::select('master_release',$LMBL,null,[
                            'class' => 'form-control','placeholder' => 'Choose one...'],array('name'=>'LMBL[]'))}}
                            </div>     
                        </div> 
                    </div>
                    <div class="col-md-12">
                    <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('ETD', 'ETD *') }}
                                {!! Form::date('ETD', null, [
                                    'class' => 'form-control',
                                    'required' => 'required',]) !!}
                            
                            </div>     
                        </div>

                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('ETA', 'ETA *') }}
                                {!! Form::date('ETA', null, [
                                    'class' => 'form-control',
                                    'required' => 'required',]) !!}
                            </div>     
                        </div>
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('ATD', 'ATD *') }}
                                {!! Form::date('ATD', null, [
                                    'class' => 'form-control',
                                    'required' => 'required',]) !!}
                            
                            </div>     
                        </div>

                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('ATA', 'ATA *') }}
                                {!! Form::date('ATA', null, [
                                    'class' => 'form-control',
                                    'required' => 'required',]) !!}
                            </div>     
                        </div>
                    </div>
                    <div class="col-md-12">   
                        
                        <div class="col-md-3" id="view-vessel-div">    
                            <div class="form-group" id="view-vessel">
                                {{ Form::label('vessel', 'Vessel *',['id' => 'vessel-text']) }}
                                {!! Form::text('vessel', null, [
                                'class' => 'form-control',
                                'maxlength' =>'50', 'size'=>'50'
                                ]) !!}
                            </div>     
                        </div> 

                        <div class="col-md-3" id="voyage">    
                            <div class="form-group">
                                {{ Form::label('vogage', 'Voyage', ['id' => 'vogage-text']) }}
                                {!! Form::text('vogage', null, [
                                'class' => 'form-control','maxlength' =>'25', 'size'=>'25'
                                ]) !!}
                            </div>     
                        </div> 
                    </div>
                    <div class="col-md-12">    
                        <div class="col-md-3">    
                            <div class="form-group">
                            <label for="POL" class="control-label" id="POL-text">Select Origin Port *</label>
                            <input id="basics" type="text" name="Spol" value="{{old('Spol')}}"  class="form-control" placeholder="Search" />
                            <input type="hidden" id="POL" name="POL" value="{{old('POL')}}">
                            </div>     
                        </div> 
                        <div class="col-md-3">    
                            <div class="form-group">
                            <label for="POD" class="control-label" id="POD-text">Select Destination Port *</label>
                            <input id="podsearch" type="text" name="Spod" value="{{old('Spod')}}" class="form-control" placeholder="Search" />
                            <input type="hidden" id="POD" name="POD" value="{{old('POD')}}">
                            </div>     
                        </div> 
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('final_dest', 'Final Dest. ') }}
                                {!! Form::text('final_dest', null, [
                                'class' => 'form-control',
                                ]) !!}
                            </div>     
                        </div> 
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('INCOTERM', 'INCOTERM ') }}
                                {{ Form::select('INCOTERM',$Incoterm,null,[
                            'class' => 'form-control','placeholder' => 'Choose one...'],array('name'=>'Incoterm[]'))}}
                            </div>     
                        </div>
                    </div>     
                    
                    <div class="col-md-12">
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('shipper', 'Shipper *') }}
                                <input id="shipper_desc" name="Sship" requerid value="{{old('Sship')}}" type="text"  class="form-control" placeholder="Search Shipper" />
                                <input type="hidden" id="shipper" requerid name="shipper" value="{{old('shipper')}}">
                                
                            </div>     
                        </div>
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('consignee', 'Consignee *') }}
                                <input id="consignee_desc" name="Scon" requerid="requerid" value="{{old('Scon')}}" type="text" class="form-control" placeholder="Search Consignee" />
                                <input type="hidden" id="consignee" requerid="requerid" name="consignee" value="{{old('consignee')}}">
                            </div>     
                        </div>
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('agente', 'Customs Broker ') }}
                                <input id="agente_desc" name="Scb" value="{{old('Scb')}}" type="text" class="form-control" placeholder="Search Customs Broker" />
                                <input type="hidden" id="agente" name="agente" value="{{old('agente')}}">
                            </div>     
                        </div>
                        <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('agente_envio', 'Forwarder Agent') }}
                                <input id="agente_envio_desc" name="Sfe" value="{{old('Sfe')}}" type="text" class="form-control" placeholder="Search Shipping Agents" />
                                <input type="hidden" id="agente_envio" name="agente_envio" value="{{old('agente_envio')}}">
                            </div>     
                        </div>
                    </div>
                    <div class="col-md-12" > 
                    <div class="col-md-3">    
                        <div class="form-group">
                                {{ Form::label('shipping_line', 'Shipping Line', ['id' => 'shl-text']) }}
                                <input id="shipping_line_desc" name="Slo" value="{{old('Slo')}}" type="text" class="form-control" placeholder="Search  Line" />
                                <input type="hidden" id="shipping_line" name="shipping_line" value="{{old('shipping_line')}}">
                            </div>     
                        </div>
                        <div class="col-md-3">    
                            <div class="form-group">
                            {{ Form::label('terminal', 'Terminal') }}
                            {!! Form::text('terminal', null, [
                                'class' => 'form-control']) !!}
                            
                            </div>     
                        </div>
                        <div class="col-md-3" id="HRD-div">    
                            <div class="form-group">
                                {{ Form::label('', 'House Release Date') }}
                                {!! Form::date('house_release_date', null, [
                                    'class' => 'form-control']) !!}
                            
                            </div>     
                        </div>
                        <div class="col-md-3" id="MRD-div">    
                            <div class="form-group">
                                {{ Form::label('master_release_date', 'Master Release Date') }}
                                {!! Form::date('master_release_date', null, [
                                    'class' => 'form-control']) !!}
                            
                            </div>     
                        </div>
                        
                        
                    </div>    
                    
                    <div class="col-md-12">
                       <div class="col-md-3">    
                            <div class="form-group">
                                {{ Form::label('commodity', 'Commodity ') }}
                                {!! Form::textarea('commodity', null, ['class'=>'form-control', 'rows' => '2']) !!}
                                
                            </div>     
                        </div>
                        <div class="col-md-3" id="FDDC-div">    
                            <div class="form-group">
                                {{ Form::label('free_demurrages_days_cust', 'Free dem days Customer *') }}
                                {!! Form::number('free_demurrages_days_cust', 21, [
                                'class' => 'form-control', 'requerid' => 'requerid', 'id' => 'free_demurrages_days_cust'
                                ]) !!}
                            
                            </div>     
                        </div>
                        <div class="col-md-3" id="FDDSL-div">    
                            <div class="form-group">
                                {{ Form::label('free_demurrages_days_sl', 'Free dem days SL *') }}
                                {!! Form::number('free_demurrages_days_sl', 21, [
                                'class' => 'form-control', 'requerid' => 'requerid', 'id' => 'free_demurrages_days_sl'
                                ]) !!}
                            
                            </div>     
                        </div>
                        <div class="col-md-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('dp', 'Demurrage Price *') }}
                                    {!! Form::number('precio_demora', 170.00, ['class'=>'form-control','required' => 'required', 'step'=>'0.01']) !!}
                                    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('dp', 'Demurrage % IVA *') }}
                                    {!! Form::number('iva', 0, ['class'=>'form-control','required' => 'required', 'step'=>'0.01']) !!}
                                    
                                </div>
                            </div>         
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('booking', 'Booking') }}
                                {!! Form::text('booking', null, ['class'=>'form-control','rows' => '3']) !!}
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('origin_contry', 'Origin Contry') }}
                                {{ Form::select('origin_contry',$pais,null,[
                                        'class' => 'form-control','placeholder' => 'seleccione uno...'],array('name'=>'cargos[]'))}}
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                            {{ Form::label('destination_contry', 'Destination Contry') }}
                                {{ Form::select('destination_contry',$pais,null,[
                                        'class' => 'form-control','placeholder' => 'seleccione uno...'],array('name'=>'cargos[]'))}}
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('booking_status', 'Booking Status') }}
                                {!! Form::text('booking_status', null, ['class'=>'form-control','rows' => '3']) !!}
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('expense_bl', 'Expense BL') }}
                                {!! Form::text('expense_bl', null, ['class'=>'form-control','rows' => '3']) !!}
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('expense_contr', 'Expense Contr') }}
                                {!! Form::text('expense_contr', null, ['class'=>'form-control','rows' => '3']) !!}
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('tarifa', 'Rate') }}
                                {!! Form::text('tarifa', null, ['class'=>'form-control','rows' => '3']) !!}
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                       <div class="col-md-6">    
                            <div class="form-group">
                                {{ Form::label('comments', 'Comments') }}
                                {!! Form::textarea('comments', null, ['class'=>'form-control','rows' => '3']) !!}
                                
                            </div>     
                        </div>
                    </div>                   
                </div>
                <div class="box-footer text-right">
                <a href="{{ url('operaciones') }}" class="btn-lg btn-danger"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</a>
                    {{ Form::button('<i class="fa fa-arrow-right" aria-hidden="true"></i> Next', ['class' => 'btn-lg btn-primary', 'type' => 'submit']) }}
                </div>
                {{ Form::close() }}
            </div>
            <!-- /.box -->

        </div>
        <!--/.col (left) -->

    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection
@yield('content')
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-angular.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">



    $(document).ready(function(){

        $('#demo').multiselect();

        $("#botonadd").hide();
        $('input[name="cy"]').click(function(){
            var inputValue = $(this).attr("value");
            if(inputValue == 2){
                $("#div-cydoor" ).append('<div class="col-md-12">{{ Form::label("cdt", "CY Door Type *") }}</div><div class="form-group"><div class="col-md-6">{!! Form::select("cy_door", array("" => "Choose one...","1" => "All Truck", "2" => "Train - Truck", "3" => "Train + Ramp"), old("cy_door"), ["class" => "form-control", "id" => "cdt"]); !!}</div></div>');
            }else{ $("#div-cydoor" ).empty(); }
        });
        

        $('input[name="tipo_operacion"]').click(function(){
            var inputValue = $(this).attr("value");
            if(inputValue == 1){
                $('#view-vessel-div').show(); 
                $('#hbl-text').text("House (HBL) *"); 
                $('#mbl-text').text("MBL (MBL)");
                $('#shl-text').text("shipping line"); 
                $('#vogage-text').text("Voyage");
                $('#vessel-text').text("Vessel *");
                $('#shl-text').text("Shipping Line");
                $('#voyage').show(); 
                $('#HRD-div').show(); 
                $('#MRD-div').show();
                $('#FDDC-div').show(); 
                $('#FDDSL-div').show();
                $('#free_demurrages_days_cust').val('21'); 
                $('#free_demurrages_days_sl').val('21');
                $('#view-HRM').show(); 
                $('#view-MRM').show(); 
                $("#div-terrestre").empty();
                $("#div-maritima").empty(); 
                $('#div-maritima').show();
                $('#POL-text').text("Select Origin Port *");
                $('#POD-text').text("Select Destination Port *");

                $("#div-maritima" ).append('<div class="col-md-12"><br>{{ Form::label("hbl", "Operation Profile *") }}</div><div class="form-group"><div class="col-md-6"><input type="radio" name="operacion_detail" {{old('operacion_detail')==2 ? 'checked='.'"'.'checked'.'"' : '' }}  value="2" id="fcl" class="form-radio"> <label class="text-small" for="fcl">FCL </label></div><div class="col-md-6"><input type="radio"  name="operacion_detail" {{old('operacion_detail')==1 ? 'checked='.'"'.'checked'.'"' : '' }} value="1" id="LCL" class="form-radio"> <label for="LCL" class="text-small">LCL </label></div></div>');
               
                $('input[name="operacion_detail"]').click(function(){
                    var tipo_detail = $(this).attr("value");
                    if(tipo_detail == 2)
                    {
                        $("#botonadd").show();
                    }
                    else{
                        $("#botonadd").hide();  
                    }
                });
            }
            else if(inputValue == 2){
                $('#view-vessel-div').hide(); 
                $("#div-terrestre").empty(); 
                $("#div-maritima").empty(); 
                $('#hbl-text').text("House (HWB) *");
                $('#mbl-text').text("MBL (MWB)"); 
                $('#shl-text').text("Air Line *");
                $('#voyage').show();
                $('#vogage-text').text("Flight"); 
                $('#free_demurrages_days_cust').val('3');
                $('#free_demurrages_days_sl').val('3'); 
                $('#view-HRM').hide();
                $('#view-MRM').hide(); 
                $('#POL-text').text("Select Origin Port *");
                $('#POD-text').text("Select Destination Port *");         
            }  
            else if(inputValue == 3){
                $('#vessel-text').text("Unit No. *");
                $('#shl-text').text("In land Carrier*"); 
                $('#details-text').empty();
                $('#details-text').append("<i class='fa fa-archive' aria-hidden='true'></i> Units Details");
                $('#voyage').hide(); 
                $('#HRD-div').hide(); 
                $('#MRD-div').hide(); 
                $('#FDDC-div').hide();
                $('#FDDSL-div').hide(); 
                $("#botonadd").show(); 
                $("#div-maritima").empty();
                $("#div-terrestre").empty(); 
                $('#div-terrestre').show(); 
                $('#view-vessel-div').show();
                $('#view-HRM').hide();
                $('#view-MRM').hide();
                $('#POL-text').text("Select Origin Port *");
                $('#POD-text').text("Select Destination Port *");
                
                
                $("#div-terrestre").append('<div class="col-md-12"><br>{{ Form::label("hbl", "Operation Profile *")}}</div><div class="form-group"><div class="col-md-6"><input type="radio"  required name="operacion_detail" value="1" {{old('operacion_detail')==1 ? 'checked='.'"'.'checked'.'"' : '' }} id="Nacional" class="form-radio"> <label for="Nacional" class="text-small">Nacional</label></div><div class="col-md-6"><input type="radio" name="operacion_detail" value="2" {{old('operacion_detail')==2 ? 'checked='.'"'.'checked'.'"' : '' }} id="Internacional" class="form-radio"> <label for="Internacional" class="text-small">Internacional </label></div></div>');
            }
            else{
                $("#div-terrestre").empty(); $("#div-maritima").empty(); 
                $("#botonadd" ).hide();
            }
        
        });

        

        /** Autocomplete **/

    $('#basics').ready(function(){
        $('input[name="tipo_operacion"]').click(function(){
            var inputValue = $(this).attr("value");
            puertoSearch("basics", "POL", inputValue);
        });
    });

    $('#podsearch').ready(function(){
        $('input[name="tipo_operacion"]').click(function(){
            var inputValue = $(this).attr("value");
            puertoSearch("podsearch", "POD", inputValue);
        });
    });

    $('#agente_desc').ready(function(){
        entidadSearch("agente", "agente_desc");
    });

    $('#shipper_desc').ready(function(){
        entidadSearch("shipper", "shipper_desc");
    });

    $('#consignee_desc').ready(function(){
        entidadSearch("consignee", "consignee_desc");
    });

    

    $('#agente_envio_desc').ready(function(){
        entidadSearch("agente_envio", "agente_envio_desc");
    });
    

    $('#shipping_line_desc').ready(function(){

        $('input[name="tipo_operacion"]').click(function(){
            var inputValue = $(this).attr("value");
            var valor = '';
            if(inputValue == 1){
                valor = 'shipping_line';

            }else if(inputValue == 2){
                valor = 'air_line';

            }else if(inputValue == 3){
                valor = 'carrier_line';
            }else{
                valor = 'shipping_line';
            }   

            shlentidadSearch(valor, "shipping_line_desc");   

        });
        
    });


    function puertoSearch(etiqueta, valor, type){
            $("#"+etiqueta).easyAutocomplete({
                url: function(search) {
                    return "{{route('autocomplete.fetch')}}?search=" + search + "&type=" + type;
                },

                getValue: "puerto",
                list: {
                        onSelectItemEvent: function() {
                            var puerto = $("#" + etiqueta).getSelectedItemData().id;
                            $("#"+valor).val(puerto);
                        
                        },
                        match: {
                            enabled: true
                        }
                }
            });
    }

    function entidadSearch(tipo,etiqueta){
        $("#"+etiqueta).easyAutocomplete({
            url: function(search) {
                return "{{route('autocomplete.entidad')}}?tipo=" + tipo +"&search=" + search;
            },
        
            getValue: "nombre",
            list: {
                    onSelectItemEvent: function() {
                        var dataID = $("#"+etiqueta).getSelectedItemData().id;
                        $("#"+tipo).val(dataID);
                    },
            match: {
                     enabled: true
                    }
            }
        });
    }

    function shlentidadSearch(tipo,etiqueta){

        $("#"+etiqueta).easyAutocomplete({
            url: function(search) {
                return "{{route('autocomplete.entidad')}}?tipo=" + tipo +"&search=" + search;
            },
        
            getValue: "nombre",
            list: {
                    onSelectItemEvent: function() {
                        var dataID = $("#"+etiqueta).getSelectedItemData().id;
                        $("#shipping_line").val(dataID);
                    },
            match: {
                     enabled: true
                    }
            }
        });
    }


    });

</script>    
@stop
@yield('scripts')
