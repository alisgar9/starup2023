@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>States <small>States Catalog</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">States</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

  @if(session()->has('flash_message'))
  <div class="alert alert-success alert-dismissible fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Success!</h4>
    {{ session()->get('flash_message') }}
  </div>
  @endif

  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">States</span>
          <span class="info-box-number">{{ $estados }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-check"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Enabled States</span>
          <span class="info-box-number">{{ $estadosEnabled }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-trash"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Eliminated States</span>
          <span class="info-box-number">{{ $estadosDisabled }}</span>
          <a href="#" data-filter=".property-sale" class="click"><span>See Deleted <i class="fa fa-arrow-circle-right"></i></span></a>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">List of States</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="areas-table" class="table table-striped table-bordered dt-responsive"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      @can('Crear')
      <a href="{{ URL::to('states/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Add State</a>
      @endcan
    </div>
    <!-- /.col -->
  </div>

  <!-- Delete Model -->
  <div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Delete State</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
        ¿Please confirm that you want to delete the State: <b><span id="fav-title"></span></b>?
          <hr>
          <form action="{{ route('states.destroy','1') }}" id="delForm" method="post">
            {{method_field('delete')}} {{csrf_field()}}
            <input name="idstate" id="state" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" value="delete" class="btn btn-success">Accept</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Delete Model -->
</section>

<script>
  $(document).ready(function () {
    // se activa cuando el modal está a punto de ser mostrado.
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      // obtener el atributo idEmpresa del elemento pulsado.
      $("#fav-title").html($(e.relatedTarget).data('title'));
      var idEmpresa = $(e.relatedTarget).data('puerto');
      modal.find('.modal-body #state').val(idEmpresa);
    });
  });

  $(function () {
    $('.click').click(function () {
      $("#example").dataTable().fnDestroy();
      loadTable('0'); 
    });
  });
  
  $(function () {
    loadTable('1');
  });

  function loadTable(eliminados) {
    $('#areas-table').DataTable({
      processing: true,
      serverSide: true,
      destroy: true,
      type: 'GET',
      ajax: "{{ route('get.states') }}?showDeleted="+eliminados,
      language: {
        "search": "Search",
        "lengthMenu": "Show _MENU_ per page",
        "zeroRecords": "Sorry, we can't find what you're looking for",
        "info": "Showing page _PAGE_ of _PAGES_ of _TOTAL_ records",
        "infoEmpty": "Records not found",
        "infoFiltered": "(Filtered in _MAX_ total records)",
        "paginate": {
          "previous": "Previous",
          "next": "Next",
        },
      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex', title: '#'},
        { data: 'nombre', name: 'nombre', title: 'Name' },
        { data: 'paisDesc', name: 'paisDesc', title: 'Country' },
        {
          data: 'activo', name: 'activo', title: 'Active', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<div class="iradio_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Enabled';
            } else {
              return '<div class="iradio_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Remove';
            }
          }
        },
        { data: 'created_at', name: 'created_at', title: 'Created' },
        { data: 'updated_at', name: 'updated_at', title: 'Updated' },
        { data: 'action', name: 'action', title: 'Actions', orderable: false, searchable: false }
      ]
    });
  }
</script>
</section>
@endsection
