@extends('layouts.app', ['activePage' => 'permisos', 'titlePage' => __('Permisos de Usuario')])

@section('content')
<div class="content">
  <div class="container-fluid">
  <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
              <div class="card-icon">
                <i class="material-icons">groupadd</i>
              </div>
              <p class="card-category">Permisos Totales</p>
              <h3 class="card-title">{{count($dataTotal)}}
              </h3>
            </div>
            <div class="card-footer">
              <div class="stats">
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i class="material-icons">groupadd</i>
              </div>
              <p class="card-category">Permisos Activos</p>
              <h3 class="card-title">{{count($permissionActive)}}</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
              <div class="card-icon">
                <i class="material-icons">groupadd</i>
              </div>
              <p class="card-category">Permisos Eliminados</p>
              <h3 class="card-title">{{count($permissionInactive)}}</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
         
        </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Catálogo de Permisos de Usuario</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            @can('role-create')
                    <span class="float-right">
                        <a class="btn btn-primary" href="{{ route('permissions.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo Permiso de Usuario</a>
                    </span>
            @endcan
            <table id="permisos-table" class="table table-hover table-striped table-bordered dt-responsive"></table>
            
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Delete Model -->
<div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <h4 class="modal-title" id="mySmallModalLabel">Eliminar Permiso de Usuario</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Por favor, confirme que desea eliminar el permiso de usuario: <b><span id="fav-title"></span></b>?
          <hr>
          @can('role-delete')
            {!! Form::open(['method' => 'DELETE','route' => ['permissions.destroy', 0],'style'=>'display:inline']) !!}
            <input name="id_permission" id="id_permiss" type="hidden" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="submit" value="delete" class="btn btn-success">Aceptar</button>
            {!! Form::close() !!}
          @endcan
        </div>
      </div>
    </div>
  </div>
  <!-- /.Delete Model -->

  <!-- Restaurar Model -->
  <div id="restore" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <h4 class="modal-title" id="mySmallModalLabel">Restaurar Permiso de Usuario </h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Por favor, confirme que desea restaurar permiso de Usuario: <b><span id="favs-title"></span></b>?
          <hr>
          {!! Form::open(['method' => 'POST','route' => ['permissions/restore'],'style'=>'display:inline']) !!}
            {{csrf_field()}}
            <input name="id_permission" id="id_permiss" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="submit" value="delete" class="btn btn-success">Aceptar</button>
            {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
  <!-- /.Restaurar Model -->


<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script>
  $( document ).ready(function() {
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      $("#fav-title").html($(e.relatedTarget).data('title'));
      var id_permiss = $(e.relatedTarget).data('id');
      modal.find('.modal-body #id_permiss').val(id_permiss);
    });

    $('#restore').on('show.bs.modal', function (e) {
      var modal = $(this);
      $("#favs-title").html($(e.relatedTarget).data('tit'));
      var id_permiss = $(e.relatedTarget).data('id');      
      modal.find('.modal-body #id_permiss').val(id_permiss);
    });

  });

  $(function () {
    $('#permisos-table').DataTable({
      processing: true,
      serverSide: true,
      type: 'GET',
      ajax: "{{ route('get.permissions') }}",
      language: {
        "search": "Buscar",
        "lengthMenu": "Mostar _MENU_ registros por página",
        "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
        "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
        "infoEmpty": "Registros no encontrados",
        "infoFiltered": "(Filtrado en _MAX_ registros totales)",
        "paginate": {
          "previous": "Anterior",
          "next": "Siguiente",
        },
      },
      columns: [
        { data: 'id', name: 'id', title: '#' },
        { data: 'name', name: 'name', title: 'Nombre' },
        {
          data: 'status', name: 'status', title: 'Estado', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<div class="iradio_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Habilitada';
            } else {
              return '<div class="iradio_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Eliminada';
            }
          }
        },
        { data: 'created_at', name: 'created_at', title: 'Creada' },
        { data: 'updated_at', name: 'updated_at', title: 'Actualizada' },
        { data: 'action', name: 'action', title: 'Acciones', orderable: false, searchable: false }
      ]
    });
  });
</script>
@endsection

