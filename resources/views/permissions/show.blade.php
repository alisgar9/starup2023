@extends('layouts.app', ['activePage' => 'permisos', 'titlePage' => __('Ver Permiso')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">

    

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Ver Permiso de Usuario') }}</h4>
                <p class="card-category">{{ __('*Todos los campos son requeridos.') }}</p>
              </div>
              <div class="card-body ">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Opps!</strong> Algo salió mal, verifique los errores a continuación.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                    <div class="form-group">
                        <strong>*Nombre:</strong>
                        {{ $permission->name }}
                        
                    </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection

