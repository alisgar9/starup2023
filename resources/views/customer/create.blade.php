@extends('adminlte::layouts.app')
<link href="{{ URL::asset('/css/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Add Customer<small>Access</small></h1>
     <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="{{ url('customer') }}">Access to Customer Portal</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> ¡Alerta!</h5>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
    </div>
    @endif
    <div class="alert alert-danger" role="alert" style="display:none;" id="error">
        <ul id="error"></ul>
    </div>
    <div class="alert alert-success" role="alert" style="display:none;" id="exito">
        
    </div>


      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Customer Access</h3>
              <p>The fields marked with <b>*</b> are required</p>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="create" method="POST" action= '{{ action("CustomerController@add") }}'>
 <div class="box-body">
 {{csrf_field()}}
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('email', 'Email * ') }}
            <div class="col-md-12 field_wrapper">
                <div class="col-md-10">
                    <input type="text" name="email[]" class="form-control"  requerid value=""/>
                </div>
                <div class="col-md-2">
                    <a href="javascript:void(0);" class="add_button btn btn-primary" title="Add field"><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('customer', 'Customer') }}
            <input id="consignee_desc" name="name" value="{{old('name')}}" requerid type="text" class="form-control" placeholder="Search Customer" />
                        <input type="hidden" id="custom" name="custom" requerid >
            
        </div>
    </div>
    
    

    


    
    
      </div>

 <div class="box-footer">
    {{ Form::button('<i class="fa fa-save" aria-hidden="true"></i> Guardar', ['class' => 'btn btn-primary', 'type' => 'submit']) }}

    </div>

    </form>

            
          </div>
          <!-- /.box -->

        

        


        </div>
        <!--/.col (left) -->
       
      </div>
      <!-- /.row -->
</section>
<!-- /.content -->


@endsection
@yield('content')
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){

        var maxField = 10; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var fieldHTML = '<div class="col-md-12" style="display: flex;"><input type="text" name="email[]" class="form-control"  requerid="requerid" value=""/ style="width: 82%;"> <a href="javascript:void(0);" class="remove_button btn btn-primary" title="Remove field" style="margin-left: 32px;"><i class="fa fa-times" aria-hidden="true"></i></a></div>'; //New input field html 
        var x = 1; //Initial field counter is 1
        $(addButton).click(function(){ //Once add button is clicked
            if(x < maxField){ //Check maximum number of input fields
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); // Add field html
            }
        });
        $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });

        
            function entidadSearch(tipo,etiqueta){
            $("#"+etiqueta).easyAutocomplete({
                url: function(search) {
                    return "{{route('autocomplete.entidad')}}?tipo=consignee&search=" + search;
                },
            
                getValue: "nombre",
                list: {
                        onSelectItemEvent: function() {
                            var dataID = $("#"+etiqueta).getSelectedItemData().id;
                            $("#"+tipo).val(dataID);
                        },
                match: {
                        enabled: true
                        }
                }
            });
        }

        $('#consignee_desc').ready(function(){
            entidadSearch("custom", "consignee_desc");
        });
    });
</script> 

   
@stop
@yield('scripts')
