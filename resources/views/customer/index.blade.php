@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Access <small>to Customer Portal</small></h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Access to Customer Portal</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

  @if(session()->has('flash_message'))
  <div class="alert alert-success alert-dismissible fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> success!</h4>
    {{ session()->get('flash_message') }}
  </div>
  @endif

  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Customer Totales</span>
          <span class="info-box-number">
            <h3>{{ $users['TotalUser'] }}</h3>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-user-plus"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Customer Enable</span>
          <span class="info-box-number">
            <h3>{{ $users['UserEnable'] }}</h3>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    
  </div>
  <!-- /.row -->



  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Customer List</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="alert alert-danger" role="alert" style="display:none;" id="error">
              <ul id="error"></ul>
          </div>
          <div class="alert alert-success" role="alert" style="display:none;" id="exito">
              
          </div>
          <table id="usuarios-table" class="table table-striped table-bordered dt-responsive nowrap"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <a href="{{ route('customer.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Add Customer Access</a>
    </div>
    <!-- /.col -->
  </div>

  <!-- Delete Model -->
  <div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Customer Disable</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Please confirm that you want to delete the user: <b><span id="fav-title"></span></b>?
          <hr>
          <form action='{{ action("CustomerController@delete") }}' id="delForm" method="post">
          {{csrf_field()}}
            <input name="id_user" id="id_user" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" value="delete" class="btn btn-success">Accept</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Delete Model -->
</section>

<script type="text/javascript">
  $(document).ready(function () {
    // se activa cuando el modal está a punto de ser mostrado.
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      // obtener el atributo idEmpresa del elemento pulsado.
      $("#fav-title").html($(e.relatedTarget).data('title'));
      var idEmpresa = $(e.relatedTarget).data('idemp');
      console.log(idEmpresa);
      modal.find('.modal-body #id_user').val(idEmpresa);
    });

  

  });

  
  $(function () {
    $('#usuarios-table').DataTable({
      processing: true,
      serverSide: true,
      type: 'GET',
      ajax: "{{ route('get.customer') }}",
      language: {
        "search": "Buscar",
        "lengthMenu": "Mostar _MENU_ registros por página",
        "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
        "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
        "infoEmpty": "Registros no encontrados",
        "infoFiltered": "(Filtrado en _MAX_ registros totales)",
        "paginate": {
          "previous": "Anterior",
          "next": "Siguiente",
        },
      },
      columns: [
        { data: 'id', name: 'id', title: '#' },
        { data: 'name', name: 'name', title: 'Name' },
        { data: 'email', name: 'email', title: 'Email' },
        { data: 'consignee', name: 'consignee', title: 'Consignee' },
        { data: 'created_at', name: 'created_at', title: 'Created' },
        {
          data: 'status', name: 'status', title: 'Status', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<div class="iradio_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Enabled';
            }else {
              return '<div class="iradio_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Deleted';
            }
          }
        },
        { data: 'action', name: 'action', title: 'Acciones', orderable: false, searchable: false }
      ]
    });
  });
</script>

@endsection