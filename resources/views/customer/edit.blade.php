@extends('adminlte::layouts.app')
<link href="{{ URL::asset('/css/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Editar Entidad<small>Control de Entidades</small></h1>
     <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="{{ url('entidades') }}">Entidades</a></li>
        <li class="active">Agregar</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> ¡Alerta!</h5>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
    </div>
    @endif


      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Editar Entidad</h3>
              <p>Los campos marcados con <b>*</b> son requeridos</p>
            <!-- </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <!-- {{ Form::model($entidades, array('route' => array('entidades.update', $entidades->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with permission data --}}-->
            <form action='{{ action("EntidadesController@update") }}' method="post">
            {{csrf_field()}}
            <div class="box-body">
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('nombre', 'Nombre(Alias) *') }}
            {!! Form::text('nombre', null, [
                    'class'                         => 'form-control',
                    'required'                      => 'required',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('razonsocial', 'Razón Social *') }}
            {!! Form::text('razonsocial', null, [
                    'class'                         => 'form-control',
                    'required'                      => 'required',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('nombrecontacto', 'Nombre de Contacto') }}
            {!! Form::text('nombrecontacto', null, [
                    'class'                         => 'form-control',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('email', 'Correo Electrónico') }}
            {{ Form::email('email', null, array('class' => 'form-control')) }}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('email', 'Correos Electrónicos para Con Copia') }}
            {!! Form::text('email_cc',json_decode($entidades->email_cc), [
                                'data-role' => 'tagsinput',
                                'class' => 'form-control'
                                ]) !!}
            
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('direccion', 'Dirección') }}
            {!! Form::text('direccion', null, [
                    'class'                         => 'form-control',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('telefono', 'Teléfono') }}
            {!! Form::text('telefono', null, [
                    'class'                         => 'form-control',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('taxid', 'Tax ID ') }}
            {!! Form::text('taxid', null, [
                    'class'                         => 'form-control',
                    ]) !!}
        </div>
    </div>

    <div class="col-md-6">
                    <div class="form-group">

                        {{ Form::label('id_clasificacion', 'Seleccione tipos de Clasificación *') }}<br>
                        {{ Form::select('id_clasificacion[]',$clasificaciones,$clasificacion,[
                                'multiple' =>'multiple','requerid' => 'requerid', 'id' =>'demo'],array('name'=>'id_clasificacion[]'))}}

                        
                    </div>        
    </div> 
    

    
    
      </div>

 <div class="box-footer">
   <input type="hidden" name="idEntidad"  value="{{$id}}">
    {{ Form::button('<i class="fa fa-save" aria-hidden="true"></i> Guardar', ['class' => 'btn btn-primary', 'type' => 'submit']) }}

    </div>
    </form>
   <!-- {{ Form::close() }}-->

            
          </div>
          <!-- /.box -->

        

        


        </div>
        <!--/.col (left) -->
       
      </div>
      <!-- /.row -->


      <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
      <div class="col-md-6">
       <div class="box-header with-border">
          <h3 class="box-title">Lista Detalles Bancarios</h3>
        </div></div>
      <div class="col-md-6 text-right">
      <br/>
        <button type='submit' class='btn btn-sm btn-success' data-toggle='modal'  data-title='Agregar Detalle Bancario' data-target='#addDetailsBank'><i class="fa fa-plus"></i> Agregar Detalle Bancario</button>
      </div>
        
        <!-- /.box-header -->
        <div class="box-body">
          <table id="usuarios-table" class="table table-striped table-bordered dt-responsive nowrap"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      
    </div>
    <!-- /.col -->
  </div>



      <!-- Delete Model -->
  <div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Eliminar Entidad</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Por favor, confirme que desea eliminar el Detalle Bancario: <b><span id="fav-title"></span></b>?
          <hr>
          <form action="{{ route('entidadbanco.destroy','0') }}" id="delForm" method="post">
            {{method_field('delete')}} {{csrf_field()}}
            <input name="id_entidad_banco" id="id_emp" type="hidden" />
            <input name="id_entidad" id="id_entidad" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="submit" value="delete" class="btn btn-success">Aceptar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Delete Model -->



    <!-- Add Model -->
    <div id="addDetailsBank" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Agregar Detalle Bancario</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
            

            {{ Form::open(array('url' => 'entidadbanco')) }}
            {{csrf_field()}}
            <div class="box-body">
                <div class="col-md-12">
                        <h3 class="box-title">Detalle Bancario </h3>
                        <hr class="hr-primary" />
                        <p>Los campos marcados con <b>*</b> son requeridos</p>
                </div> 
                <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('cuentabancaria', 'Cuenta Bancaria *') }}
                                    {!! Form::text('cuentabancaria', null, [
                                    'class' => 'form-control', 'requerid' => 'requerid']) !!}
                                </div>
                </div>
                <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('cinterbancaria', 'Clabe Interbancaria *') }}
                                    {!! Form::text('cinterbancaria', null, [
                                    'class' => 'form-control', 'requerid' => 'requerid', 'size' => '18', 'maxlength' => '18']) !!}
                                </div>
                </div> 
                <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('id_banco', 'Banco *') }}
                                    {{ Form::select('id_banco',$banco,null,[
                                    'class' => 'form-control', 'requerid' => 'requerid','placeholder' => 'seleccione uno...'],array('name'=>'banco[]'))}}
                                </div>
                </div> 
                <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('id_banco_inter', 'Banco intermediario') }}
                                    {{ Form::select('id_banco_inter',$banco,null,[
                                    'class' => 'form-control','placeholder' => 'seleccione uno...'],array('name'=>'banco[]'))}}
                                </div>
                </div>
                <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('swift', 'Swift') }}
                                    {!! Form::text('swift', null, [
                                    'class' => 'form-control', 'size' => '11', 'maxlength' => '11']) !!}
                                </div>

                </div>

                
                
                </div>

            <div class="box-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                {{ Form::hidden('id_entidad', encrypt($entidades->id), array('id' => $entidades->id_entidad)) }}
                {{ Form::button('<i class="fa fa-save" aria-hidden="true"></i> Guardar', ['class' => 'btn btn-primary', 'type' => 'submit']) }}

                </div>

                {{ Form::close() }}
            
        </div>
      </div>
    </div>
  </div>
  <!-- /.Add Model -->


</section>
<!-- /.content -->



@endsection
@yield('content')
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-angular.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">
  $(document).ready(function () {

    $('#demo').multiselect();
    // se activa cuando el modal está a punto de ser mostrado.
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      // obtener el atributo idEmpresa del elemento pulsado.
      $("#fav-title").html($(e.relatedTarget).data('title'));
      var idEmpresa = $(e.relatedTarget).data('idemp');
      var idEnt = $(e.relatedTarget).data('ident');
      modal.find('.modal-body #id_emp').val(idEmpresa);
      modal.find('.modal-body #id_entidad').val(idEnt);
    });
  });
  $(function () {
    $('#usuarios-table').DataTable({
      processing: true,
      serverSide: true,
      type: 'GET',
      ajax: "{{ route('get.entidades.banco') }}?showDeleted="+{{$entidades->id}},
      language: {
        "search": "Buscar",
        "lengthMenu": "Mostar _MENU_ registros por página",
        "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
        "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
        "infoEmpty": "Registros no encontrados",
        "infoFiltered": "(Filtrado en _MAX_ registros totales)",
        "paginate": {
          "previous": "Anterior",
          "next": "Siguiente",
        },
      },
      columns: [
        { data: 'id', name: 'id', title: '#' },
        { data: 'cuentabancaria', name: 'cuentabancaria', title: 'Cuenta Bancaria' },
        { data: 'cinterbancaria', name: 'cinterbancaria', title: 'Clabe Interbancaria' },
        { data: 'banco', name: 'banco', title: 'Banco' },
        { data: 'bancointer', name: 'bancointer', title: 'Banco intermediario' },
        { data: 'swift', name: 'swift', title: 'Swift' },
        { 
          data: 'activo', name: 'status', title: 'Estado', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<small class="label pull-center bg-green">Activo</small>';
            } else {
              return '<small class="label pull-center bg-red">Inactivo</small>';
            }
          }
        },
        { data: 'action', name: 'action', title: 'Acciones', orderable: false, searchable: false }
      ]
    });
  });
</script>


   
@stop
@yield('scripts')