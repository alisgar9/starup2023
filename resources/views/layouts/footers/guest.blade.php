<footer class="footer">
    <div class="container">
        <div class="copyright float-right">
        &copy;
        <script>
            document.write(new Date().getFullYear())
        </script>, Starup Logistics México.
        </div>
    </div>
</footer>