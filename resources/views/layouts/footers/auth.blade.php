<footer class="footer">
  <div class="container-fluid">
    
    <div class="copyright float-right">
      &copy;
      <script>
        document.write(new Date().getFullYear())
      </script><a href="https://www.starup.com.mx" target="_blank"> Starup México</a> 
    </div>
  </div>
</footer>