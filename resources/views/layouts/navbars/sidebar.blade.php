<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="https://creative-tim.com/" class="simple-text logo-normal">
      {{ __('Creative Tim') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'user-management') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#laravelExample" aria-expanded="true">
          <i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i>
          <p>{{ __('Laravel Examples') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse show" id="laravelExample">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('profile.edit') }}">
                <span class="sidebar-mini"> UP </span>
                <span class="sidebar-normal">{{ __('User profile') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'user-management' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('user.index') }}">
                <span class="sidebar-mini"> UM </span>
                <span class="sidebar-normal"> {{ __('User Management') }} </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      @hasrole('Admin')
      <li class="nav-item{{ $activePage == 'users' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('users.index') }}">
          <i class="material-icons">peopleoutline</i>
            <p>{{ __('Usuarios') }}</p>
        </a>
      </li>

      <li class="nav-item{{ $activePage == 'roles' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('roles.index') }}">
          <i class="material-icons">badge</i>
            <p>{{ __('Roles') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'permisos' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('permissions.index') }}">
          <i class="material-icons">groupadd</i>
            <p>{{ __('Permisos') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'user-management') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#reports" aria-expanded="true">
        <i class="fa fa-line-chart" aria-hidden="true"></i>
          <p>{{ __('Reports') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse" id="reports">
          <ul class="nav">
          <li class="nav-item{{ $activePage == 'report-general' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('get.general') }}">
                <i class="fa fa-bar-chart" aria-hidden="true"></i>
                  <p>{{ __('General Report') }}</p>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'report-conteiners' ? ' active' : '' }}">
              <a class="nav-link" href="{{ url('report/containers') }}">
                <i class="fa fa-bar-chart" aria-hidden="true"></i>
                  <p>{{ __('Conteiners Report') }}</p>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item{{ $activePage == 'airports' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('airports.index') }}">
        <i class="fa-solid fa-plane-up"></i>
            <p>{{ __('Aiports') }}</p>
        </a>
      </li>

      @endhasrole
      @hasrole('Ventas')
      

      <li class="nav-item{{ $activePage == 'report-general' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('get.general') }}">
          <i class="material-icons">content_paste</i>
            <p>{{ __('Reporte General') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'report-conteiners' ? ' active' : '' }}">
              <a class="nav-link" href="{{ url('report/containers') }}">
                <i class="fa fa-bar-chart" aria-hidden="true"></i>
                  <p>{{ __('Conteiners Report') }}</p>
              </a>
      </li>
      @endhasrole
      
    </ul>
  </div>
</div>
