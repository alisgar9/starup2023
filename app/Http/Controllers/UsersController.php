<?php

namespace App\Http\Controllers;

use DB;
use App\Models\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;
use App\Models\LogActivity;
use Illuminate\Support\Str;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Illuminate\Support\Arr;

class UsersController extends Controller
{
    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct()
    {
         $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index','store']]);
         $this->middleware('permission:user-create', ['only' => ['create','store']]);
         $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    } 
    
    /**
     * Display a listing of the users
     *
     * @param  \App\Models\User  $model
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->paginate(5);
        $dataTotal = User::orderBy('id','DESC')->get();
        $dataActive = User::where('approved', '=','1')->get();
        $dataInactive = User::where('approved', '=', '0')->get();

        // Registro en log.
        \Log::info('|::|user.index Usuarios lista.', ['Usuario' => Auth::user()]);
        return view('users.index', compact('data', 'dataTotal', 'dataActive', 'dataInactive'));
    }

    public function create()
    {
        // Registro de log.
        \Log::info('|::| Muestra el formulario users.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        //Get all roles and pass it to the view
        $roles = Role::pluck('name','id')->all();
        return view('users.create', compact('roles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'name' => 'required|max:120',
            'username' => 'sometimes|required|max:255|unique:users',
            'email' => 'required|email|unique:users',
            'roles' => 'required',
            'roles.*' => 'numeric',
            'password' => 'required|min:6|confirmed',
        ], [
            'name.required' => 'El campo Nombre es requerido!',
            'username.required' => 'El campo Nombre de usuario es requerido!',
            'email.required' => 'El campo Correo es requerido!',
            'email.email' => 'Incluye un signo "@" en la dirección de correo!',
            'roles.required' => 'El campo Rol es requerido!',
            'password.required' => 'El campo Contraseña es requerido!',
            'password.min' => 'La contraseña debe tener al menos 6 caracteres!',
            'password.confirmed' => 'La confirmación de contraseña no coincide!',
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
    
        $user = User::create($input);
        $user->assignRole($request->input('roles'));

        // Registro en log.
        \Log::info('|::| Recurso User.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Municipio' => $user]);
         // Registro de Actividades
         $activity = activity()->log('Recurso recién creado: '. $user->email);
         $activity->causer_type = 'UsersController';
         $activity->save();
        

        //Redirect to the users.index view and display message
        return redirect()->route('users.index')
            ->with('success',
                'Usuario agregado con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find(decrypt($id));
        $roles = Role::pluck('name', 'id')->all();
        $userRole = $user->roles->pluck('id')->all();
        // Registro en log.
        \Log::info('|::| user.view Vista del recurso especificado.', ['Usuario' => Auth::user(), 'Usuario' => $user]);
        // Registro de Actividades
        $activity = activity()->log('Vista del recurso especificado '. $user->name);
        $activity->causer_type = 'UsersController';
        $activity->save();

        return view('users.show', compact('user', 'roles', 'userRole'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find(decrypt($id));
        $roles = Role::pluck('name', 'id')->all();
        $userRole = $user->roles->pluck('id')->all();

        // Registro en log.
         \Log::info('|::| writeupdate Formulario de Actualización  el recurso especificado.', ['Usuario' => Auth::user(), 'Usuario' => $user]);
    
        return view('users.edit', compact('user', 'roles', 'userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'confirmed',
            'roles' => 'required'
        ]);
    
        $input = $request->all();
        
        if(!empty($input['password'])) { 
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));    
        }
    
        $user = User::find(decrypt($id));
        $user->update($input);

        DB::table('model_has_roles')
            ->where('model_id', decrypt($id))
            ->delete();
    
        $user->assignRole($request->input('roles'));

        // Registro en log.
        \Log::info('|::| users.update Actualizar el recurso especificado.', ['Usuario' => Auth::user(), 'Usuario' => $user]);
        // Registro de Actividades
        $activity = activity()->log('Actualizar el recurso especificado '. $user->name);
        $activity->causer_type = 'UsersController';
        $activity->save();
    
        return redirect()->route('users.index')
            ->with('success', 'User updated successfully.');
    }



    public function destroy($id, Request $request)
    {
        $user = User::find(decrypt($request->id_user));
        $user->approved = 0;
        $user->save();

        // Registro en log.
        \Log::info('|::| Users.delete Eliminar el recurso especificado.', ['Usuario' => Auth::user(), 'Usuario' => $user]);
        // Registro de Actividades
        $activity = activity()->log('Recurso eliminado '. $user->name);
        $activity->causer_type = 'UsersController';
        $activity->save();

        
        return redirect()->route('users.index')
            ->with('success', 'Se inhabilito el usuario');
    }

    public function getData()
    {
        $user = User::all(); 
        
        return Datatables::of($user)
            ->addColumn('action', function ($user) {

                $result = '';

                if ($user->approved == 1){

                    if (Auth::user()->can('user-view'))
                        $result .= " <a href='".route('users.show', encrypt($user->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                    if (Auth::user()->can('user-edit'))
                        $result .= " <a href='".route('users.edit', encrypt($user->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";
    
                    if (Auth::user()->can('user-delete') && $user->admin != 1 )
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-id='".encrypt($user->id)."' data-title='".$user->name."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }
                else
                {
                        if (Auth::user()->admin == 1)
                        $result .= " <button type='submit' class='btn btn-sm btn-primary' data-toggle='modal' alt='Restaurar' data-id='".encrypt($user->id)."' data-tit='".$user->name."' data-target='#restore'><i class='fa fa-refresh' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->editColumn('id', '{{$id}}')
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->make(true);
    }

    public function restore(Request $request)
    {
        
        $user = User::find(decrypt($request->id_user));
        $user->approved = 1;
        $user->save();

        // Registro en log.
        \Log::info('|::| User.restore Restaurar el recurso especificado.', ['Usuario' => Auth::user(), 'User' => $user]);
        // Registro de Actividades
        $activity = activity()->log('Recurso restaurado '. $user->name);
        $activity->causer_type = 'UsersController';
        $activity->save();



        return redirect()->route('users.index')
            ->with('success', 'Se habilito el usuario.');
    }


}
