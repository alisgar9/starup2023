<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;
use App\Models\Aeropuerto;
use App\Models\Pais;
use App\Models\LogActivity;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\LogActivityController;

class AeropuertoController extends Controller
{
    private $controller = 'AeropuertoController';
    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct()
    {
         $this->middleware('permission:airport-list|airport-create|airport-edit|airport-delete|airport-view', ['only' => ['index','store']]);
         $this->middleware('permission:airport-create', ['only' => ['create','store']]);
         $this->middleware('permission:airport-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:airport-delete', ['only' => ['destroy']]);
    } 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Registro de log.
        \Log::info('|::| Aeropuerto Index.', ['Usuario' => Auth::user()]);

         //Get all puertos and pass it to the view
         $aeropuertos = Aeropuerto::count();
         $aeropuertosEnabled = Aeropuerto::where('activo','=',1)->count();
         $aeropuertosDisabled = Aeropuerto::where('activo','=',0)->count();
         return view('aeropuerto.index')->with('aeropuertos', $aeropuertos)->with('aeropuertosEnabled', $aeropuertosEnabled)->with('aeropuertosDisabled', $aeropuertosDisabled);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Registro de log.
        \Log::info('|::| Muestra el formulario Airport.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        
        $pais = Pais::orderBy('nombre', 'ASC')->get()->pluck('nombre', 'id')->toArray();

        //Get all municipios and show view
        return view('aeropuerto.create', ['pais' => $pais]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate fields
        $this->validate($request, [
            'nombre' => 'required|max:50|unique:adm_estados',
            'pais' => 'required|max:3',
        ],[
            'nombre.required' => 'The Name field is required!',
            'pais.required' => 'The Country field is required!',
        ]);

        $aeropuerto = new Aeropuerto();
        $aeropuerto->nombre = Str::upper($request['nombre']);
        $aeropuerto->pais = $request['pais'];
        $aeropuerto->clave = Str::upper($request['clave']);
        $aeropuerto->save();

        // Registro en log.
        \Log::info('|::| Recurso Airport.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'State' => $aeropuerto]);

        // Registro en Bitácora.
        $LogActivityController = new LogActivityController();
        $LogActivityController->addToLog('Recurso recién creado: '.$aeropuerto->nombre, $this->controller);

        return redirect()->route('airports.index')
            ->with('success',
             'Airport: '. $aeropuerto->nombre.' added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!isset($_SERVER['HTTP_REFERER'])){
            Auth::logout();
            return view('errors.forbidden', ['message' => 'You cannot access directly.']);
        }

        // Registro en log.
        \Log::info('|::| Airport.show - Mostrar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);
        
        $aeropuerto = Aeropuerto::findOrFail(decrypt($id));
        return view('aeropuerto.show', compact('aeropuerto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       // Registro en log.
       \Log::info('|::| Airport.edit - Show the form to edit the specified resource.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

       $aeropuerto = Aeropuerto::findOrFail(decrypt($id));
       $pais = Pais::orderBy('nombre', 'ASC')->get()->pluck('nombre', 'id')->toArray();
       return view('aeropuerto.edit', compact('aeropuerto','pais'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //Validate name, email and password fields
         $this->validate($request, [
            'nombre' => 'required|max:50|unique:adm_estados,nombre,'.$id,
            'pais' => 'required|max:3',
        ],[
            'nombre.required' => 'The Name field is required!',
            'pais.required' => 'The Country field is required!',
        ]);


        $aeropuerto = Aeropuerto::findOrFail($id);
        $input = array(
            'nombre' => Str::upper($request['nombre']),
            'pais' => $request['pais'],
            'clave' => Str::upper($request['clave'])
        );
        
        $request->all();
        $aeropuerto->fill($input)->save();

        // Registro en Bitácora.
        $LogActivityController = new LogActivityController();
        $LogActivityController->addToLog('Recurso actualizado: '.$aeropuerto->nombre, $this->controller);
        
        return redirect()->route('airports.index')
            ->with('success',
             'Updated '. $aeropuerto->nombre.' Airport!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aeropuerto = Aeropuerto::findOrFail(decrypt($request->idstate));
        $aeropuerto->activo = 0;
        $aeropuerto->save();

        // Registro en log.
        \Log::info('|::| State.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'Contenedor' => $aeropuerto]);

        // Registro en Bitácora.
        $LogActivityController = new LogActivityController();
        $LogActivityController->addToLog('Recurso recién eliminado: '.$aeropuerto->nombre, $this->controller);

        return redirect()->route('airports.index')
            ->with('success',
             'State successfully removed!');
    }

    public function getData(Request $request)
    {
        $showDeleted  = $request->get('showDeleted');

        $aeropuerto = Aeropuerto::where('activo','=',$showDeleted )->orderby('id','desc')->get();
        return Datatables::of($aeropuerto)
            ->addColumn('action', function ($aeropuerto) {

                $result = '';

                if ($aeropuerto->activo == 1){

                    if (Auth::user()->hasPermissionTo('airport-view'))
                        $result .= " <a href='".route('airports.show', encrypt($aeropuerto->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                    if (Auth::user()->hasPermissionTo('airport-edit'))
                        $result .= " <a href='".route('airports.edit', encrypt($aeropuerto->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";
    
                    if (Auth::user()->hasPermissionTo('airport-delete'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-puerto='".encrypt($aeropuerto->id)."' data-title='".$aeropuerto->nombre."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->addIndexColumn()
            ->addColumn('paisDesc', function ($query) {
                return $query->Pais['nombre'];
            })
            ->editColumn('id', '{{$id}}')
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->make(true);
    }
}
