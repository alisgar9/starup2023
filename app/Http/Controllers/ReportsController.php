<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;
use App\Models\LogActivity;
use Illuminate\Support\Str;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use DB;
use App\Exports\UsersExport;
use App\Exports\ReportGeneralExport;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller
{
    
    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct()
    {
         $this->middleware('permission:reportgnral-list|reportgnral-create|reportgnral-edit|reportgnral-delete', ['only' => ['index','store']]);
         $this->middleware('permission:reportgnral-create', ['only' => ['create','store']]);
         $this->middleware('permission:reportgnral-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:reportgnral-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        //$consignee = app('App\Http\Controllers\APIController')->RequestAPI('GET', 'api/consignee');

        //$user = Auth::user()->email;

        //$UserLog = DB::connection('mysql2')->table('users')->where('email', '=', 'oscar@starup.com.mx')->first();
        //dd($UserLog);
        $consignee = DB::connection('mysql2')->table('adm_entidad')->where([['status', 1]])->orderBy('nombre')->pluck('nombre', 'id');
        return view('reportes.reportGeneral', compact('consignee'));
    }

    public function download(Request $request){
        $custom= $request->custom;
        $dateStart=$request->etas;
        $dateFinish=$request->etast;

        $data =  DB::connection('mysql2')->table('adm_operaciones_contenedor as A')
            ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.id AS SUMEX','P.hbl','P.mbl', 'P.ETD', 'P.ETA', 'P.ATD', 'P.ATA', 'P.vessel','P.free_demurrages_days_cust', 'P.POL','P.POD', 'P.tipo_operacion', 'P.operacion', 'P.PI', 'P.PO', 'P.final_dest','P.shipper', 'P.factura','P.comments', 'P.telex', 'P.terminal', 'P.shipping_line', 'P.booking','P.origin_contry','P.destination_contry','P.booking_status','P.expense_bl','P.expense_contr','P.tarifa', 'C.descripcion AS tipo_operacion_desc')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where([['U.activo','=',1],['P.consignee','=',$custom],[
                function($query){
                    $query->where('A.activo', '=', 1)
                      ->orWhere('A.activo', '=', 2)
                      ->orWhere('A.activo', '=', 3);
                }
               ]])
            ->whereBetween('P.ETA', array($dateStart, $dateFinish))
            ->orderby('P.id', 'asc')->get()->toArray();
                
            //$customer = Entidades::where("id","=",$custom)->first();
            $datas = array();
                    
            foreach ($data as $key => $value) {
                $i= $key+2;
                $folio = 'SUMEX0'.$value->SUMEX;

                if($value->telex) {                    
                    $telex = $value->telex;
                }else{
                    $telex = 'Not';
                }

                $datas[] = array(
                'folio' => $folio,
                'shipper' => isset($value->shipper) ? $this->Descentidad($value->shipper)->nombre : NULL,
                'PO' => isset($value->PO) ? json_decode($value->PO) : NULL,
                'PI' => isset($value->PI) ? json_decode($value->PI) : NULL,
                'booking' => isset($value->booking) ? json_decode($value->booking) : NULL,
                'POL' => isset($value->POL) ? $this->DescPuerto($value->POL, $value->tipo_operacion) : NULL,
                'origin_contry' => isset($value->origin_contry) ? $this->searchCountry($value->origin_contry) : NULL,
                'POD' => isset($value->POD) ? $this->DescPuerto($value->POD, $value->tipo_operacion) : NULL,
                'destination_contry' => isset($value->destination_contry) ? $this->searchCountry($value->destination_contry) : NULL,
                'final_dest' => isset($value->final_dest) ? $value->final_dest : NULL,
                'vessel' => isset($value->vessel) ? $value->vessel : NULL,
                'ETD' => isset($value->ETD) ? Carbon::parse($value->ETD)->format("d/m/Y") : NULL,
                'ATD' => isset($value->ATD) ? Carbon::parse($value->ATD)->format("d/m/Y") : NULL,
                'ETA' => isset($value->ETA) ? Carbon::parse($value->ETA)->format("d/m/Y") : NULL,
                'ATA' => isset($value->ATA) ? Carbon::parse($value->ATA)->format("d/m/Y") : NULL,
                'hbl' => isset($value->hbl) ? $value->hbl : NULL, 
                'mbl' => isset($value->mbl) ? $value->mbl : NULL, 
                'container_number' => isset($value->container_number) ? $value->container_number : NULL,
                'SUMEX' => isset($value->SUMEX) ? $this->totalContainers($value->SUMEX) : NULL,
                'booking_status' => isset($value->booking_status) ? $value->booking_status : NULL,
                'free_demurrages_days_cust' => isset($value->free_demurrages_days_cust) ? $value->free_demurrages_days_cust : NULL,
                'comments' => isset($value->comments) ? $value->comments : NULL,
                'tarifa' => isset($value->tarifa) ? $value->tarifa : NULL,
                'expense_bl' => isset($value->expense_bl) ? $value->expense_bl : NULL,
                'expense_contr' => isset($value->expense_contr) ? $value->expense_contr : NULL,
                'factura' => isset($value->factura) ? $value->factura : NULL,
                'telex' => isset($value->telex) ? $value->telex : NULL,
                'terminal' => isset($value->terminal) ? $value->terminal : NULL,
                'shipping_line' => isset($value->shipping_line) ? $this->Descentidad($value->shipping_line)->nombre : NULL,
                'diastranscurridos' => $this->DiasTranscurridos($value->ETA,$value->ETD)
                );
                        
            }

            $export = new ReportGeneralExport($datas);
            return Excel::download($export, 'ReporGeneral.xlsx');
            //return Excel::download($datas, 'invoices.xlsx');
            //return (new ReportGeneralExport($datas))->download('invoices.xlsx');

    }

    public function Descentidad($id){
        
        $entidad = DB::connection('mysql2')->table('adm_entidad')->where("id","=",$id)->first();

        return $entidad;
    }

    public function DescPuerto($id, $type){

        if($type == 1){
            $result = DB::connection('mysql2')->table('adm_puertos')->where('id', '=', $id)->first();
            $name = $result->puerto;
            
        }
        elseif($type == 2){
            $result = DB::connection('mysql2')->table('adm_aeropuertos')->where('id', '=', $id)->first();
            $name = $result->nombre;
        }
        elseif($type == 3){
            $result = DB::connection('mysql2')->table('adm_estados')->where('id', '=', $id)->first();
            $name = $result->nombre;
        }else{
            $name = ' ';
        }
        return $name;
    }

    public function searchCountry($id){
        $pais = DB::connection('mysql2')->table('adm_paises')->where("id","=",$id)->first();
        if($pais)
        { $paisSelect = $pais->nombre; }
        else{$paisSelect = 'N/A'; }
        
        return $paisSelect;
    }

    public function totalContainers($operacion_id){
        $containers = DB::connection('mysql2')->table('adm_operaciones_contenedor')->where([["operacion_id","=",$operacion_id],[
                function($query){
                    $query->where('activo', '=', 1)
                      ->orWhere('activo', '=', 2)
                      ->orWhere('activo', '=', 3);
                }
               ]])->get();
        if($containers == NULL){
            $container = 0;
        }else{
            $container = count($containers);
        }
        return $container;
    }

    public function DiasTranscurridos($inicio,$final){

        $inicio = Carbon::parse($inicio);
        $final = Carbon::parse($final);

        return $diasDiferencia = $final->diffInDays($inicio);
    }
    public function download2(Request $request){
        

        

        
        //$data = Entidades::get()->toArray();
        
        return \Excel::store('ReportGeneral', function($excel) use ($data, $customer) {
            $excel->sheet('mySheet', function($sheet) use ($data, $customer)
            {

                $sheet->cell('A1', function($cell) {$cell->setValue('FOLIO');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('Shipper');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('# PO');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('# PI');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('# Booking');   });
                $sheet->cell('F1', function($cell) {$cell->setValue('Origin Port');   });
                $sheet->cell('G1', function($cell) {$cell->setValue('Origin Country');   });
                $sheet->cell('H1', function($cell) {$cell->setValue('Destination Port');   });
                $sheet->cell('I1', function($cell) {$cell->setValue('Destination Country');   });
                $sheet->cell('J1', function($cell) {$cell->setValue('Final Destination');   });
                $sheet->cell('K1', function($cell) {$cell->setValue('Vessel');   });
                $sheet->cell('L1', function($cell) {$cell->setValue('ETD');   });
                $sheet->cell('M1', function($cell) {$cell->setValue('ATD');   });
                $sheet->cell('N1', function($cell) {$cell->setValue('ETA');   });
                $sheet->cell('O1', function($cell) {$cell->setValue('ATA');   });
                $sheet->cell('P1', function($cell) {$cell->setValue('HBL');   });
                $sheet->cell('Q1', function($cell) {$cell->setValue('MBL');   });
                $sheet->cell('R1', function($cell) {$cell->setValue('# Container');   });
                $sheet->cell('S1', function($cell) {$cell->setValue('40H');   });
                $sheet->cell('T1', function($cell) {$cell->setValue('Status Booking');   });
                $sheet->cell('U1', function($cell) {$cell->setValue('Free Days');   });
                $sheet->cell('V1', function($cell) {$cell->setValue('comments');  });
                $sheet->cell('W1', function($cell) {$cell->setValue('Rate');  });
                $sheet->cell('X1', function($cell) {$cell->setValue('Expense BL');  });
                $sheet->cell('Y1', function($cell) {$cell->setValue('Expense Contr');  });
                $sheet->cell('Z1', function($cell) {$cell->setValue('Invoice Number');  });
                $sheet->cell('AA1', function($cell) {$cell->setValue('TELEX');   });
                $sheet->cell('AB1', function($cell) {$cell->setValue('TERMINAL');   });
                $sheet->cell('AC1', function($cell) {$cell->setValue('Shipping Lineeee');   });
                $sheet->cell('AD1', function($cell) {$cell->setValue('Transit Time');   });
               
                $sheet->setColumnFormat(array('A' => '@', 'B' => '@','C' => '@', 'D' => '@','E' => '@','F' => '@', 'G' => '@', 'H' => '@', 'I' => '@','J' => '@', 'K' => '@','L' => NumberFormat::FORMAT_DATE_DDMMYYYY,'M' => NumberFormat::FORMAT_DATE_DDMMYYYY, 'N' => NumberFormat::FORMAT_DATE_DDMMYYYY,'O' => NumberFormat::FORMAT_DATE_DDMMYYYY,'P' => '@','Q' => '@','R' => '@','S' => '@','T' => '@','U' => '@','V' => '@','W' => '@','X' => '@','Y' => '@','Z' => '@'));
                


                if (!empty($data)) {

                    
                    foreach ($data as $key => $value) {
                        $i= $key+2;
                        $folio = 'SUMEX0'.$value->SUMEX;

                        if($value->telex) {                    
                            $telex = $value->telex;
                        }else{
                            $telex = 'Not';
                        }

                        //dd($folio);

                     $sheet->cell('A'.$i, $folio);
                     $sheet->cell('B'.$i, isset($value->shipper) ? $this->Descentidad($value->shipper)->nombre : NULL); 
                     $sheet->cell('C'.$i, isset($value->PO) ? json_decode($value->PO) : NULL);
                     $sheet->cell('D'.$i, isset($value->PI) ? json_decode($value->PI) : NULL);
                     $sheet->cell('E'.$i, isset($value->booking) ? json_decode($value->booking) : NULL);
                     $sheet->cell('F'.$i, isset($value->POL) ? $this->DescPuerto($value->POL, $value->tipo_operacion) : NULL );
                     $sheet->cell('G'.$i, isset($value->origin_contry) ? $this->searchCountry($value->origin_contry) : NULL);
                     $sheet->cell('H'.$i, isset($value->POD) ? $this->DescPuerto($value->POD, $value->tipo_operacion) : NULL );
                     $sheet->cell('I'.$i, isset($value->destination_contry) ? $this->searchCountry($value->destination_contry) : NULL);
                     $sheet->cell('J'.$i, isset($value->final_dest) ? $value->final_dest : NULL);
                     $sheet->cell('K'.$i, isset($value->vessel) ? $value->vessel : NULL);
                     $sheet->cell('L'.$i, isset($value->ETD) ? Date::stringToExcel($value->ETD) : NULL);
                     $sheet->cell('M'.$i, isset($value->ATD) ? Date::stringToExcel($value->ATD) : NULL);
                     $sheet->cell('N'.$i, isset($value->ETA) ? Date::stringToExcel($value->ETA) : NULL);
                     $sheet->cell('O'.$i, isset($value->ETA) ? Date::stringToExcel($value->ATA) : NULL);
                     $sheet->cell('P'.$i, isset($value->hbl) ? $value->hbl : NULL); 
                     $sheet->cell('Q'.$i, isset($value->mbl) ? $value->mbl : NULL); 
                     $sheet->cell('R'.$i, isset($value->container_number) ? $value->container_number : NULL);
                     $sheet->cell('S'.$i,isset($value->SUMEX) ? $this->totalContainers($value->SUMEX) : NULL);
                     $sheet->cell('T'.$i,  isset($value->booking_status) ? $value->booking_status : NULL);
                     $sheet->cell('U'.$i, isset($value->free_demurrages_days_cust) ? $value->free_demurrages_days_cust : NULL);
                     $sheet->cell('V'.$i, isset($value->comments) ? $value->comments : NULL);
                     $sheet->cell('W'.$i, isset($value->tarifa) ? $value->tarifa : NULL);
                     $sheet->cell('X'.$i, isset($value->expense_bl) ? $value->expense_bl : NULL);
                     $sheet->cell('Y'.$i, isset($value->expense_contr) ? $value->expense_contr : NULL);
                     $sheet->cell('Z'.$i, isset($value->factura) ? $value->factura : NULL);
                     $sheet->cell('AA'.$i, isset($value->telex) ? $value->telex : NULL);
                     $sheet->cell('AB'.$i, isset($value->terminal) ? $value->terminal : NULL);
                     $sheet->cell('AC'.$i, isset($value->shipping_line) ? $this->Descentidad($value->shipping_line)->nombre : NULL);
                     $sheet->cell('AD'.$i, $this->DiasTranscurridos($value->ETA,$value->ETD));
                    
                    }
                }
            });
        })->export('xlsx');

    }
    
    
    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData(Request $request)
    {
       $IdCurrentUser = Auth::id();
       
      //$detallesoperacion = OperacionDetails::where([['activo','=',1]] )->get();

       $detallesoperacion = DB::connection('mysql2')->table('adm_operaciones_contenedor as A')
       ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo', 'P.id AS SUMEX','P.hbl','P.mbl', 'P.ETD', 'P.ETA', 'P.ATD', 'P.ATA', 'P.free_demurrages_days_cust', 'P.POL','P.POD','P.operacion', 'P.PI', 'P.PO', 'P.shipper', 'P.factura','P.comments', 'P.telex', 'P.terminal','C.descripcion AS tipo_operacion_desc')
       ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
       ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
       ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
       ->where([['U.activo','=',1],['P.consignee','=',$request->custom],[
        function($query){
            $query->where('A.activo', '=', 1)
              ->orWhere('A.activo', '=', 2);
        }
       ]])
       ->whereBetween('P.ETA', array($request->etas, $request->etast))
       ->orderby('P.id', 'asc')
       ->get();

       return Datatables::of($detallesoperacion)
           
          
            ->addIndexColumn()
            ->editColumn('POL', function ($query) {

                $puerto = DB::connection('mysql2')->table('adm_puertos')->where("id","=",$query->POL)->first();
                
                return $puerto->puerto;
            })
            ->editColumn('POD', function ($query) {

                $puerto = DB::connection('mysql2')->table('adm_puertos')->where("id","=",$query->POD)->first();
                
                return $puerto->puerto;
            })
            ->editColumn('shipper', function ($query) {
                if($query->shipper) {                    
                    $shipper = DB::connection('mysql2')->table('adm_entidad')->where("id","=",$query->shipper)->first();
                    return $shipper->nombre;
                }else{
                    return 'No Apply';
                }
            })
            ->editColumn('telex', function ($query) {
                if($query->telex) {                    
                    return $query->telex;
                }else{
                    return 'Not';
                }
            })
           ->editColumn('SUMEX', '{{ "SUMEX0".$SUMEX }}')
           ->editColumn('ETA', function ($query) {
                if($query->ETA) {                    
                    return Carbon::parse($query->ETA)->format("Y/m/d");
                }else{
                    return 'Not';
                }
            })
            ->editColumn('ETD', function ($query) {
                if($query->ETD) {                    
                    return Carbon::parse($query->ETD)->format("Y/m/d");
                }else{
                    return 'Not';
                }
            })
            ->editColumn('ATD', function ($query) {
                if($query->ATD) {                    
                    return Carbon::parse($query->ATD)->format("Y/m/d");
                }else{
                    return 'Not';
                }
            })
            ->editColumn('ATA', function ($query) {
                if($query->ATA) {                    
                    return Carbon::parse($query->ATA)->format("Y/m/d");
                }else{
                    return 'Not';
                }
            })
            ->editColumn('bbb', function ($query) {
                if($query->ATA) {                    
                    return Carbon::parse($query->ATA)->format("Y/m/d");
                }else{
                    return 'Not';
                }
            })

           ->make(true);
    }



}
