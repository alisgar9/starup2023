<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;
use App\Models\LogActivity;
use Illuminate\Support\Str;

class PermissionController extends Controller
{
    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct()
    {
         $this->middleware('permission:permission-list|permission-create|permission-edit|permission-delete', ['only' => ['index','store']]);
         $this->middleware('permission:permission-create', ['only' => ['create','store']]);
         $this->middleware('permission:permission-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:permission-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Permission::orderBy('id','DESC')->paginate(5);
        $dataTotal = Permission::orderBy('id','DESC')->get();
        $permissionActive = Permission::where('status', '=','1')->get();
        $permissionInactive = Permission::where('status', '=', '0')->get();

        // Registro en log.
        \Log::info('|::|permissions.index Permisos de Usuario lista.', ['Usuario' => Auth::user()]);
        return view('permissions.index', compact('data', 'dataTotal', 'permissionActive', 'permissionInactive'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Registro en log.
        \Log::info('|::|permissions.createview Formulario para crear nuevo recurso.', ['Usuario' => Auth::user()]);
        return view('permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:permissions,name',
        ]);
    
        $Permission = Permission::create(['name' => $request->input('name')]);

         // Registro en log.
         \Log::info('|::| permissions.create crear nuevo recurso.', ['Usuario' => Auth::user(), 'Permiso' => $Permission]);
         // Registro de Actividades
         $activity = activity()->log('crear nuevo recurso '. $Permission->name);
         $activity->causer_type = 'PermissionController';
         $activity->save();
    
        return redirect()->route('permissions.index')
            ->with('success', 'Permiso Creado Satisfactoriamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = Permission::find(decrypt($id));

         // Registro en log.
         \Log::info('|::| permissions.view Vista del recurso especificado.', ['Usuario' => Auth::user(), 'Permiso' => $permission]);
         // Registro de Actividades
         $activity = activity()->log('Vista del recurso especificado '. $permission->name);
         $activity->causer_type = 'PermissionController';
         $activity->save();
    
        return view('permissions.show', compact('permission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::find(decrypt($id));

        // Registro en log.
        \Log::info('|::| writeupdate Formulario de Actualización  el recurso especificado.', ['Usuario' => Auth::user(), 'Permiso' => $permission]);
    
        return view('permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
    
        $permission = Permission::find(decrypt($id));
        $permission->name = $request->input('name');
        $permission->save();

        // Registro en log.
        \Log::info('|::| permissions.update Actualizar el recurso especificado.', ['Usuario' => Auth::user(), 'Rol' => $permission]);
        // Registro de Actividades
        $activity = activity()->log('Actualizar el recurso especificado '. $permission->name);
        $activity->causer_type = 'PermissionController';
        $activity->save();
        
        return redirect()->route('permissions.index')
            ->with('success', 'Permiso Actualizado Satisfactoriamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $permission = Permission::find(decrypt($request->id_permission));
        $permission->status = 0;
        $permission->save();

        // Registro en log.
        \Log::info('|::| permissions.delete Eliminar el recurso especificado.', ['Usuario' => Auth::user(), 'Permiso' => $permission]);
        // Registro de Actividades
        $activity = activity()->log('Recurso eliminado '. $permission->name);
        $activity->causer_type = 'PermissionController';
        $activity->save();

        
        return redirect()->route('permissions.index')
            ->with('success', 'Permiso Inactivo');
    }

    public function getData()
    {
        $permiss = Permission::all(); 
        return Datatables::of($permiss)
            ->addColumn('action', function ($permiss) {

                $result = '';

                if ($permiss->status == 1){

                    if (Auth::user()->can('permission-view'))
                        $result .= " <a href='".route('permissions.show', encrypt($permiss->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                    if (Auth::user()->can('permission-edit'))
                        $result .= " <a href='".route('permissions.edit', encrypt($permiss->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";
    
                    if (Auth::user()->can('permission-delete'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-id='".encrypt($permiss->id)."' data-title='".$permiss->name."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }
                else
                {
                        if (Auth::user()->admin == 1)
                        $result .= " <button type='submit' class='btn btn-sm btn-primary' data-toggle='modal' alt='Restaurar' data-id='".encrypt($permiss->id)."' data-tit='".$permiss->name."' data-target='#restore'><i class='fa fa-refresh' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->editColumn('id', '{{$id}}')
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->make(true);
    }

    public function restore(Request $request)
    {
        
        $permission = Permission::find(decrypt($request->id_permission));
        $permission->status = 1;
        $permission->save();

        // Registro en log.
        \Log::info('|::| Rol.restore Restaurar el recurso especificado.', ['Usuario' => Auth::user(), 'Permiso' => $permission]);
        // Registro de Actividades
        $activity = activity()->log('Recurso restaurado '. $permission->name);
        $activity->causer_type = 'PermissionController';
        $activity->save();



        return redirect()->route('permissions.index')
            ->with('success', 'Se Actualizó el Permiso');
    }
}