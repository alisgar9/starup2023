<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;
use App\Models\LogActivity;
use Illuminate\Support\Str;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use DB;
use App\Exports\UsersExport;
use App\Exports\ReportContainersExport;
use Maatwebsite\Excel\Facades\Excel;

class ConteinerReportController extends Controller
{
    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct()
    {
         $this->middleware('permission:reportgnral-list|reportgnral-create|reportgnral-edit|reportgnral-delete', ['only' => ['index','store']]);
         $this->middleware('permission:reportgnral-create', ['only' => ['create','store']]);
         $this->middleware('permission:reportgnral-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:reportgnral-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $consignee = DB::connection('mysql2')->table('adm_entidad')->where([['status', 1]])->orderBy('nombre')->pluck('nombre', 'id');
        return view('reportes.reportContainers', compact('consignee'));
    }
    
    public function LastFreeDay($eta,$freeDay){

        $free_demurrages_days_cust = $freeDay - 1;           
        //$newDateETA = $eta->addDays($free_demurrages_days_cust);
        $newDateETA =  (new Carbon($eta))->addDays($free_demurrages_days_cust);
        $newDateETA_clear = Carbon::parse($newDateETA);
                    
        return $newDateETA_clear;

    }

    public function Descentidad($id){
        
        $entidad = DB::connection('mysql2')->table('adm_entidad')->where("id","=",$id)->first();

        return $entidad;
    }
    
    /** 
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function generateReport(Request $request)
    {

        $custom= $request->custom;
        $dateStart=$request->etas;
        $dateFinish=$request->etast;
        $type = $request->type;

        $data =  DB::connection('mysql2')->table('adm_operaciones_contenedor as A')
            ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.id AS SUMEX','P.hbl','P.mbl','P.ETA','P.ETD', 'P.ATA','P.free_demurrages_days_cust','P.tipo_operacion','P.operacion', 'P.precio_demora','P.PO','P.PI', 'P.iva', 'P.agente','C.descripcion AS tipo_operacion_desc')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where([['U.activo','=',1],['P.consignee','=',$custom],[
                function($query) use ($type){
                    if($type == 1)
                    $query->where('A.activo', '=', 1);
                    elseif($type == 2)
                    $query->where('A.activo', '=', 2);
                    else
                    $query->where('A.activo', '=', 2)
                    ->orWhere('A.activo', '=', 2)
                    ->orWhere('A.activo', '=', 3);
                }
               ]])
            ->whereBetween('P.ETA', array($dateStart, $dateFinish))
            ->orderby('P.id', 'asc')->get()->toArray();
           

            $datas = array();

            foreach ($data as $key => $value) {
                $i= $key+2;
                $folio = 'SUMEX0'.$value->SUMEX;

                $datas[] = array(
                    'FOLIO' => $folio,
                    'HBL' => $value->hbl,
                    'MBL' => $value->mbl,
                    'ETD' => Carbon::parse($value->ETD)->format("d/m/Y"),
                    'ATA' => Carbon::parse($value->ATA)->format("d/m/Y"),
                    'LastFreeDay' => Carbon::parse($this->LastFreeDay($value->ATA,$value->free_demurrages_days_cust))->format("d/m/Y"),
                    'Container' => $value->container_number,
                    'CustomsBroker' => isset($value->agente) ? $this->Descentidad($value->agente)->nombre : NULL
                );
                        
            }

            $export = new ReportContainersExport($datas);
            return Excel::download($export, 'ReporConteiner.xlsx');
        
    }

}
