<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;
use App\Models\LogActivity;
use Illuminate\Support\Str;

class RoleController extends Controller
{
    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct()
    {
         $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
         $this->middleware('permission:role-create', ['only' => ['create','store']]);
         $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Role::orderBy('id','DESC')->paginate(5);

        // Registro en log.
        \Log::info('|::| Rol.index recursos.', ['Usuario' => Auth::user()]);

        return view('roles.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::get();

        // Registro en log.
        \Log::info('|::| Rol.createview Formulario para crear nuevo recurso.', ['Usuario' => Auth::user()]);
        

        return view('roles.create', compact('permission'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);
    
        $role = Role::create(['name' => Str::ucfirst($request->input('name'))]);
        $role->syncPermissions($request->input('permission'));
         // Registro en log.
         \Log::info('|::| Rol.create crear nuevo recurso.', ['Usuario' => Auth::user(), 'Rol' => $role]);
         // Registro de Actividades
         $activity = activity()->log('crear nuevo recurso '. $role->name);
         $activity->causer_type = 'RoleController';
         $activity->save();
    
        return redirect()->route('roles.index')
            ->with('success', 'Role Creado Satisfactoriamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find(decrypt($id));
        $rolePermissions = Permission::join('role_has_permissions', 'role_has_permissions.permission_id', 'permissions.id')
            ->where('role_has_permissions.role_id',$id)
            ->get();

         // Registro en log.
        \Log::info('|::| Rol.view Vista del recurso especificado.', ['Usuario' => Auth::user(), 'Rol' => $role]);
        // Registro de Actividades
        $activity = activity()->log('Vista del recurso especificado '. $role->name);
        $activity->causer_type = 'RoleController';
        $activity->save();
    
        return view('roles.show', compact('role', 'rolePermissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find(decrypt($id));
        $permission = Permission::get();
        $rolePermissions = DB::table('role_has_permissions')
            ->where('role_has_permissions.role_id', decrypt($id))
            ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
            ->all();


        // Registro en log.
        \Log::info('|::| Rol.writeupdate Formulario de Actualización  el recurso especificado.', ['Usuario' => Auth::user(), 'Rol' => $role]);

    
        return view('roles.edit', compact('role', 'permission', 'rolePermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);

    
        $role = Role::find(decrypt($id));
        $role->name = Str::ucfirst($request->input('name'));
        $role->save();
    
        $role->syncPermissions($request->input('permission'));

        // Registro en log.
        \Log::info('|::| Rol.update Actualizar el recurso especificado.', ['Usuario' => Auth::user(), 'Rol' => $role]);
        // Registro de Actividades
        $activity = activity()->log('Recurso Actualizado '. $role->name);
        $activity->causer_type = 'RoleController';
        $activity->save();

    
        return redirect()->route('roles.index')
            ->with('success', 'Rol Actualizado Satisfactoriamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {

        $role = Role::find(decrypt($request->id_rol));
        $role->status = 0;
        $role->save();

        // Registro en log.
        \Log::info('|::| Rol.delete Eliminar el recurso especificado.', ['Usuario' => Auth::user(), 'Rol' => $role]);
        // Registro de Actividades
        $activity = activity()->log('Recurso eliminado '. $role->name);
        $activity->causer_type = 'RoleController';
        $activity->save();

        
        return redirect()->route('roles.index')
            ->with('success', 'Rol de Usuario Inactivo');
    }

    public function getData()
    {
        $roles = Role::all(); 
        return Datatables::of($roles)
            ->addColumn('action', function ($roles) {

                $result = '';

                if ($roles->status == 1){

                    if (Auth::user()->can('role-view'))
                        $result .= " <a href='".route('roles.show', encrypt($roles->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                    if (Auth::user()->can('role-edit'))
                        $result .= " <a href='".route('roles.edit', encrypt($roles->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";
    
                    if (Auth::user()->can('role-delete'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idrol='".encrypt($roles->id)."' data-title='".$roles->name."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }
                else
                {
                        if (Auth::user()->admin == 1)
                        $result .= " <button type='submit' class='btn btn-sm btn-primary' data-toggle='modal' alt='Restaurar' data-idrol='".encrypt($roles->id)."' data-tit='".$roles->name."' data-target='#restore'><i class='fa fa-refresh' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->editColumn('id', '{{$id}}')
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->make(true);
    }

    public function restore(Request $request)
    {
        
        $role = Role::find(decrypt($request->id_rol));
        $role->status = 1;
        $role->save();

        // Registro en log.
        \Log::info('|::| Rol.restore Restaurar el recurso especificado.', ['Usuario' => Auth::user(), 'Rol' => $role]);
        // Registro de Actividades
        $activity = activity()->log('Recurso restaurado '. $role->name);
        $activity->causer_type = 'RoleController';
        $activity->save();



        return redirect()->route('roles.index')
            ->with('success', 'Se Actualizó el Rol de Usuario Inactivo');
    }


}