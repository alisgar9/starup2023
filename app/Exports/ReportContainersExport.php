<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReportContainersExport implements FromArray, WithHeadings
{
    public function __construct(array $datas)
    {
        $this->datas = $datas;
    }

    public function array(): array
    {
        
        return $this->datas;
    }

    public function headings(): array
    {
        return [
            "FOLIO", 
            "HBL", 
            "MBL",
            "ETD",
            "ATA",
            "Last Free Day",
            "# Container",
            "Customs Broker"
        ];
    }
}
