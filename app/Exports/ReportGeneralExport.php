<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReportGeneralExport implements FromArray, WithHeadings
{
    public function __construct(array $datas)
    {
        $this->datas = $datas;
    }

    public function array(): array
    {
        
        return $this->datas;
    }

    public function headings(): array
    {
        return [
            "FOLIO", 
            "Shipper", 
            "# PO",
            "# PI",
            "# Booking",
            "Origin Port",
            "Origin Country",
            "Destination Port",
            "Destination Country",
            "Final Destination",
            "Vessel",
            "ETD",
            "ATD",
            "ETA",
            "ATA",
            "HBL",
            "MBL",
            "# Container",
            "40H",
            "Status Booking",
            "Free Days",
            "comments",
            "Rate",
            "Expense BL",
            "Expense Contr",
            "Invoice Number",
            "TELEX",
            "TERMINAL",
            "Shipping Line",
            "Transit Time"
        ];
    }
}
