<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OperacionUsers extends Model
{
  /**
  * El nombre de la tabla donde se almacena los datos
  * @var String
  * @access protected
  */
  protected $table = 'adm_operaciones_usuarios';

  /**
  * El nombre de la llave primaria
  * @var String
  * @access protected
  */
  protected $primaryKey = 'id';

  /**
   * Los atributos que pueden ingresarlos de forma masiva
   *
   * @var array
   */
  protected $fillable = [
      'id_user',
      'id_operacion',
      'activo',
      'type'
  ];

// establecer un valor predeterminado.
protected $attributes = array(
  'activo' => 1,
);

public function user_compartido()
{
    return $this->belongsTo('App\Operacion','id');
}


  
}
