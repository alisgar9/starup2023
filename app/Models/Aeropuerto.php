<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aeropuerto extends Model
{
  /**
  * El nombre de la tabla donde se almacena los datos
  * @var String
  * @access protected
  */
  protected $table = 'adm_aeropuertos';

  /**
  * El nombre de la llave primaria
  * @var String
  * @access protected
  */
  protected $primaryKey = 'id';

  /**
   * Los atributos que pueden ingresarlos de forma masiva
   *
   * @var array
   */
  protected $fillable = [
      'nombre',
      'pais',
      'clave',
      'activo',
  ];

  // establecer un valor predeterminado.
  protected $attributes = array(
    'activo' => 1,
  );

  // tabla relacional
  public function pais()
  {
      return $this->belongsTo('App\Models\Pais','pais');
  }


}
