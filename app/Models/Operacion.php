<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operacion extends Model
{
  /**
  * El nombre de la tabla donde se almacena los datos
  * @var String
  * @access protected
  */
  protected $table = 'adm_operaciones';

  /**
  * El nombre de la llave primaria
  * @var String
  * @access protected
  */
  protected $primaryKey = 'id';

  /**
   * Los atributos que pueden ingresarlos de forma masiva
   *
   * @var array
   */
  protected $fillable = [
      'hbl',
      'mbl',
      'file_hbl',
      'file_mbl',
      'house_release',
      'master_release',
      'ETD',
      'ETA',
      'ATD',
      'ATA',
      'vessel',
      'POL',
      'POD',
      'final_dest',
      'shipper',
      'consignee',
      'agente',
      'agente_envio',
      'shipping_line',
      'INCOTERM',
      'commodity',
      'EIR',
      'file_EIR',
      'empty_conteiner',
      'free_demurrages_days_cust',
      'free_demurrages_days_sl',
      'free_storage_days',
      'house_release_date',
      'master_release_date',
      'tipo_operacion',
      'operacion_detail',
      'operacion',
      'share',
      'id_user',
      'cy',
      'cy_door',
      'precio_demora',
      'iva',
      'PO',
      'PI',
      'factura',
      'sc',
      'send_mail',
      'pre_operation',
      'comments',
      'terminal',
      'booking',
      'origin_contry',
      'destination_contry',
      '40H',
      'booking_status',
      'expense_bl',
      'expense_contr',
      'tarifa',
  ];
  
  protected $dates = [
    'ETD'  => 'date:Y-m-d',
    'ETA'  => 'date:Y-m-d',
];

  // establecer un valor predeterminado.
  protected $attributes = array(
    'activo' => 1,
  );

  // tabla relacional
  public function POLdesc()
  {
      return $this->belongsTo('App\Puerto','POL');
  }

  public function PODdesc()
  {
      return $this->belongsTo('App\Puerto','POD');
  }


  public function POLdescAir()
  {
      return $this->belongsTo('App\Aeropuerto','POL');
  }

  public function PODdescAir()
  {
      return $this->belongsTo('App\Aeropuerto','POD');
  }

  public function POLdescLine()
  {
      return $this->belongsTo('App\Estado','POL');
  }

  public function PODdescLine()
  {
      return $this->belongsTo('App\Estado','POD');
  }


  public function shipperDesc()
  {
      return $this->belongsTo('App\Entidades','shipper');
  }
  public function consigneeDesc()
  {
      return $this->belongsTo('App\Entidades','consignee');
  }
  public function agenteDesc()
  {
      return $this->belongsTo('App\Entidades','agente');
  }
  public function slineDesc()
  {
      return $this->belongsTo('App\Entidades','shipping_line');
  }
  public function incoDesc()
  {
      return $this->belongsTo('App\Incoterm','INCOTERM');
  }
  public function libhblDesc()
  {
      return $this->belongsTo('App\Liberacion','house_release');
  }
  public function libmblDesc()
  {
      return $this->belongsTo('App\Liberacion','master_release');
  }
  public function sharedUsers()
  {
      return $this->belongsTo('App\OperacionDetails','id_operacion');
  }
  public function AEnDesc()
  {
      return $this->belongsTo('App\Entidades','agente_envio');
  }
  public function shareuser()
  {
      return $this->belongsTo('App\OperacionUsers','id_operacion');
  }
  


}
