<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
     /**
    * El nombre de la tabla donde se almacena los datos
    * @var String
    * @access protected
    */
    protected $table = 'adm_paises';

    /**
     * El nombre de la llave primaria
    * @var String
    * @access protected
    */
    protected $primaryKey = 'id';

    /**
     * Los atributos que pueden ingresarlos de forma masiva
     *
     * @var array
     */
    protected $fillable = [
        'iso',
        'nombre'
    ]; 

    
}
