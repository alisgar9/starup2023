<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
  /**
  * El nombre de la tabla donde se almacena los datos
  * @var String
  * @access protected
  */
  protected $table = 'adm_estados';

  /**
  * El nombre de la llave primaria
  * @var String
  * @access protected
  */
  protected $primaryKey = 'id';

  /**
   * Los atributos que pueden ingresarlos de forma masiva
   *
   * @var array
   */
  protected $fillable = [
      'nombre',
      'pais',
      'clave',
      'activo',
      'created_at',
      'updated_at'
  ];

  // establecer un valor predeterminado.
  protected $attributes = array(
    'activo' => 1,
  );

  // tabla relacional
  public function pais()
  {
      return $this->belongsTo('App\Pais','pais');
  }


}
