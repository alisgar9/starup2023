<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
  /**
  * El nombre de la tabla donde se almacena los datos
  * @var String
  * @access protected
  */
  protected $table = 'adm_cargo';

  /**
  * El nombre de la llave primaria
  * @var String
  * @access protected
  */
  protected $primaryKey = 'idcargo';

  /**
   * Los atributos que pueden ingresarlos de forma masiva
   *
   * @var array
   */
  protected $fillable = [
      'descripcion',
      'activo',
  ];

  // establecer un valor predeterminado.
  protected $attributes = array(
    'activo' => 1,
  );
}
