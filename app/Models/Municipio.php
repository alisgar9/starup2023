<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
  /**
  * El nombre de la tabla donde se almacena los datos
  * @var String
  * @access protected
  */
  protected $table = 'municipios';

  /**
  * El nombre de la llave primaria
  * @var String
  * @access protected
  */
  protected $primaryKey = 'id';

  /**
   * Los atributos que pueden ingresarlos de forma masiva
   *
   * @var array
   */
  protected $fillable = [
      'clave',
      'municipio',
      'cabecera_municipal',
      'estado'
  ];

    // establecer un valor predeterminado.
    protected $attributes = array(
        'estado' => 1,
    );

  public function recibos()
  {
      return $this->belongsToMany('App\Recibo');
  }
}
