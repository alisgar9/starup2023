
CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `agent` varchar(255) DEFAULT NULL,
  `log_name` text NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `properties` text NOT NULL,
  `causer_id` int(11) NOT NULL,
  `causer_type` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `activity_log`
--

INSERT INTO `activity_log` (`id`, `subject`, `url`, `method`, `ip`, `agent`, `log_name`, `updated_at`, `created_at`, `properties`, `causer_id`, `causer_type`, `description`) VALUES
(243, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-02 04:04:49', '2022-08-02 04:04:49', '[]', 1, 'App\\Models\\User', 'Look, I logged something'),
(244, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-02 04:21:15', '2022-08-02 04:21:15', '[]', 1, 'RoleController', 'Recurso eliminadoventas'),
(245, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-02 04:22:15', '2022-08-02 04:22:15', '[]', 1, 'RoleController', 'Recurso restauradoventas'),
(246, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-02 04:23:57', '2022-08-02 04:23:57', '[]', 1, 'RoleController', 'Recurso eliminadoventas'),
(247, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-02 04:35:11', '2022-08-02 04:35:11', '[]', 1, 'RoleController', 'Recurso restaurado ventas'),
(248, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-02 04:40:17', '2022-08-02 04:40:17', '[]', 1, 'RoleController', 'crear nuevo recurso Supervisor'),
(249, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-02 05:24:52', '2022-08-02 05:24:52', '[]', 1, 'PermissionController', 'crear nuevo recurso permission-view'),
(250, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-02 05:29:11', '2022-08-02 05:29:11', '[]', 1, 'RoleController', 'Recurso Actualizado Admin'),
(251, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-02 05:46:47', '2022-08-02 05:46:47', '[]', 1, 'PermissionController', 'Actualizar el recurso especificado permission-views'),
(252, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-02 06:01:38', '2022-08-02 06:01:38', '[]', 1, 'PermissionController', 'Recurso eliminado permission-views'),
(253, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-02 06:01:48', '2022-08-02 06:01:48', '[]', 1, 'PermissionController', 'Recurso restaurado permission-views'),
(254, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-02 06:02:00', '2022-08-02 06:02:00', '[]', 1, 'PermissionController', 'Actualizar el recurso especificado permission-view'),
(255, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-02 06:02:06', '2022-08-02 06:02:06', '[]', 1, 'PermissionController', 'Vista del recurso especificado user-list'),
(256, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:12:03', '2022-08-03 05:12:03', '[]', 1, 'UsersController', 'Actualizar el recurso especificado ALICIA ALANIS GARCIA'),
(257, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:12:19', '2022-08-03 05:12:19', '[]', 1, 'UsersController', 'Actualizar el recurso especificado ALICIA ALANIS GARCIA'),
(258, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:18:52', '2022-08-03 05:18:52', '[]', 1, 'UsersController', 'Actualizar el recurso especificado ALICIA ALANIS'),
(259, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:19:03', '2022-08-03 05:19:03', '[]', 1, 'UsersController', 'Actualizar el recurso especificado ALICIA ALANIS'),
(260, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:20:15', '2022-08-03 05:20:15', '[]', 1, 'UsersController', 'Actualizar el recurso especificado ALICIA ALANIS'),
(261, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:20:36', '2022-08-03 05:20:36', '[]', 1, 'UsersController', 'Actualizar el recurso especificado ALICIA ALANIS'),
(262, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:21:29', '2022-08-03 05:21:29', '[]', 1, 'PermissionController', 'crear nuevo recurso user-view'),
(263, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:22:55', '2022-08-03 05:22:55', '[]', 1, 'RoleController', 'Recurso Actualizado Admin'),
(264, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:23:08', '2022-08-03 05:23:08', '[]', 1, 'UsersController', 'Vista del recurso especificado ALICIA ALANIS GARCIA'),
(265, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:25:36', '2022-08-03 05:25:36', '[]', 1, 'UsersController', 'Vista del recurso especificado ALICIA ALANIS GARCIA'),
(266, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:26:06', '2022-08-03 05:26:06', '[]', 1, 'UsersController', 'Vista del recurso especificado ALICIA ALANIS GARCIA'),
(267, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:26:21', '2022-08-03 05:26:21', '[]', 1, 'UsersController', 'Vista del recurso especificado ALICIA ALANIS GARCIA'),
(268, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:27:28', '2022-08-03 05:27:28', '[]', 1, 'UsersController', 'Vista del recurso especificado ALICIA ALANIS GARCIA'),
(269, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:28:53', '2022-08-03 05:28:53', '[]', 1, 'UsersController', 'Vista del recurso especificado ALICIA ALANIS GARCIA'),
(270, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:29:24', '2022-08-03 05:29:24', '[]', 1, 'UsersController', 'Vista del recurso especificado ALICIA ALANIS GARCIA'),
(271, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:29:41', '2022-08-03 05:29:41', '[]', 1, 'UsersController', 'Vista del recurso especificado ALICIA ALANIS GARCIA'),
(272, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:29:56', '2022-08-03 05:29:56', '[]', 1, 'UsersController', 'Vista del recurso especificado ALICIA ALANIS'),
(273, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:43:35', '2022-08-03 05:43:35', '[]', 1, 'UsersController', 'Recurso restaurado alis alis alis'),
(274, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:43:47', '2022-08-03 05:43:47', '[]', 1, 'UsersController', 'Recurso restaurado jesus garcia'),
(275, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:43:50', '2022-08-03 05:43:50', '[]', 1, 'UsersController', 'Recurso restaurado Jitesh Meniya'),
(276, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:43:57', '2022-08-03 05:43:57', '[]', 1, 'UsersController', 'Recurso eliminado Jitesh Meniya'),
(277, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 05:51:01', '2022-08-03 05:51:01', '[]', 1, 'UsersController', 'Vista del recurso especificado ALICIA ALANIS GARCIA'),
(278, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 06:09:41', '2022-08-03 06:09:41', '[]', 1, 'UsersController', 'Recurso restaurado Jitesh Meniya'),
(279, NULL, NULL, NULL, NULL, NULL, 'default', '2022-08-03 06:10:15', '2022-08-03 06:10:15', '[]', 1, 'UsersController', 'Actualizar el recurso especificado ALICIA ALANIS'),
(280, NULL, NULL, NULL, NULL, NULL, 'default', '2022-10-24 20:35:46', '2022-10-24 20:35:46', '[]', 1, 'PermissionController', 'crear nuevo recurso reportgnral-list'),
(281, NULL, NULL, NULL, NULL, NULL, 'default', '2022-10-24 20:36:05', '2022-10-24 20:36:05', '[]', 1, 'PermissionController', 'crear nuevo recurso reportgnral-create'),
(282, NULL, NULL, NULL, NULL, NULL, 'default', '2022-10-24 20:36:31', '2022-10-24 20:36:31', '[]', 1, 'PermissionController', 'crear nuevo recurso reportgnral-edit'),
(283, NULL, NULL, NULL, NULL, NULL, 'default', '2022-10-24 20:36:49', '2022-10-24 20:36:49', '[]', 1, 'PermissionController', 'crear nuevo recurso reportgnral-delete'),
(284, NULL, NULL, NULL, NULL, NULL, 'default', '2022-10-24 20:37:10', '2022-10-24 20:37:10', '[]', 1, 'RoleController', 'Recurso Actualizado Admin'),
(285, NULL, NULL, NULL, NULL, NULL, 'default', '2022-10-24 20:37:24', '2022-10-24 20:37:24', '[]', 1, 'RoleController', 'Recurso Actualizado Ventas'),
(286, NULL, NULL, NULL, NULL, NULL, 'default', '2022-10-24 20:44:08', '2022-10-24 20:44:08', '[]', 1, 'UsersController', 'Recurso recién creado: oscar@starup.com.mx'),
(287, NULL, NULL, NULL, NULL, NULL, 'default', '2022-10-24 20:45:58', '2022-10-24 20:45:58', '[]', 1, 'UsersController', 'Recurso restaurado OSCAR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_07_29_190636_create_permission_tables', 1),
(6, '2022_07_29_190638_create_posts_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(1, 'App\\Models\\User', 7),
(2, 'App\\Models\\User', 8),
(3, 'App\\Models\\User', 8),
(2, 'App\\Models\\User', 9),
(2, 'App\\Models\\User', 10),
(3, 'App\\Models\\User', 10),
(2, 'App\\Models\\User', 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 activo, 0 inactivo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'user-list', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(2, 'user-create', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(3, 'user-edit', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(4, 'user-delete', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(5, 'role-list', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(6, 'role-create', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(7, 'role-edit', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(8, 'role-delete', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(9, 'permission-list', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(10, 'permission-create', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(11, 'permission-edit', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(12, 'permission-delete', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(13, 'post-list', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(14, 'post-create', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(15, 'post-edit', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(16, 'post-delete', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(17, 'role-view', 'web', 1, '2022-07-30 05:57:41', '2022-07-30 05:57:41'),
(18, 'permission-view', 'web', 1, '2022-08-02 05:24:52', '2022-08-02 06:02:00'),
(19, 'user-view', 'web', 1, '2022-08-03 05:21:29', '2022-08-03 05:21:29'),
(20, 'reportgnral-list', 'web', 1, '2022-10-24 20:35:46', '2022-10-24 20:35:46'),
(21, 'reportgnral-create', 'web', 1, '2022-10-24 20:36:05', '2022-10-24 20:36:05'),
(22, 'reportgnral-edit', 'web', 1, '2022-10-24 20:36:31', '2022-10-24 20:36:31'),
(23, 'reportgnral-delete', 'web', 1, '2022-10-24 20:36:49', '2022-10-24 20:36:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 Activo, 0 Inactivo',
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `status`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 1, 'web', '2022-07-30 05:57:51', '2022-08-02 05:29:11'),
(2, 'Ventas', 1, 'web', '2022-07-30 05:57:51', '2022-10-24 20:37:24'),
(3, 'Supervisor', 1, 'web', '2022-08-02 04:40:17', '2022-08-02 04:40:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(13, 3),
(14, 3),
(15, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 aprovado, 0 inaprovado',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `admin`, `remember_token`, `approved`, `created_at`, `updated_at`) VALUES
(1, 'ALICIA ALANIS GARCIA', 'alisgar9@gmail.com', NULL, '$2y$10$odlWYVCFm0Dlqq6qOWN9iOQcseR8214A4/aUU/f2cAMCNZzSDlQnu', 1, NULL, 1, '2022-07-30 05:59:03', '2022-08-03 05:08:24'),
(11, 'OSCAR', 'oscar@starup.com.mx', NULL, '$2y$10$jHwDMBMy0e4FFMcy/qlmbuTiQmPiXDGWbjvJtzzQ1SzZCj78RLN02', 0, 'PaWPN4rlz9HpmpiRxkkVvlZMjUXwKCmrDdevEAvA5wmxSr5HdeGwpQcgKspM', 1, '2022-10-24 20:44:08', '2022-10-24 20:45:58');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_title_unique` (`title`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indices de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=288;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
